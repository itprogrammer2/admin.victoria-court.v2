<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;


class Area_Model extends Model
{
    
    public static function LoadArea(){

        $result = DB::connection('rmsnew')
        ->table('tblroomareas')
        ->select(
            '*'
        )
        ->get();

        return $result;

    }

    public static function ValidateArea($area){

        $result = DB::connection('rmsnew')
        ->table('tblroomareas')
        ->select(
           DB::raw("COUNT(*) AS 'area_count'")
        )
        ->where('room_area', '=', $area)
        ->first();

        if($result->area_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function SaveArea($data){

        DB::connection('rmsnew')
        ->table('tblroomareas')
        ->insert([
            "room_area"=>$data->area
        ]);

    }

    public static function LoadAreaInformation($data){

        $result = DB::connection('rmsnew')
        ->table('tblroomareas')
        ->select(
            'room_area'
        )
        ->where('id', '=', $data->id)
        ->first();

        return $result;
        
    }

    public static function UpdateArea($data){

        DB::connection('rmsnew')
        ->table('tblroomareas')
        ->where('id', '=', $data->id)
        ->update([
            "room_area"=>$data->area
        ]);

    }

}
