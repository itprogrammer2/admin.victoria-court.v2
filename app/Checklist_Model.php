<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Checklist_Model extends Model
{

    public static function LoadChecklist(){

        $result = DB::connection('rmsnew')
        ->table('tbl_links')
        ->select(
            'tbl_links.id',
            DB::raw("tbl_areas.name AS 'area_name'"),
            DB::raw("tbl_components.name AS 'component_name'"),
            DB::raw("tbl_standards.name AS 'standard_name'"),
            DB::raw("tbl_remarks.name AS 'remark_name'")
        )
        ->join('tbl_areas', 'tbl_areas.id', '=', 'tbl_links.area_id')
        ->join('tbl_components', 'tbl_components.id', '=', 'tbl_links.component_id')
        ->join('tbl_standards', 'tbl_standards.id', '=', 'tbl_links.standard_id')
        ->join('tbl_remarks', 'tbl_remarks.id', '=', 'tbl_links.remarks_id')
        ->get();

        return $result;

    }

    public static function LoadAreas(){

        $result =  DB::connection('rmsnew')
        ->table('tbl_areas')
        ->select(
            'id',
            'name'
        )
        ->get();

        return $result;

    }

    public static function LoadComponents(){

        $result =  DB::connection('rmsnew')
        ->table('tbl_components')
        ->select(
            'id',
            'name'
        )
        ->get();

        return $result;

    }

    public static function LoadStandards(){

        $result =  DB::connection('rmsnew')
        ->table('tbl_standards')
        ->select(
            'id',
            'name'
        )
        ->get();

        return $result;

    }

    public static function LoadRemarks($data){

        $result =  DB::connection('rmsnew')
        ->table('tbl_remarks')
        ->select(
            'id',
            'name'
        )
        ->where('standard_id', '=', $data->id)
        ->get();

        return $result;

    }

    public static function SaveChecklistInformation($data){

        DB::connection('rmsnew')
        ->table('tbl_links')
        ->insert([
            "area_id"=>$data->area,
            "component_id"=>$data->component,
            "standard_id"=>$data->standard,
            "remarks_id"=>$data->remark,
            "finding_type_id"=>$data->findingtype,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadFindingType($remarksid){

        $result = DB::connection('rmsnew')
        ->table('tbl_deduction')
        ->select(
            'tbl_findings_type.id',
            'tbl_findings_type.name',
            'tbl_deduction.points'
        )
        ->leftjoin('tbl_findings_type', 'tbl_findings_type.id', '=', 'tbl_deduction.tbl_findings_type_id')
        ->where('tbl_deduction.remarks_id', '=', $remarksid)
        ->get();

        return $result;

    }

    public static function GetChecklistInformation($linkid){

        $result = DB::connection('rmsnew')
        ->table('tbl_links')
        ->select(
            '*'
        )
        ->where('id', '=', $linkid)
        ->first();

        return $result;

    }

    public static function UpdateChecklistInformation($data){

        DB::connection('rmsnew')
        ->table('tbl_links')
        ->where('id', '=', $data->linkid)
        ->update([
            "area_id"=>$data->area,
            "component_id"=>$data->components,
            "standard_id"=>$data->standard,
            "remarks_id"=>$data->remarks,
            "finding_type_id"=>$data->findingtype,
            "updated_at"=>DB::raw("NOW()")
        ]);


    }

    public static function DeleteChecklistInformation($data){

        DB::connection('rmsnew')
        ->table('tbl_links')
        ->where('id', '=', $data->linkid)
        ->delete();


    }

}
