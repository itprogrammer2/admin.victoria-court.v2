<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Deduction_Model extends Model
{

    public static function SaveDeduction($remarksid, $findingtype, $points){

        DB::connection('rmsnew')
        ->table('tbl_deduction')
        ->insert([
            'remarks_id'=>$remarksid,
            'tbl_findings_type_id'=>$findingtype,
            'points'=>$points,
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function ValidateDeduction($remarksid){

        $result = DB::connection('rmsnew')
        ->table('tbl_deduction')
        ->select(
            DB::raw("COUNT(*) AS 'remarks_deduction_count'")
        )
        ->where('remarks_id', '=', $remarksid)
        ->first();

        if($result->remarks_deduction_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function UpdateDeduction($id, $findingtype, $points){

        DB::connection('rmsnew')
        ->table('tbl_deduction')
        ->where('id', '=', $id)
        ->update([
            'tbl_findings_type_id'=>$findingtype,
            'points'=>$points,
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function GetRemarksDeductionInfo($remarksid){

        $result = DB::connection('rmsnew')
        ->table('tbl_deduction')
        ->select(
            '*'
        )
        ->where('remarks_id', '=', $remarksid)
        ->get();

        return $result;

    }

    public static function DeleteDeduction($id){

        DB::connection('rmsnew')
        ->table('tbl_deduction')
        ->where('id', '=', $id)
        ->delete();

    }

}
