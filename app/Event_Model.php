<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Event_Model extends Model
{

    public static function LoadEventReason(){

        $result = DB::connection('rmsnew')
        ->table('events_reasons')
        ->select(
            'id',
            'reason',
            'color'
        )
        ->get();

        return $result;

    }

    public static function ValidateEventReason($reason){

        $result = DB::connection('rmsnew')
        ->table('events_reasons')
        ->select(
            DB::raw("COUNT(*) AS 'reasoncount'")
        )
        ->where('reason', '=', $reason)
        ->first();

        if($result->reasoncount!=0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function SaveEventReason($reason, $color){

        DB::connection('rmsnew')
        ->table('events_reasons')
        ->insert([
            "reason"=>$reason,
            "color"=>$color,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadEventProfile($id){

        $result = DB::connection('rmsnew')
        ->table('events_reasons')
        ->select(
            'reason',
            'color'
        )
        ->where('id', '=', $id)
        ->first();

        return $result;

    }

    public static function UpdateEventReason($id, $reason, $color){

        DB::connection('rmsnew')
        ->table('events_reasons')
        ->where('id', '=', $id)
        ->update([
            "reason"=>$reason,
            "color"=>$color,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

}
