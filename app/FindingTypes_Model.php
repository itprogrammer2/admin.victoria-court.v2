<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class FindingTypes_Model extends Model
{
   

    public static function LoadFindingTypes(){

        $result = DB::connection('rmsnew')
        ->table('tbl_findings_type')
        ->select(
            'id',
            'name'
        )
        ->get();

        return $result;

    }


}
