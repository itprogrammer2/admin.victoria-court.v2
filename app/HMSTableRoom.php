<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HMSTableRoom extends Model
{
    //
    protected $table = 'tblroom';
    protected $connection = 'hms';
}
