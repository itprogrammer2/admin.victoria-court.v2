<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;
use DB;
use App\Main_Model as Main;
use Illuminate\Support\Collection;
use Storage;
use App\Area_Model as Area;

class AreaController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        return view('area');

    }

    function LoadArea(){

        $area = Area::LoadArea();

        $data = array();
        foreach($area as $val){

            $obj = new \stdClass;

            $obj->id = $val->id;
            $obj->name = $val->room_area;
        
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-flat btn-primary" value="'.$val->id.'" data-toggle="modal" data-target="#updatearea"><i class="fa fa-edit"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function SaveArea(Request $request){

        $validate = Area::ValidateArea($request->area);

        if($validate){

            return json_encode([
                "success"=>false,
                "message"=>"Area information already exist."
            ]);

        }
        else{

            Area::SaveArea($request);

            return json_encode([
                "success"=>true,
                "message"=>"Area information has been save."
            ]);
 
        }


    }

    function LoadAreaInformation(Request $request){

        $area = Area::LoadAreaInformation($request);

        return json_encode([
            "area"=>$area->room_area
        ]);

    }

    function UpdateArea(Request $request){

        Area::UpdateArea($request);

        return json_encode([
            "success"=>true,
            "message"=>"Area information has been update."
        ]);

    }


}
