<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;
use DB;
use Illuminate\Support\Collection;
use App\Checklist_Model as Checklist;

class ChecklistController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        return view('inspection.checklist');

    }

    function LoadChecklist(){

        $checklist = Checklist::LoadChecklist();

        $data = array();
        foreach($checklist as $val){

            $obj = new \stdClass;

            $obj->area = $val->area_name;
            $obj->component = $val->component_name;
            $obj->standard = $val->standard_name;
            $obj->remarks = $val->remark_name;

            $obj->panel = '<div class="btn-group"><button id="btnedit" name="btnedit" class="btn btn-flat btn-primary" value="'.$val->id.'" data-toggle="modal" data-target="#updatechecklist"><i class="fa fa-edit"></i></button>
            <button id="btndelete" name="btndelete" class="btn btn-flat btn-danger" value="'.$val->id.'" data-toggle="modal" data-target="#deletechecklist"><i class="fa fa-trash"></i></button>
            </div>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);


    }

    function LoadAreas(){

        $areas = Checklist::LoadAreas();

        return json_encode([
            "data"=>$areas
        ]);

    }

    function LoadComponents(){

        $components = Checklist::LoadComponents();

        return json_encode([
            "data"=>$components
        ]);

    }

    function LoadStandards(){

        $standards = Checklist::LoadStandards();

        return json_encode([
            "data"=>$standards
        ]);

    }

    function LoadRemarks(Request $request){

        $remarks = Checklist::LoadRemarks($request);

        return json_encode([
            "data"=>$remarks
        ]);


    }

    function SaveChecklistInformation(Request $request){

        Checklist::SaveChecklistInformation($request);

        return json_encode([
            "success"=>true,
            "message"=>"Checklist information has been save."
        ]);

    }

    function LoadFindingType(Request $request){

        $findingtype = Checklist::LoadFindingType($request->remarksid);

        return json_encode([
            "data"=>$findingtype
        ]);

    }

    function GetChecklistInformation(Request $request){

        $checklistinfo = Checklist::GetChecklistInformation($request->linkid);

        return json_encode([
            'area_id'=>$checklistinfo->area_id,
            'component_id'=>$checklistinfo->component_id,
            'standard_id'=>$checklistinfo->standard_id,
            'remarks_id'=>$checklistinfo->remarks_id,
            'finding_type_id'=>$checklistinfo->finding_type_id
        ]);

    }

    function UpdateChecklistInformation(Request $request){

        Checklist::UpdateChecklistInformation($request);

        return json_encode([
            "success"=>true,
            "message"=>"Checklist information has been updated."
        ]);
    }

    function DeleteChecklistInformation(Request $request){

        Checklist::DeleteChecklistInformation($request);

        return json_encode([
            "success"=>true,
            "message"=>"Checklist information has been deleted."
        ]);


    }

}
