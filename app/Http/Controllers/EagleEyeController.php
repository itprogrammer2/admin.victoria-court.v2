<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EagleEye as EagleEye;
use App\InspectionComponent as Components;
use App\Standard as Standard;
use App\Remark;
use App\FindingType;
use App\InspectionLink as Link;
use DB;
use Illuminate\Support\Collection;
use DataTables;
use Response;
use App\FindingTypes_Model as Finding;
use App\Deduction_Model as Deduction;

class EagleEyeController extends Controller
{
    //
    public function index(){
    	return view('inspection.eagleeye');
    }

    public function components(){
    	return view('inspection.components');
    }

    public function standard(){
    	return view('inspection.standard');
    }

    public function remarks(){
    	return view('inspection.remarks');
    }

    public function findings(){
    	return view('inspection.finding');
    }

    public function getRoomInspection(){
    	$query = EagleEye::get();

    	$data = array();
    	foreach($query as $out){
    		$obj = new \stdClass;
    		$obj->name = $out->name;
    		$data[] = $obj;
    	}

    	$info = new Collection($data);
    	return DataTables::of($info)->rawColumns(['name'])->make(true);
    }

    public function saveRoomForInspection(Request $r){

    	// $this->validate($r, [
    	// 	'room' => 'required|string',
    	// ]);

    	EagleEye::insert([
    		'name' => $r->room,
    		'created_at' => DB::raw("NOW()")
    	]);

    	return json_encode([
    		"success" => true,
    		"message" => 'Inspection Location Added Succesfully'
    	]);

    }

    public function getComponentInspection(){
    	$query = Components::get();

    	$data = array();
    	foreach($query as $out){
    		$obj = new \stdClass;
    		$obj->name = $out->name;
    		$data[] = $obj;
    	}
    	$info = new Collection($data);
    	return DataTables::of($info)->rawColumns(['name'])->make(true);

    }

    public function saveComponentForInspection(Request $r){

    	$this->validate($r->all(), [
    		'component' => 'required|string',
        ]);

        $result = Components::select(
            DB::raw("COUNT(*) AS 'components_count'")
        )
        ->where('name', '=', $r->component)
        ->get();

        if($result[0]->components_count=="0"){

            Components::insert([
                'name' => $r->component,
                'created_at' => DB::raw("NOW()"),
            ]);


            return json_encode([
                "success" => true,
                "message" => 'Inspection Components Added Successfully'
            ]);


        }
        else{


            return json_encode([
                "success" => false,
                "message" => 'Inspection Components Already Exist'
            ]);


        }

    }

    public function getStandardInspection(){
    	$query = Standard::get();

    	$data = array();
    	foreach($query as $out){
    		$obj = new \stdClass;
    		$obj->name = $out->name;
    		$obj->description = $out->description;
    		$data[] = $obj;
    	}
    	$info = new Collection($data);
    	return DataTables::of($info)->make(true);
    }

    public function saveStandardForInspection(Request $r){

    	Standard::insert([
    		'name' => $r->standardName,
    		'description' => $r->standardDescription,
    		'created_at' => DB::raw("NOW()"),
    	]);

    	return json_encode([
    		'success' => true,
    		'message' => 'Inspection Standard Added Successfully'
    	]);

    }

    public function saveRemarksForInspection(Request $r){

        // $result = Remark::select(
        //     DB::raw("COUNT(*) AS 'remarks_count'")
        // )
        // ->where('standard_id', '=', $r->standard)
        // ->where('name', '=', $r->inspectionremarks)
        // ->get();

        // if($result[0]->remarks_count=="0"){

        //     $remarksid = Remark::insertGetId([
        //         'standard_id'=>$r->standard,
        //         'name' => $r->inspectionremarks,
        //         'created_at' => DB::raw("NOW()")
        //     ]);

        //     for($i=0;$i<count($r->findingtype);$i++){
        //         Deduction::SaveDeduction($remarksid, $r->findingtype[$i]["value"], $r->points[$i]["value"]);
        //     }

        //     return json_encode([
        //         'success' => true,
        //         'message' => 'Inspection Remarks Added Successfully'
        //     ]);

        // }
        // else{

        //     return json_encode([
        //         'success' => false,
        //         'message' => 'Inspection Remarks Already Exist'
        //     ]);

        // }

        $remarksid = Remark::insertGetId([
            'standard_id'=>$r->standard,
            'name' => $r->inspectionremarks,
            'created_at' => DB::raw("NOW()")
        ]);

        for($i=0;$i<count($r->findingtype);$i++){
            Deduction::SaveDeduction($remarksid, $r->findingtype[$i]["value"], $r->points[$i]["value"]);
        }

        return json_encode([
            'success' => true,
            'message' => 'Inspection Remarks Added Successfully'
        ]);

    }

    public function getRemarksInspection(){

        $query = Remark::LoadRemarks();

    	$data = array();
    	foreach($query as $out){

            $obj = new \stdClass;

            $obj->standard = $out->standard_name;
            $obj->remarks = $out->remarks_name;
            $obj->findingtype = $out->finding_name;
            $obj->points = $out->points;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-flat btn-info" data-toggle="modal" data-target="#remarksupdate" value="'. $out->id .'"><i class="fa fa-edit"></i></button>';

    		$data[] = $obj;
    	}

    	$info = new Collection($data);
    	return DataTables::of($info)->rawColumns(['panel'])->make(true);
    }

    public function saveFindingsForInspection(Request $r){
        // return $r->inspectionFinding;

    	FindingType::insert([
            'name' => $r->inspectionFinding,
            'created_at' => date('Y-m-d H:i:s')
        ]);

    	return json_encode([
    		'success' => true,
    		'message' => 'Inspection Finding Type Added Successfully',
    	]);

    }

    public function getFindingsInspection(){
        $query = FindingType::get();

        $data = array();
        foreach($query as $out){
            $obj = new \stdClass;
            $obj->name = $out->name;
            $data[] = $obj;
        }

        $info = new Collection($data);
        return DataTables::of($info)->make(true);
    }

    public function forTblLink(){

        // use App\EagleEye as EagleEye;
        // use App\InspectionComponent as Components;
        // use App\Standard as Standard;
        // use App\Remark;
        // use App\FindingType;
        $area = EagleEye::select('id', 'name')->get();
        $components = Components::select('id', 'name')->get();
        $standard = Standard::select('id', 'name', 'description')->get();
        $remarks = Remark::select('id', 'name')->get();
        $findingType = FindingType::select('id', 'name')->get();

        $query = EagleEye::select('tbl_areas.id as area_id', 'tbl_areas.name as area_name',
                                    'tbl_components.id as component_id', 'tbl_components.name as component_name',
                                    'tbl_standards.id as standard_id', 'tbl_standards.name as standard_name', 'tbl_standards.description as standard_description',
                                    'tbl_remarks.id as remarks_id', 'tbl_remarks.name as remarks_name',
                                    'tbl_findings_type.id as finding_type_id', 'tbl_findings_type.name as finding_type_name')
                ->join('tbl_components', 'tbl_areas.id', '=', 'tbl_components.id')
                ->join('tbl_standards', 'tbl_areas.id', '=', 'tbl_standards.id')
                ->join('tbl_remarks', 'tbl_areas.id', '=', 'tbl_remarks.id')
                ->join('tbl_findings_type', 'tbl_areas.id', '=', 'tbl_findings_type.id')
                ->get();

        // $query = InspectionLink::with('getTblArea', 'getTblComponents', 'getTblStandard', 'getTblRemark', 'getFindingType')
        //                 ->where('tbl_areas.id', '=', $r->id)
        //                 ->get();

        return json_encode([
            'wholeResponse' => $query,
            'tblArea' => $area,
            'components' => $components,
            'standard' => $standard,
            'remarks' => $remarks,
            'findingType' => $findingType,
        ]);

    }

    public function getAreas(Request $r){

        $query = Link::with('getTblArea')
                    ->where('id', '=', $r->areaId)
                    ->get();
        return json_encode([
            'getAllArea' => $query
        ]);
    }

    // public function getComponent($areaId, $remarksId = null){
    public function getComponents(Request $r){

        $query = Link::with('getTblArea', 'getTblComponents', 'getTblRemark')
                        ->where('area_id', '=', $r->areaId)
                        ->where('component_id', '=', $r->componentId)
                        ->where('remarks_id', '=', $r->remarks)
                        ->get();
        if($query){
            return json_encode([
                'areaWithComponent' => $query
            ]);
        }else{
            return json_encode([
                'message' => "There's an error please contact backend devevloper error at parsing id's"
            ]);
        }

    }

    public function GetStandards(){

        $standards = Standard::select('*')->get();

        return json_encode([
            "data"=>$standards
        ]);

    }

    public function LoadFindingTypes(){

        $findingtype = Finding::LoadFindingTypes();

        return json_encode([
            "data"=>$findingtype
        ]);

    }

    public function LoadRemarksInformation(Request $request){

        $remarksinfo = Remark::select(
            DB::raw("tbl_standards.id AS 'standard_id'"),
            DB::raw("tbl_remarks.name AS 'remarks_name'")
        )
        ->leftjoin('tbl_standards', 'tbl_standards.id', '=', 'tbl_remarks.standard_id')
        ->where('tbl_remarks.id', '=', $request->id)
        ->first();

        $deductioninfo = Deduction::GetRemarksDeductionInfo($request->id);

        return json_encode([
            "standard_id"=>$remarksinfo->standard_id,
            "remarks_name"=>$remarksinfo->remarks_name,
            "deductioninfo"=>$deductioninfo
        ]);

    }

    public function UpdateRemarksInformation(Request $request){

        $validation = Deduction::ValidateDeduction($request->id);

        if($validation){

            Remark::where('id', '=', $request->id)
            ->update([
                'standard_id'=>$request->standard,
                'name' => $request->inspectionremarks,
                'updated_at' => DB::raw("NOW()")
            ]);

            if(isset($request->findingtype)){

                for($i=0;$i<count($request->findingtype);$i++){

                    if(isset($request->itemdarray)){

                        Deduction::DeleteDeduction($request->itemdarray[$i]);

                    }
                    else {

                        if($request->updateitems[$i]["value"]!=0){
                            Deduction::UpdateDeduction($request->updateitems[$i]["value"], $request->findingtype[$i]["value"], $request->points[$i]["value"]);
                        }
                        else{
                            Deduction::SaveDeduction($request->id, $request->findingtype[$i]["value"], $request->points[$i]["value"]);
                        }

                    }


                }

            }


            return json_encode([
                "success"=>true,
                "message"=>"Remarks information has been update."
            ]);

        }
        else{

            Remark::where('id', '=', $request->id)
            ->update([
                'standard_id'=>$request->standard,
                'name' => $request->inspectionremarks,
                'updated_at' => DB::raw("NOW()")
            ]);

            if(isset($request->findingtype)){

                for($i=0;$i<count($request->findingtype);$i++){

                    Deduction::SaveDeduction($request->id, $request->findingtype[$i]["value"], $request->points[$i]["value"]);

                }

            }

            return json_encode([
                "success"=>true,
                "message"=>"Remarks information has been update."
            ]);

        }

    }

    //This Function Will Only Separate Comma And Insert It Again
    public function RemarksSeparateComma(){

        $remarks = Remark::GetAllRemarks();

        foreach($remarks as $val){

            $data = explode(',', $val->name);
            for($i=0;$i<count($data);$i++){

                Remark::RemarksSeparateComma($val->standard_id, $data[$i]);

            }

        }

        return json_encode([
            "success"=>true
        ], 200);

    }

}
