<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event_Model as Event;
use Auth;
use DataTables;
use Illuminate\Support\Collection;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        return view('event');

    }

    function LoadEventReason(){

        $event = Event::LoadEventReason();

        $data = array();
        foreach($event as $val){

            $obj = new \stdClass;

            $obj->id = $val->id;
            $obj->reason = $val->reason;
            $obj->color = $val->color;

            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-flat btn-primary" value="'.$val->id.'"><i class="fa fa-edit"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function SaveEventReason(Request $request){

        //Validation
        $validation = Event::ValidateEventReason($request->reason);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Reason information already exist."
            ]);

        }
        else{

            Event::SaveEventReason($request->reason, $request->color);

            return json_encode([
                "success"=>true,
                "message"=>"Reason information has been save."
            ]);

        }

    }

    function LoadEventProfile(Request $request){

        $eventinfo = Event::LoadEventProfile($request->id);

        return json_encode([
            "reason"=>$eventinfo->reason,
            "color"=>$eventinfo->color
        ]);

    }

    function UpdateEventReason(Request $request){

        Event::UpdateEventReason($request->id, $request->reason, $request->color);

        return json_encode([
            "success"=>true,
            "message"=>"Reason information has been update."
        ]);

    }

}
