<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\Inventory_Model as Inventory;
use DataTables;
use Illuminate\Support\Collection;
use App\Main_Model as Main;

class InventoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        return view('inventory');

    }

    function LoadInventoryInformation(){

        $inventory = Inventory::select(
            'id',
            'name',
            'description'
        )
        ->get();

        $data = array();
        foreach($inventory as $val){

            $obj = new \stdClass;

            $obj->name = $val->name;
            $obj->description = $val->description;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'.$val->id .'" data-toggle="modal" data-target="#updateinventory"><i class="fa fa-pencil"></i></button> <button id="btnremove" name="btnremove" class="btn btn-danger btn-flat" title="Remove" value="'.$val->id .'"><i class="fa fa-trash"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function NewInventory(Request $request){

        $user_id = Auth::user()->id;
        
        $validation = Inventory::select(
            DB::raw("COUNT(*) AS 'inventory_count'")
        )
        ->where('name', '=', $request->name)
        ->get();

        if($validation[0]->inventory_count==0){

            Inventory::insert([
                "name"=>$request->name,
                "description"=>$request->description
            ]);

            //Insert System Activity
            Main::insert([
                "user_id"=>$user_id,
                "activity"=>"Add new inventory item.",
                "created_at"=>DB::raw("NOW()")
            ]);

            return json_encode([
                "success"=>true,
                "message"=>"Item Has Been Added."
            ]);


        }
        else{

            return json_encode([
                "success"=>false,
                "message"=>"Item Name Already Exist."
            ]);

        }

    }

    function RemoveInventory(Request $request){

        $user_id = Auth::user()->id;

        Inventory::where('id', '=', $request->id)
        ->delete();

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Delete item inventory.",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Inventory Item Has Been Remove."
        ]);

    }

    function ProfileInventory(Request $request){

        $profileinventory = Inventory::select(
            'name',
            'description'
        )
        ->where('id', '=', $request->id)
        ->get();

        return json_encode([
            "name"=>$profileinventory[0]->name,
            "description"=>$profileinventory[0]->description
        ]);

    }

    function UpdateInventory(Request $request){

        $user_id = Auth::user()->id;

        Inventory::where('id', '=', $request->id)
        ->update([
            "name"=>$request->name,
            "description"=>$request->description,
            "updated_at"=>DB::raw("NOW()")
        ]);

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Update item inventory.",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Inventory Item Has Been Update."
        ]);

    }

}
