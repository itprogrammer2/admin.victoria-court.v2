<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\Kiosk_Model as Kiosk;
use DataTables;
use Illuminate\Support\Collection;

class KioskController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        return view('kiosk');

    }

    function LoadRestrictionsWeb(){

        $restrictions = Kiosk::LoadRestrictionsWeb();

        $data = array();
        foreach($restrictions as $val){

            $obj = new \stdClass;

            $obj->role = $val->role;

            if($val->restriction==1){
                $obj->panel = '<div class="checkbox"><label><input id="web'. $val->id . '" name="chkenabledweb[]" type="checkbox" value="'. $val->id . '" checked> Enabled</label></div>';
            }else{
                $obj->panel = '<div class="checkbox"><label><input id="web'. $val->id . '" name="chkenabledweb[]" type="checkbox" value="'. $val->id . '"> Enabled</label></div>';
            }

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function RestrictionEnabledWeb(Request $request){

         Kiosk::RestrictionEnabledWeb($request->role);

    }

    function RestrictionDisabledWeb(Request $request){

         Kiosk::RestrictionDisabledWeb($request->role);

    }

    function LoadRestrictionsMobile(){

        $restrictions = Kiosk::LoadRestrictionsMobile();

        $data = array();
        foreach($restrictions as $val){

            $obj = new \stdClass;

            $obj->role = $val->role;

            if($val->restriction==1){
                $obj->panel = '<div class="checkbox"><label><input id="mobile'. $val->id . '" name="chkenabledmobile[]" type="checkbox" value="'. $val->id . '" checked> Enabled</label></div>';
            }else{
                $obj->panel = '<div class="checkbox"><label><input id="mobile'. $val->id . '" name="chkenabledmobile[]" type="checkbox" value="'. $val->id . '"> Enabled</label></div>';
            }

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function RestrictionEnabledMobile(Request $request){

        Kiosk::RestrictionEnabledMobile($request->role);

    }

    function RestrictionDisabledMobile(Request $request){

        Kiosk::RestrictionDisabledMobile($request->role);

    }

}
