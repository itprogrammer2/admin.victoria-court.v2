<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Login_Model as Login;

class LoginController extends Controller
{
    
    function index(){

        return view('login');

    }

    function Login(Request $request){

       
        if(Auth::attempt(['username'=>$request->txtusername, 'passwordhash'=>$request->txtpassword])){

            $access = Login::LoginRestriction(Auth::user()->id);

            if($access){

                Session::flash('message', "Successfully login.");
                return redirect('/main'); 

            }
            else{

                Session::flash('message', "You dont have any access here.");
                return redirect()->back();

            }

        }
        else{

            Session::flash('message', "Invalid username or password.");
            return redirect()->back();

        }


    }

    function Logout(){

        Auth::logout();
        return;

    }

}
