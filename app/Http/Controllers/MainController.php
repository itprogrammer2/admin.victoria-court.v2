<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Main_Model as Main;
use DataTables;
use Illuminate\Support\Collection;

class MainController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        return view('main');

    }

    function LoadSystemActivities(){

        $systemactivities = Main::select(
            'systemactivities.id',
            DB::raw("CONCAT(user.lastname, ', ', user.firstname, ' ', user.middlename) AS 'modified_by'"),
            'systemactivities.activity',
            DB::raw("DATE_FORMAT(systemactivities.created_at, '%Y-%m-%d') AS 'created_date'"),
            DB::raw("DATE_FORMAT(systemactivities.created_at, '%h:%i %p') AS 'created_time'")
        )
        ->leftjoin('user', 'user.username', '=', 'systemactivities.user_id')
        ->get();

        $data = array();
        foreach($systemactivities as $val){

            $obj = new \stdClass;

            $obj->activity = $val->activity;
            $obj->modifiedby = $val->modified_by;
            $obj->date = $val->created_date;
            $obj->time = $val->created_time;

            $data[] = $obj;

        }

        $info = new Collection($data);
        
        return Datatables::of($info)->make(true);
        // rawColumns(['panel', 'color'])

    }

}
