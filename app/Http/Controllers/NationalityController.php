<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nationality_Model as Nationality;
use DB;
use DataTables;
use Illuminate\Support\Collection;
use App\Main_Model as Main;

class NationalityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        return view('nationality');

    }

    function LoadNationality(){

        $nationality = Nationality::LoadNationality();

        $data = array();
        foreach($nationality as $val){

            $obj = new \stdClass;

            $obj->id = $val->id;
            $obj->name = $val->name;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'.$val->id .'" data-toggle="modal" data-target="#updatenationality"><i class="fa fa-pencil"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function SaveNationality(Request $request){

        $validation = Nationality::ValidateNationality($request->nationality);
        
        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Nationality Information Already Exist."
            ]);

        }
        else{

            Nationality::SaveNationality($request);

            return json_encode([
                "success"=>true,
                "message"=>"Nationality Information Has Been Save."
            ]);

        }

    }

    function GetNationalityInfo(Request $request){

        $nationality = Nationality::GetNationalityInfo($request);

        return json_encode([
            "nationality"=>$nationality->name
        ]);

    }

    function UpdateNationalityInfo(Request $request){


        Nationality::UpdateNationalityInfo($request);

        return json_encode([
            "success"=>true,
            "message"=>"Nationality Information Has Been Update."
        ]);

    }

}
