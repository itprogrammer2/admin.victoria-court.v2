<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotificationSchedule_Model as Schedule;
use DB;
use DataTables;
use Illuminate\Support\Collection;
use Auth;

class NotificationScheduleController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        $localcode = Schedule::GetLocalCode();

        return view('notificationschedule')
        ->with('local_code', $localcode);

    }

    function LoadGroup(){

        $groups = Schedule::LoadGroup();

        return json_encode([
            "data"=>$groups
        ]);


    }

    function SaveSchedule(Request $request){

        $scheduleid = Schedule::SaveSchedule($request);

        for($i=0;$i<count($request->groups);$i++){

            Schedule::SaveScheduleGroup($scheduleid, $request->groups[$i]["value"]);

        }

        return json_encode([
            "success"=>true,
            "message"=>"Schedule information has been save."
        ]);

    }

    function LoadSchedules(){

        $schedules = Schedule::LoadSchedules();

        $data = array();
        foreach($schedules as $val){

            $obj = new \stdClass;

            $obj->name = $val->schedule_name;
            $obj->description = $val->description;
            $obj->operation = $val->operation;
            $obj->time = $val->schedule_time;
            $obj->day = $val->day;
            $obj->createdat = $val->created_at;
            $obj->panel = "<button id='btnedit' name='btnedit' class='btn btn-flat btn-success' value='". $val->id ."' title='Edit Information'><i class='fa fa-pencil-square-o'></i></button> <button id='btndelete' name='btndelete' class='btn btn-flat btn-danger' value='". $val->id ."' title='Delete Schedule'><i class='fa fa-trash'></i></button>";

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);


    }

    function LoadScheduleInformation(Request $request){

        $scheduleinformation = Schedule::LoadScheduleInformation($request);
        $schedulegroup = Schedule::LoadScheduleGroup($request);

        return json_encode([
            "name"=>$scheduleinformation->schedule_name,
            "description"=>$scheduleinformation->description,
            "operation"=>$scheduleinformation->operation,
            "time"=>$scheduleinformation->schedule_time,
            "mon"=>$scheduleinformation->mon,
            "tue"=>$scheduleinformation->tue,
            "wed"=>$scheduleinformation->wed,
            "thu"=>$scheduleinformation->thu,
            "fri"=>$scheduleinformation->fri,
            "sat"=>$scheduleinformation->sat,
            "sun"=>$scheduleinformation->sun,
            "group"=>$schedulegroup
        ]);

    }

    function UpdateSchedule(Request $request){

        //Update Schedule Information
        Schedule::UpdateSchedule($request);

        //Delete Group Schedule
        Schedule::DeleteGroupSchedule($request);
        //Save Group Schedule
        for($i=0;$i<count($request->groups);$i++){

            Schedule::SaveScheduleGroup($request->id, $request->groups[$i]["value"]);

        }

        return json_encode([
            "success"=>true,
            "message"=>"Schedule information has been update."
        ]);

    }

    function RemoveSchedule(Request $request){

        //Delete Schedule Information
        Schedule::DeleteSchedule($request);

        //Delete Group Schedule
        Schedule::DeleteGroupSchedule($request);

        return json_encode([
            "success"=>true,
            "message"=>"Schedule information has been remove."
        ]);

    }

}
