<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotificationModel;
use DB;
use DataTables;
use Illuminate\Support\Collection;
use Auth;

class NotificationsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function add(){
    	return view('notificationadd');
    }

    // get roles
    public function getRoles(){

    	$query = DB::connection('rmsnew')
    	->table('roles')
    	->get();

    	return response()->json([
    		'response' => $query
    	], 200);

    }

    // get added roles
    public function getAdded(){
    	return $query = NotificationModel::getRolesAdded();
    }

    // add roles
    public function addRole(Request $r){
    	// return "hello";
    	//validate first if exist
    	$validate = $r->validate([
    		'role_id' => 'required|unique:rmsnew.notification_blocklists'
    	]);

    	//save
    	$query = NotificationModel::addNotificationBlockList($r);

    	if($query){
    		return response()->json([
    			'message' => 'Successfully Added',
    			'response' => $query
    		], 200);
    	}

    }

    // delete role
    public function removeRole(Request $r){

        // return $r->data['id'];
        $id = $r->data;

        $query = NotificationModel::deleteRoles($id);

        if($query){
            return response()->json([
                'query' => $query,
                'message' => 'Successfully Deleted'
            ], 200);
        }else{
            return response()->json([
                'query' => $query,
                'message' => 'Error on deleting'
            ], 403);
        }

    }

    // group add
    public function groupadd(){
    	return view('notificationgroupadd');
    }

    public function groupadd2(Request $r){
        
        $validation = NotificationModel::ValidateGroupName($r);

        if($validation){

            return json_encode([
                "success"=>false
            ]);
 
        }
        else{

            $query = NotificationModel::createGroup($r);

            if($query){

                return response()->json([
                    'success'=>true,
                    'message' => 'Successfull Added',
                    'response' => $query
                ], 200);

            }

        }

    	
    }

    //get Group Added
    public function getGroup(){

        return $query = NotificationModel::getGroup();
        
    }

    // delete group
    public function removegroup(Request $r){
        
        $id = $r->data;

        $query = NotificationModel::deletegroup($id);

        if($query){
            return response()->json([
                'message' => 'Successfully Deleted',
                'response' => $query
            ], 200);
        }

    }

    public function membershipadd(){

        return view('notificationmembershipadd');

    }

    public function matinonagroup(){
        $query = NotificationModel::getGroupMatino();

        if($query){
            return response()->json([
                'query' => $query,
                'message' => 'Sucessful'
            ], 200);
        }
    }

    public function getMemberInfo(){
        $query = NotificationModel::getMemberInfo();

        if($query){
            return response()->json([
                'query' => $query
            ], 200);
        }else{
            return response()->json([
                'query' => array()
            ], 403);
        }
    }

    public function getRolesMembership(){
        $query = NotificationModel::membershipRoles();

        if($query){
            return response()->json([
                'query' => $query
            ], 200);
        }else{
            return response()->json([
                'query' => $query
            ], 403);
        }
    }

    public function memberadd(Request $r){
        $query = NotificationModel::addMemberAdd($r);

        if($query){
            return response()->json([
                'query' => $query,
                'message' => 'Successful'
            ], 200);
        }else{
            return response()->json([
                'query' => $query,
                'message' => 'Failed'
            ], 403);
        }
    }

    function LoadGroupInformation(){

        $groupinformation = NotificationModel::LoadGroupInformation();

        $data = array();
        foreach($groupinformation as $val){

            $obj = new \stdClass;

            $obj->group = $val->name;
            $obj->created_at = $val->created_at;
            $obj->panel = '<button id="btngroupinfo" name="btngroupinfo" class="btn btn-success btn-flat" title="Group Information" value="'.$val->id .'"><i class="fa fa-pencil"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadGroupData(Request $request){

        $groupdata = NotificationModel::LoadGroupData($request);

        return json_encode([
            "data"=>$groupdata
        ]);

    }

    function LoadRoles(){

        $roles = NotificationModel::LoadRoles();

        return json_encode([
            "data"=>$roles
        ]);

    }

    function AddRoles(Request $request){

        $validation = NotificationModel::ValidateGroupRole($request);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"This role already exist in this group."
            ]);

        }
        else{

            NotificationModel::AddRoles($request);

            return json_encode([
                "success"=>true,
                "message"=>"Role has been added."
            ]);

        }

    }

    function RemoveRoles(Request $request){

        NotificationModel::RemoveRoles($request);

        return json_encode([
            "success"=>true,
            "message"=>"Role has been remove."
        ]);

    }

}
