<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room_Model as Room;
use App\RoomArea;
use App\RoomType;
use Auth;
use DataTables;
use DB;
use App\Main_Model as Main;
use Illuminate\Support\Collection;
use Storage;
use App\RMSLogs_Model AS RMSLogs;

class RoomController extends Controller
{
    
    private $overridestatus = 54;

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index($view=null, $id=null){

        if($view!=null && $id!=null){
            return view('extensions.room.'.$view)->with('id', $id);
        }
        else if($view!=null && $id==null){
            return view('extensions.room.'.$view);
        }
        else{
            return view('rooms');
        }

    }

    function LoadRoomInformation(Request $request){

        $roomgroup = Room::GetRoomGroup();
 
        if(isset($request->search)){

            $room = Room::select(
                'tblroom.id',
                'tblroom.room_no',
                'tblroom.room_type_id'
            )
            ->orderBy('tblroom.room_area_id', 'asc')
            ->orderBy('tblroom.room_type_id', 'asc')
            ->whereRaw("room_no LIKE '%". $request->search ."%'")
            ->get();


        }
        else{

            $room = Room::select(
                'tblroom.id',
                'tblroom.room_no',
                'tblroom.room_type_id'
            )
            ->orderBy('tblroom.room_area_id', 'asc')
            ->orderBy('tblroom.room_type_id', 'asc')
            ->get();

        }

        return view('extensions.room.roomstable')->with('roomgroup', $roomgroup)->with('room', $room);

    }

    function LoadRoomProfile(Request $request){

        $roomprofile = Room::select(
            'tblroom.room_no',
            'tblroom.room_name',
            'tblroom.room_description',
            'tblroomtype.id'
        )
        ->leftjoin('tblroomtype', 'tblroomtype.id', '=', 'tblroom.room_type_id')
        ->where('tblroom.id', '=', $request->id)
        ->get();

        $roomcleaninfo = Room::LoadRoomCleanInfo($request->id);

        return json_encode([
            "data"=>$roomprofile,
            "datacleaninfo"=>$roomcleaninfo
        ]);

    }

    function UpdateRoomDetails(Request $request){

        $user_id = Auth::user()->id;

        //Update Room Info
        Room::where('id', '=', $request->id)
        ->update([
            "room_name"=>$request->roomname,
            "room_description"=>$request->roomdescription,
            "room_type_id"=>$request->roomtype,
            "updated_at"=>DB::raw("NOW()")
        ]);

        //Update Room Cleaning Information Regular
        //Validation
        $valregclean = Room::ValidateCleanInfo($request->id, $request->regpointid);

        if($valregclean){

            //Update Data
            Room::UpdateCleanInfo($request->id, $request->regpointid, $request->regpoints, $request->reghours, $request->regmins);

        }
        else{

            //Insert Data
            Room::InserCleanInfo($request->id, $request->regpointid, $request->regpoints, $request->reghours, $request->regmins);

        }

        //Update Room Cleaning Information General Cleaning
        //Validation
        $valgenclean = Room::ValidateCleanInfo($request->id, $request->genpointid);
        if($valgenclean){

            //Update Data
            Room::UpdateCleanInfo($request->id, $request->genpointid, $request->genpoints, $request->genhours, $request->genmins);

        }
        else{

            //Insert Data
            Room::InserCleanInfo($request->id, $request->genpointid, $request->genpoints, $request->genhours, $request->genmins);

        }
        
        //Insert System Activity
        Main::insert([
           "user_id"=>$user_id,
           "activity"=>"Room ". $request->roomno ." Details Has Been Update.",
           "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Room Details Has Been Update."
        ]);

    }

    function LoadINVItems(){

        $roominventory = Room::LoadINVItems();

        return json_encode([
            "data"=>$roominventory
        ]);

    }

    function LoadRoomInventory(Request $request){

        $roominventory = Room::LoadRoomInventory($request->id);

        $data = array();
        foreach($roominventory as $val){

            $obj = new \stdClass;

            $obj->itemname = $val->name;
            $obj->itemdescription = $val->description;
            $obj->itemcode = $val->itemcode;
            $obj->brand = $val->brand;
            $obj->branddescription = $val->brand_description;
            $obj->serialNumber = $val->serialNumber;
            $obj->trackNumber = $val->trackNumber;
            $obj->differentiateValue = $val->differentiateValue;
            $obj->actualCost = $val->actualCost;
            $obj->qty = $val->quantity;
            $obj->panel = '<button id="btninvremove" name="btninvremove" class="btn btn-danger btn-flat" title="Remove" value="'.$val->id .'" style="font-size: 10px;"><i class="fa fa-trash"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function AddRoomInventory(Request $request){

        $user_id = Auth::user()->id;

        $roomno = Room::select(
            'room_no'
        )
        ->where('id', '=', $request->id)
        ->get();

        $validation = Room::ValidateRoomInventory($request);


        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Inventory Item Already Exist."
            ]);

        }
        else{

            Room::AddRoomInventory($request);

            //Insert System Activity
            Main::insert([
                "user_id"=>$user_id,
                "activity"=>"Add a new inventory item list ".$roomno[0]->room_no.".",
                "created_at"=>DB::raw("NOW()")
            ]);

            return json_encode([
                "success"=>true,
                "message"=>"Inventory Item Has Been Added."
            ]);


        }

    }

    function RemoveRoomInventory(Request $request){

        $user_id = Auth::user()->id;

        $roomno = Room::select(
            'room_no'
        )
        ->where('id', '=', DB::raw("(SELECT room_id FROM roominventorylist WHERE id='".$request->id."')"))
        ->get();

        Room::RemoveRoomInventory($request->id);

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Remove a new inventory item list ".$roomno[0]->room_no.".",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Inventory Item Has Been Remove."
        ]);

    }

    function UploadRoomImage(Request $request){

        if($request->hasFile('uploadimage')){
            
            $user_id = Auth::user()->id;

            $roomno = Room::select(
                'room_no'
            )
            ->where('id', '=', $request->txtuploadid)
            ->get();

            //Storage::putFile('public/images', $request->file('image'));
            $request->uploadimage->storeAs('public', $request->uploadimage->getClientOriginalName());
            Room::UploadRoomImage($request->txtuploadid, $request->uploadimage->getClientOriginalName());

            //Insert System Activity
            Main::insert([
                "user_id"=>$user_id,
                "activity"=>"Upload a new image in ".$roomno[0]->room_no.".",
                "created_at"=>DB::raw("NOW()")
            ]);

            return json_encode([
                "success"=>true,
                "message"=>"Successfully Upload Image."
            ]);

        }

    }

    function LoadRoomImages(Request $request){

        $roomimage = Room::LoadRoomImages($request->id);

        return view('extensions.room.roomimage')->with('roomimage', $roomimage);

    }

    function RemoveRoomImage(Request $request){

        $user_id = Auth::user()->id;
        $filename = Room::GetRoomImage($request->imageid);

        $roomno = Room::select(
            'room_no'
        )
        ->where('id', '=', DB::raw("(SELECT room_id FROM roomimages WHERE id='".$request->imageid."')"))
        ->get();

        Room::RemoveRoomImage($request->imageid);

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Remove a image in ".$roomno[0]->room_no.".",
            "created_at"=>DB::raw("NOW()")
        ]);

        unlink(storage_path('app/public/'.$filename));

        return json_encode([
            "success"=>true,
            "message"=>"Successfully Remove Image."
        ]);

    }

    function LoadStatusOveride(){

        $statusoveride = Room::LoadStatusOveride();

        return json_encode([
            "data"=>$statusoveride
        ]);

    }

    function OverideRoomStatus(Request $request){

        $user_id = Auth::user()->id;

        $prevstatus = Room::select(
            'room_no',
            'room_status_id'
        )
        ->where('id', '=', $request->id)
        ->get();

        if($request->overidestatusid==0){

            //Update RMS
            DB::connection('rmsnew')
            ->table('tblroom')
            ->where('id', '=', $request->id)
            ->update([
                "from_userinfo"=>$user_id,
                "updated_at"=>DB::raw("NOW()")
            ]);

            //Update HMS
            DB::connection('hms')
            ->table('tblroom')
            ->whereRaw("RoomNo = (SELECT room_no FROM rms.tblroom WHERE id='".$request->id."')")
            ->update([
                "Stat"=>"WELCOME",
                "CRoom_Stat"=>'2'
            ]);

        }
        else if($request->overidestatusid==1){

            //Update RMS
            DB::connection('rmsnew')
            ->table('tblroom')
            ->where('id', '=', $request->id)
            ->update([
                "from_userinfo"=>$user_id,
                "updated_at"=>DB::raw("NOW()")
            ]);

            //Update HMS
            DB::connection('hms')
            ->table('tblroom')
            ->whereRaw("RoomNo = (SELECT room_no FROM rms.tblroom WHERE id='".$request->id."')")
            ->update([
                "Stat"=>"CLEAN",
                "CRoom_Stat"=>$request->overidestatusid
            ]);

        }
        else if($request->overidestatusid==2){

            //Update RMS
            DB::connection('rmsnew')
            ->table('tblroom')
            ->where('id', '=', $request->id)
            ->update([
                "from_userinfo"=>$user_id,
                "updated_at"=>DB::raw("NOW()")
            ]);

            //Update HMS
            DB::connection('hms')
            ->table('tblroom')
            ->whereRaw("RoomNo = (SELECT room_no FROM rms.tblroom WHERE id='".$request->id."')")
            ->update([
                "Stat"=>"OCCUPIED",
                "CRoom_Stat"=>$request->overidestatusid
            ]);

        }
        else{

            //Update RMS
            DB::connection('rmsnew')
            ->table('tblroom')
            ->where('id', '=', $request->id)
            ->update([
                "from_userinfo"=>$user_id,
                "updated_at"=>DB::raw("NOW()")
            ]);

            //Update HMS
            DB::connection('hms')
            ->table('tblroom')
            ->whereRaw("RoomNo = (SELECT room_no FROM rms.tblroom WHERE id='".$request->id."')")
            ->update([
                "Stat"=>"DIRTY",
                "CRoom_Stat"=>$request->overidestatusid
            ]);

        }

        //RMSLogs
        $validatelogs = RMSLogs::ValidateRMSLogs($request->id);
        if($validatelogs){
            
            //Close RMS Logs Status
            RMSLogs::UpdateRMSLogs($this->overridestatus, $request->id);

        }

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Status has been overide in ".$prevstatus[0]->room_no.".",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Room Status Has Been Overide."
        ]);

    }

    function LoadRoomRates(Request $request){

        $roomrates = Room::LoadRoomRates($request->id);

        $data = array();
        foreach($roomrates as $val){

            $obj = new \stdClass;

            $obj->id = $val->ID;
            $obj->description = $val->RateDesc;
            $obj->count = $val->rate_count;
            $obj->lastupdate = $val->last_updated;

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->make(true);

    }

    function loadRoomSelection(){

        $RA = RoomArea::get();
        $RT = RoomType::get();
        return response()->json([
            'roomArea' => $RA,
            'roomType' => $RT,
        ]);

    }

    function addRooms(Request $r){

        $add = new Room;
        $add->room_no = $r->roomNo;
        $add->room_name = $r->roomName;
        $add->room_description = $r->roomDesc;
        $add->room_area_id = $r->roomArea;
        $add->room_type_id = $r->roomType;
        $add->save();

        return response()->json([
            'addRooms'=>$add,
            'success'=>true,
            'message'=>'Room Information Has Been Save.'
        ], 200);

    }

    function LoadRoomTypes(){

        $roomtypes = Room::LoadRoomTypes();

        $data = array();
        foreach($roomtypes as $val){

            $obj = new \stdClass;

            $obj->roomtype = $val->room_type;
            $obj->panel = '<button id="btnroomtypeedit" name="btnroomtypeedit" class="btn btn-flat btn-success" value="'. $val->id .'"><i class="fa fa-edit"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function SaveRoomTypeInformation(Request $request){

        $validation = Room::CheckRoomType($request);

        if($validation){

            Room::SaveRoomTypeInformation($request);

            return json_encode([
                "success"=>true,
                "message"=>"Room Type Information Has Been Save."
            ]);

        }
        else{

            return json_encode([
                "success"=>false,
                "message"=>"Room Type Information Already Exist."
            ]);

        }
        

    }

    function LoadGroupRoomType(){

        $grouproomtypes = Room::LoadGroupRoomType();

        $data = array();
        foreach($grouproomtypes as $val){

            $obj = new \stdClass;

            $obj->groupname = $val->group_name;
            $obj->roomtype = $val->room_types;
            $obj->panel = '<button id="btnupdategrouproomtype" name="btnupdategrouproomtype" class="btn btn-flat btn-success" data-toggle="modal" data-target="#updategrouproomtype" value="'. $val->group_name .'"><i class="fa fa-edit"></i></button> <button id="btndeletegrouproomtype" name="btndeletegrouproomtype" class="btn btn-flat btn-danger" value="'. $val->group_name .'"><i class="fa fa-trash"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function GetRoomTypeInformation(Request $request){

        $roomtype = Room::GetRoomTypeInformation($request);

        return json_encode([
            "roomtype"=>$roomtype
        ]);

    }

    function UpdateRoomType(Request $request){

        Room::UpdateRoomType($request);

        return json_encode([
            "success"=>true,
            "message"=>"Room Type Information Has Been Update."
        ]);

    }

    function LoadAutoGroupRoomType(){

        $grouptype = Room::LoadAutoGroupRoomType();

        return json_encode([
            "data"=>$grouptype
        ]);

    }

    function LoadRTypes(){

        $roomtype = Room::LoadRTypes();

        return json_encode([
            "data"=>$roomtype
        ]);

    }

    function SaveGroupRoomTypeInformation(Request $request){

        $validation = Room::ValidateGroupRoomType($request->groupname);

        if($validation){

            for($i=0;$i<count($request->roomtypes);$i++){

                Room::SaveGroupRoomTypeInformation($request->groupname, $request->roomtypes[$i]["value"]);
    
            }
    
            return json_encode([
                "success"=>true,
                "message"=>"Group Room Type Information Has Been Save."
            ]);

        }
        else{

            return json_encode([
                "success"=>false,
                "message"=>"Group Room Type Information Already Exist."
            ]);

        }


    }

    function DeleteGroupRoomTypeInformation(Request $request){

        Room::DeleteGroupRoomTypeInformation($request);

        return json_encode([
            "success"=>true,
            "message"=>"Group Room Type Information Has Been Remove."
        ]);

    }

    function LoadGroupRoomTypeItems(Request $request){

        $grouproomtypeitems = Room::LoadGroupRoomTypeItems($request);

        return json_encode([
            "data"=>$grouproomtypeitems
        ]);

    }

    function UpdateGroupRoomTypeInformation(Request $request){

        Room::DeleteGroupRoomTypeInformation($request);
        for($i=0;$i<count($request->roomtypes);$i++){

            Room::SaveGroupRoomTypeInformation($request->groupname, $request->roomtypes[$i]["value"]);

        }

        return json_encode([
            "success"=>true,
            "message"=>"Group Room Type Information Has Been Update."
        ]);

    }

    function LoadRoomType(){

        $roomtype = Room::LoadRoomType();

        return json_encode([
            "data"=>$roomtype
        ]);

    }

    function SyncInformations(){

        $hmsrooms = Room::GetAllHMSRooms();

        for($i=0;$i<count($hmsrooms);$i++){

            $room_type_id = Room::GetRoomTypeID($hmsrooms[$i]->RoomType);

            Room::SyncInformations($hmsrooms[$i]->RoomNo, $room_type_id);

        }

        return json_encode([
            "success"=>true,
            "message"=>"Room Information Has Been Sync."
        ]);

    }

    function SyncRoomInformations(){

        $dbrms = env("DB_RMS");
        $dbhms = env("DB_HMS");

        $rooms = Room::SyncRoomInformations($dbhms, $dbrms);

        return json_encode([
            "success"=>true,
            "message"=>"Room Information Has Been Sync."
        ]);


    }

    function CheckRoomCount(){

        $roomcount = Room::CheckRoomCount();

        return json_encode([
            "count"=>$roomcount
        ]);

    }

}
