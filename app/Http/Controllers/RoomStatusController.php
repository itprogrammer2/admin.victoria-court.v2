<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;
use Illuminate\Support\Collection;
use App\RoomStatus_Model as RoomStatus;
use DB;
use App\Main_Model as Main;

class RoomStatusController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index($view=null){

        if($view!=null){
            return view('extensions.roomstatus.'.$view);
        }
        else{
            return view('roomstatus');
        }

    }

    function RoomStatusInformation(){

        $roomstatus = RoomStatus::select('*')
                        ->get();

        $data = array();
        foreach($roomstatus as $val){

            $obj = new \stdClass;


            $obj->roomstatus = $val->room_status;
            $obj->color = '<input type="color" id="body" name="color[]"
            value="'. $val->color .'" disabled/>';
            if($val->is_blink==0){
                $obj->blink = '<div class="checkbox"><label><input id="blink'.$val->id.'" name="chkblink[]" type="checkbox" value="'. $val->id .'"></label> Enabled</div>';
            }
            else{
                $obj->blink = '<div class="checkbox"><label><input id="blink'.$val->id.'" name="chkblink[]" type="checkbox" value="'. $val->id .'" checked> Enabled</label></div>';
            }
            if($val->is_timer==0){
                $obj->timer = '<div class="checkbox"><label><input id="timer'.$val->id.'" name="chktimer[]" type="checkbox" value="'. $val->id .'"></label> Enabled</div>';
            }
            else{
                $obj->timer = '<div class="checkbox"><label><input id="timer'.$val->id.'" name="chktimer[]" type="checkbox" value="'. $val->id .'" checked> Enabled</label></div>';
            }
            if($val->is_name==0){
                $obj->name = '<div class="checkbox"><label><input id="name'.$val->id.'" name="chkname[]" type="checkbox" value="'. $val->id .'"></label> Enabled</div>';
            }
            else{
                $obj->name = '<div class="checkbox"><label><input id="name'.$val->id.'" name="chkname[]" type="checkbox" value="'. $val->id .'" checked> Enabled</label></div>';
            }
            if($val->is_buddy==0){
                $obj->buddy = '<div class="checkbox"><label><input id="buddy'.$val->id.'" name="chkbuddy[]" type="checkbox" value="'. $val->id .'"></label> Enabled</div>';
            }
            else{
                $obj->buddy = '<div class="checkbox"><label><input id="buddy'.$val->id.'" name="chkbuddy[]" type="checkbox" value="'. $val->id .'" checked> Enabled</label></div>';
            }
            if($val->is_cancel==0){
                $obj->close = '<div class="checkbox"><label><input id="cancel'.$val->id.'" name="chkcancel[]" type="checkbox" value="'. $val->id .'"></label> Enabled</div>';
            }
            else{
                $obj->close = '<div class="checkbox"><label><input id="cancel'.$val->id.'" name="chkcancel[]" type="checkbox" value="'. $val->id .'" checked> Enabled</label></div>';
            }

            if($val->has_checklist==0){
                $obj->checklist = '<div class="checkbox"><label><input id="checklist'.$val->id.'" name="chkchecklist[]" data-id="'. $val->id .'" type="checkbox" value="'. $val->id .'"></label> Enabled</div>';
            }
            else{
                $obj->checklist = '<div class="checkbox"><label><input id="checklist'.$val->id.'" name="chkchecklist[]" data-id="'. $val->id .'" type="checkbox" value="'. $val->id .'" checked> Enabled</label></div>';
            }

            // $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" data-toggle="modal" data-target="#uroomstatus" value="'. $val->id .'"><i class="fa fa-edit"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'color', 'blink', 'timer', 'name', 'buddy', 'close', 'checklist'])->make(true);

    }

    function RoomStatusProfile(Request $request){

        $roomstatus =  RoomStatus::select('*')
                        ->where('id', '=', $request->id)
                        ->get();

        return json_encode([
            "room_status"=>$roomstatus[0]->room_status,
            "color"=>$roomstatus[0]->color,
        ]);

    }

    function UpdateRoomStatus(Request $request){

        RoomStatus::where('id', '=', $request->id)
                    ->update([
                        "room_status"=>$request->room_status,
                        "color"=>$request->color,
                        "updated_at"=>DB::raw("NOW()")
                    ]);

        return json_encode([
            "success"=>true,
            "message"=>"Room status information has been update."
        ]);

    }

    function GroupRoomStatusInformation(){

        $roomstatus = RoomStatus::GroupRoomStatusInformation();

        $data = array();
        foreach($roomstatus as $val){

            $obj = new \stdClass;

            $obj->roomstatus = $val->room_status;
            $obj->availablestatus = $val->available_room_status;
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" data-toggle="modal" data-target="#ugrouproomstatus" value="'.$val->id .'"><i class="fa fa-edit"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadRoomStatus(){

        $roomstatus = RoomStatus::select('*')
                                ->get();

        return json_encode([
            "data"=>$roomstatus
        ]);

    }

    function SaveGroupRoomStatus(Request $request){

        $user_id = Auth::user()->id;
        $validate = RoomStatus::ValidateGroupRoomStatus($request->room_status_id);

        if($validate){

            return json_encode([
                "success"=>false,
                "message"=>"This Group Room Status Already Exist."
            ]);

        }
        else{

            for($x=0;$x<count($request->available_status_id);$x++){

                RoomStatus::SaveGroupRoomStatus($request->room_status_id, $request->available_status_id[$x]["value"]);

            }

            //Insert System Activity
            Main::insert([
                "user_id"=>$user_id,
                "activity"=>"Add a new group status.",
                "created_at"=>DB::raw("NOW()")
            ]);

            return json_encode([
                "success"=>true,
                "message"=>"Group Room Status Information Has Been Save."
            ]);

        }

    }

    function LoadRoomStatusProfile(Request $request){

        $roomstatus = RoomStatus::select('room_status')
                                ->where('id', '=', $request->id)
                                ->get();

        $roomprofile = RoomStatus::LoadRoomStatusProfile($request->id);

        return json_encode([
            "roomstatus"=>$roomstatus,
            "roomprofile"=>$roomprofile
        ]);


    }

    function DeleteRoomGroupStatus(Request $request){

        $user_id = Auth::user()->id;

        //Delete Room Status
        RoomStatus::DeleteRoomGroupStatus($request->id);

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Delete a group status.",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Group Room Status Information Has Been Delete."
        ]);

    }

    function UpdateRoomGroupStatus(Request $request){

        $user_id = Auth::user()->id;

        //Delete Information
        if(isset($request->updatedeletearray)){

            RoomStatus::DeleteRoomGroupStatusProfile($request->updatedeletearray);

        }

        //Update Information
        for($i=0;$i<count($request->groupid);$i++){

            if($request->groupid[$i]["value"]!=0){

                RoomStatus::UpdateRoomGroupStatus($request->groupid[$i]["value"], $request->available_status_id[$i]["value"]);

            }
            else{

                RoomStatus::SaveGroupRoomStatus($request->id, $request->available_status_id[$i]["value"]);

            }

        }

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Modified a group status.",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Group Room Status Information Has Been Update."
        ]);

    }

    function EnabledBlink(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_blink"=>"1",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function DisabledBlink(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_blink"=>"0",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function EnabledTimer(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_timer"=>"1",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function DisabledTimer(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_timer"=>"0",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function EnabledName(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_name"=>"1",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function DisabledName(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_name"=>"0",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function EnabledBuddy(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_buddy"=>"1",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function DisabledBuddy(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_buddy"=>"0",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function EnabledCancel(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_cancel"=>"1",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function DisabledCancel(Request $request){

        RoomStatus::where('id', '=', $request->id)
        ->update([
            "is_cancel"=>"0",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    function LoadStandards(){

        $standardinfo = RoomStatus::LoadStandards();

        return json_encode([
            "data"=>$standardinfo
        ]);

    }

    function UpdateForChecklist(Request $request){

        RoomStatus::UpdateForChecklist($request->id);

    }

    function DiUpdateForChecklist(Request $request){

        RoomStatus::DiUpdateForChecklist($request->id);

    }

    function UpdateChecklistSTD(Request $request){

        $data = array();
        for($i=0;$i<count($request->stddata);$i++){

            $data[] = $request->stddata[$i]["value"];

        }

        $stdids = implode(',', $data);

        RoomStatus::UpdateChecklistSTD($request->id, $stdids);

    }

    function ValidateChecklistID(Request $request){

        $validation = RoomStatus::ValidateChecklistID($request->id);

        if($validation){

            return json_encode([
                "success"=>true
            ]);

        }
        else{

            return json_encode([
                "success"=>false
            ]);

        }

    }


}
