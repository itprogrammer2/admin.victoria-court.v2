<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Illuminate\Http\Request;
use App\Setting_Model as Settings;
use DataTables;
use Illuminate\Support\Collection;

class SettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index($view=null, $id=null){

        if($view!=null && $id!=null){

            $group_id = explode(',', $id);

            return view('extensions.settings.'.$view)->with('group_id', $group_id);

        }
        else{
            return view('setting');
        }

    }

    function LoadSettings(){

        $settings = Settings::LoadSettings();

        return json_encode([
            "settings"=>$settings
        ]);

    }

    function SaveSettings(Request $request){

        Settings::SaveSettings($request);

        return json_encode([
            "success"=>true,
            "message"=>"Setting Information Has Been Save."
        ]);

    }

    function LoadRoomTypeGroup(){

        $roomtypes = Settings::LoadRoomTypeGroup();

        $data = array();
        foreach($roomtypes as $val){

            $obj = new \stdClass;

            $obj->roomtype = $val->group_name;
            $obj->panel = '<a href="'. url("/settings/settinggrouproomtype/".$val->group_id) .'" class="btn btn-success btn-flat"><i class="fa fa-pencil"></i></a>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadRoomType(Request $request){

        $roomtypes = Settings::LoadRoomType($request);

        return json_encode([
            "data"=>$roomtypes
        ]);

    }

    function LoadStatusPoints(Request $request){

        $statuspoints = Settings::LoadStatusPoints($request);

        $data = array();
        foreach($statuspoints as $val){

            $obj = new \stdClass;

            $obj->status = $val->room_status;
            $obj->points = $val->points;
            $obj->panel = '<button id="btnpoints" name="btnpoints" value="'. $val->id .'" class="btn btn-flat btn-success" data-toggle="modal" data-target="#points"><i class="fa fa-pencil"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadStatusName(Request $request){

        $status = Settings::LoadStatusName($request);

        return json_encode([
            "status"=>$status
        ]);

    }

    function SavePointsInformation(Request $request){

        $validation = Settings::CheckPointsInformation($request);

        if($validation){

            Settings::InsertPointsInformation($request);

            return json_encode([
                "success"=>true,
                "message"=>"Point Information Has Been Save."
            ]);

        }
        else{

            Settings::UpdatePointInformation($request);

            return json_encode([
                "success"=>true,
                "message"=>"Point Information Has Been Update."
            ]);

        }

    }

    function LoadPoints(Request $request){

        $points = Settings::LoadPoints($request);

        return json_encode([
            "points"=>$points
        ]);

    }

    function LoadScoreRating(){

        $scorerating = Settings::LoadScoreRating();

        $data = array();
        foreach($scorerating as $val){

            $obj = new \stdClass;

            $obj->id = $val->id;
            $obj->score = $val->score;
            $obj->rating = $val->rating;
            $obj->panel = '<button id="btnupdaterating" name="btnupdaterating" class="btn btn-flat btn-success" value="'.$val->id.'"><i class="fa fa-edit"></i></button> <button id="btndelete" name="btndelete" class="btn btn-flat btn-danger" value="'.$val->id.'"><i class="fa fa-trash"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function SaveNewRating(Request $request){

        Settings::SaveNewRating($request);

        return json_encode([
            "success"=>true,
            "message"=>"Rating information has been save."
        ]);

    }

    function LoadRatingInformation(Request $request){

        $ratinginfo = Settings::LoadRatingInformation($request);

        return json_encode([
            "score_from"=>$ratinginfo->score_from,
            "score_to"=>$ratinginfo->score_to,
            "rating"=>$ratinginfo->rating
        ]);

    }

    function UpdateRating(Request $request){

        Settings::UpdateRating($request);

        return json_encode([
            "success"=>true,
            "message"=>"Rating information has been update."
        ]);

    }

    function DeleteRating(Request $request){

        Settings::DeleteRating($request);

        return json_encode([
            "success"=>true,
            "message"=>"Rating information has been remove."
        ]);

    }

    function LoadRoomBalancer(){

        $roombalancer = Settings::LoadRoomBalancer();

        $data = array();
        foreach($roombalancer as $val){

            $obj = new \stdClass;

            $obj->order = $val->order;
            $obj->groupname = $val->group_name;
            $obj->percentage = $val->percentage;
            $obj->panel = '<button id="btnremovegrpbalancer" name="btnremovegrpbalancer" class="btn btn-danger" title="Delete" value="'. $val->id .'"><i class="fa fa-trash" aria-hidden="true"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadGRPType(){

        $content = "";
        $grpinfo = Settings::LoadGRPType();

        foreach($grpinfo as $val){

            $content .= '<option value="'. $val->id .'">'. $val->room_type .'</option>';

        }

        return json_encode([
            "content"=>$content
        ]);

    }

    function SaveRoomTypePercentage(Request $request){

        //Validation Order
        $valorder = Settings::OrderValidation($request->order);

        if($valorder){

            return json_encode([
                "success"=>false,
                "message"=>"Order already exist."
            ]);

        }
        else{

            //Validation Room Type
            $valroomtype = Settings::RoomTypeValidation($request->grpid);

            if($valroomtype){

                return json_encode([
                    "success"=>false,
                    "message"=>"Room type already exist."
                ]);

            }
            else{

                Settings::SaveRoomTypePercentage($request->grpid, $request->percentage, $request->order);

                return json_encode([
                    "success"=>true,
                    "message"=>"Sucessfully save the group percentage"
                ]);

            }

        }

    }

    function DeleteRoomTypePercentage(Request $request){

        $id = explode(',', $request->id);

        for($i=0;$i<count($id);$i++){

            Settings::DeleteRoomTypePercentage($id[$i]);

        }

        return json_encode([
            "success"=>true,
            "message"=>"Sucessfully delete the group percentage"
        ]);

    }

}
