<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Transaction;

use DB;
use App\AccessLevel as AccessLevel;
use App\LoginMobile as LoginMobile;
use App\InspectionComponent as InspectionComponent;
use App\Standard as Standard;
use App\InspectionLink;
use App\HMSTableRoom;

class TransactionController extends Controller
{
    public function saveInspection(Request $request){
    	
		$this->rule = array(
			'room_no' => 'required|integer',
			'user_id' => 'required|integer',
			'new_room_status' => 'required|integer'
		);
		
    	$this->validate($request->all(), $this->rule);
    	if (!$this->data['error']) {
			$remarks = $this->fixRemarksData($request);
    		try {
				$connection = DB::connection('hms');
    			$connection->transaction(function () use($connection, $request, $remarks) {
    				$connection->table('hms.tblroom')
						->where('RoomNo' ,'=', $request->room_no)
    					->update(array('CRoom_Stat' => $request->new_room_status)
					);
					$connection->table('rmsnew.tbl_transactions')
						->insert($remarks);
				});
	    		$this->data['response'] = array('message'=>'Inspection successfully saved');
    		} catch (\Exception $e) {
    			$this->data['error'] = array($e->getMessage());
				$this->statusCode = 403;
    		}
    	}
    	return response()->json($this->data, $this->statusCode);
    }

    private function fixRemarksData($request) {
    	$remarks = array();
		if (!empty($request->remarks)) { 
			$allRemarks = json_decode($request->remarks, true);
			foreach ($allRemarks as $key => $value) {
				array_push($remarks, $value);
			}
		} else {
			array_push($remarks, array(
				'links_id'=> 0,
				'user_id' => $request->user_id,
				'room_no' => $request->room_no));
		}
		return $remarks;
    }

    private function isRoomExisting($room_no) {
    	$validRoomTemp = DB::connection('hms')
    		->table('tblroom')
    		->select(DB::raw("COUNT(*) AS 'room_count'"))
    		->where('RoomNo','=',$room_no)
    		->first()->room_count;
		return $validRoomTemp == 1;
    }

    public function changeRoomStatus(Request $request) {
    	$this->rule = array(
			'room_no' => 'required|integer',
			'new_room_status' => 'required|integer',
			'current_room_status' => 'required|integer'
		);
    	$this->validate($request->all(), $this->rule);
    	if (!$this->data['error']) {
    		if ($this->isRoomExisting($request->room_no)) {
    			// return $this->isAllowedToChange($request->role_id, $request->current_room_status);
    			if ($this->isAllowedToChange($request->role_id, $request->current_room_status)) {
    				try {
			    		$connection = DB::connection('hms');
						$connection->transaction(function () use($connection, $request) {
							$connection->table('hms.tblroom')
								->where('RoomNo' ,'=', $request->room_no)
								->update(array('CRoom_Stat' => $request->new_room_status)
							);
						});
						$this->data['response'] = array('message'=>'Room status changed');
			    	} catch (Exception $e) {
						$this->data['error'] = array($e->getMessage());
						$this->statusCode = 403;	
			    	}	
    			} else {
    				$this->data['error'] = array("Room status for inspection only");
					$this->statusCode = 403;	
    			}
    			
    		} else {
    			$this->data['error'] = array("Room not existing or not allowed for inspection");
				$this->statusCode = 403;	
    		}
    	}
    	return response()->json($this->data, $this->statusCode);
    }

    public function login(Request $request){
    	$this->rule = array(
			'username' => 'required',
			'password' => 'required'
		);
    	$this->validate($request->all(), $this->rule);
    	if (!$this->data['error']) {
    		$login = LoginMobile::where('username', '=', $request->username)
        					->where('password', '=', $request->password)
        					->select('*')->first();
			$this->data['response'] = $login;
    	}
        return response()->json($this->data, $this->statusCode);
    }

    private function isAllowedToChange($role_id, $room_status) {
		$response = AccessLevel::where('role_id', '=', $role_id)
			->Where('room_status_id', '=', $room_status)->first();
    	return $response;
    }

    public function endStatus(Request $request) {
    	$this->rule = array(
			'role_id' => 'required|integer',
			'current_room_status' => 'required|integer'
		);
    	$this->validate($request->all(), $this->rule);
    	if (!$this->data['error']) {

    		$this->data['response'] = AccessLevel::where('role_id', '=', $request->role_id)
			->Where('room_status_id', '=', $request->current_room_status)
			->join('roomstatus', 'allow_status_id', '=', 'roomstatus.id')
			->get();
    	}
        return response()->json($this->data, $this->statusCode);
    }

    public function checklist(Request $request) {

    	$query = Standard::get();

    	foreach($query as $data){

    		$area =  InspectionLink::distinct()->with('area')->where('standard_id', $data->id)->get(['area_id']);

    		$test2 = [];

    		foreach($area as $data1){
    			

    			$component =  InspectionLink::distinct()->with('component')
    				->where('standard_id', $data->id)
    				->where('area_id', $data1->area_id)
    				->get(['component_id']);

    				$test3 = [];

    				foreach($component as $data2){

    					$remarks =  InspectionLink::distinct()->with('remarks')
		    				->where('standard_id', $data->id)
		    				->where('area_id', $data1->area_id)
		    				->where('component_id', $data2->component_id)
		    				->get(['remarks_id','id']);

		    				$test4 = [];

		    				foreach($remarks as $data3){

		    					$test4[] = array(
				    				'id' => $data3->id,
				    				'name' => $data3->remarks->name
				    			);
		    				}


    					$test3[] = array(
		    				'name' => $data2->component->name,
		    				'remarks' =>$test4
		    			);
    				}

    			$test2[] = array(
    				'name' => $data1->area->name,
    				'component' => $test3
    			);

    			$test3 = [];
    		}

    		if(count($area) != 0){
    			$test1[] = array(
	    			'name' => $data->name,
	    			'area' => $test2
	    		);
    		}
    	}

    	return $test1;

    	// $data1 = [];

    	// foreach($query as $data){
    	// 	$data1[$data->id] = InspectionLink::where('standard_id', $data->id)->get();
    	// }

    	// foreach($query as $data){
    	// 	return $data1[$data->id];
    	// }
    	$this->rule = array(
			'role_id' => 'required|integer',
			'current_room_status' => 'required|integer'
		);
    	// $this->validate($request->all(), $this->rule);
   //  	if (!$this->data['error']) {
   //  		$myChecklistData = array(); 
    		$connection = DB::connection('rmsnew');
			// $allChecklistData = $connection->table('tbl_links')
			// 	->select('tbl_areas.name as area_name', 
			// 			 'tbl_components.name as component_name',
			// 			 'tbl_standards.name as standard_name',
			// 			 'tbl_remarks.name as remark_name')
			// 	->join('tbl_areas', function ($join) {
		 //            $join->on('tbl_links.area_id', '=', 'tbl_areas.id');
		 //        })
		 //        ->join('tbl_components', function ($join) {
		 //            $join->on('tbl_links.component_id', '=', 'tbl_components.id');
		 //        })
		 //        ->join('tbl_standards', function ($join) {
		 //            $join->on('tbl_links.standard_id', '=', 'tbl_standards.id');
		 //        })
		 //        ->join('tbl_remarks', function ($join) {
		 //            $join->on('tbl_links.remarks_id', '=', 'tbl_remarks.id');
		 //        })
		 //        ->get(); 
	        $tmp = array();
			foreach($allChecklistData as $key => $value)
			{


			    // if (!array_key_exists('checklist', $tmp)) {
			    // 	$tmp['checklist']['name'] = array();
			    // }

			    // if (!in_array($value->standard_name, $tmp['checklist']['name'])) {
			    // 	$tmp['checklist']['name'][] = $value->standard_name;
			    // }


			    // if (!array_key_exists('name', $tmp['checklist'])) {
			    // 	$tmp['checklist']['name'] = array();
			    // }

			    // if (!array_key_exists('title', $tmp['checklist']['name'])) {
			    // 	$tmp['checklist']['name']['title'] = array();
			    // }

			    // if (!array_key_exists('name', $tmp['checklist']['name'])) {
			    // 	$tmp['checklist']['name']['area'] = array();
			    // }

			    // $myArray = array('result'=> array(
			    // 	array(
			    // 		'checklist_name'=> 'Checklist1',
			    // 		'area'=> array('name'=>'CEILING',
			    // 		'components'=>array(array('name'=>"FLOOR", 
			    // 			'remarks'=>array('dirty','clean')
			    // 			))
			    // 		)
			    // 	),
			    // 	array(
			    // 		'checklist_name'=> 'Checklist2',
			    // 		'area'=> array('name'=>'CEILING',
			    // 		'components'=>array(array('name'=>"FLOOR", 
			    // 			'remarks'=>array('dirty','clean')
			    // 			))
			    // 		)
			    // 	)
			    // ));
		    	// echo array_search('Checklist2', array_column($myArray['result'], 'checklist_name')); exit;
			    return $myArray;
			    
				// if (!in_array($value->standard_name, $tmp['checklist']['results'])) {

					// if (!array_key_exists('title', $tmp['checklist']['results'])) {
					// 	array_push($tmp['checklist']['results'], array('title'=>$value->standard_name,'area'=>array('components'=>array('remarks' => array()))));

					// }
					

					// array_push($tmp['checklist']['results'], array('components'=>"comp"));


					// $tmp['checklist']['name']['area'][] = $value->area_name;

			    	// if (!array_key_exists($value->area_name, $tmp[$value->standard_name])) {
			    	// 	$tmp[$value->standard_name][$value->area_name] = array();
			    	// }
			    	// if (!array_key_exists($value->component_name, $tmp[$value->standard_name][$value->area_name])) {
		    		// 	$tmp[$value->standard_name][$value->area_name][$value->component_name] = array();
		    		// }

		    		// if (!array_key_exists('remarks', $tmp[$value->standard_name][$value->area_name][$value->component_name])) {
		    		// 	$tmp[$value->standard_name][$value->area_name][$value->component_name]['remarks'] = array();
		    		// 	array_push($tmp[$value->standard_name][$value->area_name][$value->component_name]['remarks'], $value->remark_name);
		    		// } else {
		    		// 	array_push($tmp[$value->standard_name][$value->area_name][$value->component_name]['remarks'], $value->remark_name);
		    		// }
 
			    	
			    // } else {
			    	// if (!array_key_exists($value->area_name, $tmp[$value->standard_name])) {
			    	// 	$tmp[$value->standard_name][$value->area_name] = array();
			    	// }

			    	// if (!array_key_exists($value->component_name, $tmp[$value->standard_name][$value->area_name])) {
		    		// 	$tmp[$value->standard_name][$value->area_name][$value->component_name] = array();
		    		// }

		    		// if (!array_key_exists('remarks', $tmp[$value->standard_name][$value->area_name][$value->component_name])) {
		    		// 	$tmp[$value->standard_name][$value->area_name][$value->component_name]['remarks'] = array();
		    		// 	array_push($tmp[$value->standard_name][$value->area_name][$value->component_name]['remarks'], $value->remark_name);
		    		// } else {
		    		// 	array_push($tmp[$value->standard_name][$value->area_name][$value->component_name]['remarks'], $value->remark_name);
		    		// }
 

			    // }
			    

			}

			return $tmp;
        	// return $allChecklistData;
    	// }

    	// return response()->json($this->data, $this->statusCode);
    }

    
}
