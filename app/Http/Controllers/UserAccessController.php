<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAccess_Model as UserAccess;
use Auth;
use DB;
use DataTables;
use Illuminate\Support\Collection;
use App\Main_Model as Main;

class UserAccessController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index($view=null, $id=null){

        if($view!=null && $id!=null){

            return view('extensions.useraccess.'.$view)->with('id', $id);

        }
        else{
            return view('useraccess');
        }
       
    }

    function LoadUserRoles(){

        $userrole = UserAccess::LoadUserRoles();

        $data = array();
        foreach($userrole as $val){

            $obj = new \stdClass;

            $obj->id = $val->id;
            $obj->role = $val->role;
            $obj->panel = '<a href="' . url("/useraccess/useraccessprofile/".$val->id) .'"><button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit"><i class="fa fa-pencil"></i></button></a>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadRoleName(Request $request){

        $role = UserAccess::LoadRoleName($request->id);

        return json_encode([
            "role"=>$role[0]->role
        ]);

    }

    function LoadAllowStatus(Request $request){

        $allowroles = UserAccess::LoadAllowStatus($request->id);

        $data = array();
        foreach($allowroles as $val){

            $obj = new \stdClass;

            $obj->status = $val->room_status;
            $obj->allowstatus = $val->allow_status;
            $obj->panel = '<button id="btnremoveroomstatus" name="btnremoveroomstatus" class="btn btn-flat btn-primary" data-toggle="modal" data-target="#useraccessroomstatusinfo" value="'.$val->id.'"><i class="fa fa-edit"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function LoadRoomStatus(){

        $roomstatus = UserAccess::LoadRoomStatus();

        return json_encode([
            "data"=>$roomstatus
        ]);

    }

    function SaveTargetStatusInfo(Request $request){

        $validation = UserAccess::ValidateNextStatus($request);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Next status already exist please select another status."
            ]);

        }
        else{

            UserAccess::SaveTargetStatusInfo($request);

            return json_encode([
                "success"=>true,
                "message"=>"User Room Status Has Been Save."
            ]);

        }

    }

    function LoadAllowStatusProfile(Request $request){

        $status = UserAccess::GetRoomStatus($request->statusid);
        $allowstatus = UserAccess::LoadAllowStatusProfile($request->id, $request->statusid);
        
        return json_encode([
            "status"=>$status[0]->room_status,
            "data"=>$allowstatus
        ]);

    }

    function RemoveAllowStatus(Request $request){

        UserAccess::RemoveAllowStatus($request->id);

        return json_encode([
            "success"=>true,
            "message"=>"Allow Status Has Been Remove."
        ]);

    }

    function AddUserRole(Request $request){

        $validation = UserAccess::ValidateUserRole($request->role);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Role Information Already Exist."
            ]);

        }
        else{

            UserAccess::AddUserRole($request);

            return json_encode([
                "success"=>true,
                "message"=>"Role Information Has Been Save."
            ]);

        }

    }

    function LoadResources(){

        $resources = UserAccess::LoadResources();

        $data = array();
        foreach($resources as $val){

            $obj = new \stdClass;

            $obj->id = $val->id;
            $obj->name = $val->name;
            $obj->description = $val->description;
            $obj->panel = '<button id="btneditresources" name="btneditresources" class="btn btn-flat btn-success" value="'.$val->id.'"><i class="fa fa-pencil"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);

    }

    function SaveResources(Request $request){

        $validation = UserAccess::ValidateResources($request->resources);

        if($validation){

            return json_encode([
                "success"=>false,
                "message"=>"Resources Information Already Exist."
            ]);

        }
        else{

            UserAccess::SaveResources($request);

            return json_encode([
                "success"=>true,
                "message"=>"Resources Information Has Been Save."
            ]);

        }

    }

    function LoadAllowResources(Request $request){

        $resources = UserAccess::LoadAllowResources($request->id);

        $data = array();
        foreach($resources as $val){

            $obj = new \stdClass;

            $obj->resources = $val->name;
            $obj->description = $val->description;

            if($val->permission==1){
                $obj->panel = '<div class="checkbox"><label><input id="'. $val->id . '" name="chkenabled[]" type="checkbox" value="'. $val->id . '" checked> Enabled</label></div>';
            }else{
                $obj->panel = '<div class="checkbox"><label><input id="'. $val->id . '" name="chkenabled[]" type="checkbox" value="'. $val->id . '"> Enabled</label></div>';
            }

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel'])->make(true);


    }

    function ResourcesEnabled(Request $request){

        UserAccess::ResourcesEnabled($request);

    }

    function ResourcesDisabled(Request $request){

        UserAccess::ResourcesDisabled($request);

    }

}
