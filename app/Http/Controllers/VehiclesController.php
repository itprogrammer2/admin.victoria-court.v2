<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use DataTables;
use App\Main_Model as Main;
use Illuminate\Support\Collection;
use App\Vehicle_Model as Vehicle;
use DB;

class VehiclesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    function index(){

        return view('vehicle');

    }

    function LoadVehicleInformation(){

        $inventory = Vehicle::select(
            'id',
            'name',
            'plate_number'
        )
        ->get();

        $data = array();
        foreach($inventory as $val){

            $obj = new \stdClass;

            $obj->name = $val->name;

            if($val->plate_number==1){
                $obj->platenumber = '<i class="fa fa-check"></i>';
            }else{
                $obj->platenumber = '<i class="fa fa-times"></i>';
            }
            
            $obj->panel = '<button id="btnedit" name="btnedit" class="btn btn-success btn-flat" title="Edit Information" value="'.$val->id .'" data-toggle="modal" data-target="#updatevehicle"><i class="fa fa-pencil"></i></button> <button id="btnremove" name="btnremove" class="btn btn-danger btn-flat" title="Remove" value="'.$val->id .'"><i class="fa fa-trash"></i></button>';

            $data[] = $obj;

        }

        $info = new Collection($data);
        return Datatables::of($info)->rawColumns(['panel', 'platenumber'])->make(true);

    }

    function NewVehicle(Request $request){

        $user_id = Auth::user()->id;
       
        $validation = Vehicle::select(
            DB::raw("COUNT(*) AS vehicle_count")
        )
        ->where('name', '=', $request->name)
        ->get();

        if($validation[0]->vehicle_count==0){

             //Insert System Activity
            Main::insert([
                "user_id"=>$user_id,
                "activity"=>"Add vehicle information.",
                "created_at"=>DB::raw("NOW()")
            ]);

            Vehicle::insert([
                "name"=>$request->name,
                "plate_number"=>$request->platenumber,
                "created_at"=>DB::raw("NOW()")
            ]);

            return json_encode([
                "success"=>true,
                "message"=>"Vehicle Information Has Been Added."
            ]);


        }
        else{

            return json_encode([
                "success"=>false,
                "message"=>"Vehicle Information Already Exist."
            ]);

        }
       
    }

    function RemoveVehicle(Request $request){

        $user_id = Auth::user()->id;

        Vehicle::where('id', '=', $request->id)
        ->delete();

        //Insert System Activity
        Main::insert([
           "user_id"=>$user_id,
           "activity"=>"Delete vehicle information.",
           "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Vehicle Information Has Been Remove."
        ]);

    }

    function ProfileVehicle(Request $request){

        $profilevehicle = Vehicle::select(
            'name',
            'plate_number'
        )
        ->where('id', '=', $request->id)
        ->get();

        return json_encode([
           "name"=>$profilevehicle[0]->name,
           "platenumber"=>$profilevehicle[0]->plate_number
        ]);

    }

    function UpdateVehicle(Request $request){

        $user_id = Auth::user()->id;

        Vehicle::where('id', '=', $request->id)
        ->update([
            "name"=>$request->name,
            "plate_number"=>$request->platenumber,
            "updated_at"=>DB::raw("NOW()")
        ]);

        //Insert System Activity
        Main::insert([
            "user_id"=>$user_id,
            "activity"=>"Update vehicle information.",
            "created_at"=>DB::raw("NOW()")
        ]);

        return json_encode([
            "success"=>true,
            "message"=>"Vehicle Information Has Been Update."
        ]);

    }

}
