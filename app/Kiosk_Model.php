<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Kiosk_Model extends Model
{
    
    public static function LoadRestrictionsWeb(){

        $result = DB::connection('rmsnew')
        ->table('roles')
        ->select(
            'roles.id',
            'roles.role',
            DB::raw("IF(restriction_levels_web.role_id IS NULL, FALSE, TRUE) AS 'restriction'")
        )
        ->leftjoin('restriction_levels_web', 'restriction_levels_web.role_id', '=', 'roles.id')
        ->get();

        return $result;

    }

    public static function RestrictionEnabledWeb($role){

        DB::connection('rmsnew')
        ->table('restriction_levels_web')
        ->insert([
            "role_id"=>$role,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function RestrictionDisabledWeb($role){

        DB::connection('rmsnew')
        ->table('restriction_levels_web')
        ->where('role_id', '=', $role)
        ->delete();

    }

    public static function LoadRestrictionsMobile(){

        $result = DB::connection('rmsnew')
        ->table('roles')
        ->select(
            'roles.id',
            'roles.role',
            DB::raw("IF(restriction_levels_mobile.role_id IS NULL, FALSE, TRUE) AS 'restriction'")
        )
        ->leftjoin('restriction_levels_mobile', 'restriction_levels_mobile.role_id', '=', 'roles.id')
        ->get();

        return $result;

    }

    public static function RestrictionEnabledMobile($role){

        DB::connection('rmsnew')
        ->table('restriction_levels_mobile')
        ->insert([
            "role_id"=>$role,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function RestrictionDisabledMobile($role){

        DB::connection('rmsnew')
        ->table('restriction_levels_mobile')
        ->where('role_id', '=', $role)
        ->delete();

    }

}
