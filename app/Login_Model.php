<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Login_Model extends Model
{
    
    public static function LoginRestriction($id){

        $result = DB::connection('mysql')
        ->table('vc_employees')
        ->select(
            'status'
        )
        ->where('id', '=', $id)
        ->get();

        if($result[0]->status=="Admin"){

            return true;

        }
        else{

            return false;

        }

    }

}
