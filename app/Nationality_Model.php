<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Nationality_Model extends Model
{
    
    public static function LoadNationality(){

        $result = DB::connection('rmsnew')
        ->table('nationality')
        ->select(
            'id',
            'name'
        )
        ->get();

        return $result;

    }

    public static function ValidateNationality($nationality){

        $result =  DB::connection('rmsnew')
        ->table('nationality')
        ->select(
            DB::raw("COUNT(*) as 'nationality_count'")
        )
        ->where('name', '=', $nationality)
        ->first();

        if($result->nationality_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function SaveNationality($data){

        $result =  DB::connection('rmsnew')
        ->table('nationality')
        ->insert([
            "name"=>$data->nationality,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function GetNationalityInfo($data){

        $result =  DB::connection('rmsnew')
        ->table('nationality')
        ->select(
            'name'
        )
        ->where('id', '=', $data->id)
        ->first();

        return $result;

    }

    public static function UpdateNationalityInfo($data){

        $result =  DB::connection('rmsnew')
        ->table('nationality')
        ->where('id', '=', $data->id)
        ->update([
            "name"=>$data->nationality,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

}
