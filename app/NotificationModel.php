<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Datatables;
use Illuminate\Support\Collection;

class NotificationModel extends Model
{
    //
    protected $table = "notification_blocklists";
    protected $connection = "rmsnew";

    public static function addNotificationBlockList($data){
    	return $query = DB::connection('rmsnew')
    	->table('notification_blocklists')
    	->insert([
    		'role_id' => $data->role_id,
    		'created_at' => DB::raw("NOW()")
    	]);	
    }

    //get roles added
    public static function getRolesAdded(){
    	$query = DB::connection('rmsnew')
    	->table('roles')
    	->select('*')

    	->join('notification_blocklists', 'roles.id', '=', 'notification_blocklists.role_id')

    	->get();

    	$data = array();
        foreach($query as $val){

            $obj = new \stdClass;

            $obj->role = $val->role;
            $obj->created_at = $val->created_at;
            $obj->id = $val->id;

            $data[] = $obj;

        }

        $info = new Collection($data);
        
        return Datatables::of($info)->make(true);
    }

    public static function createGroup($data){

    	return $query = DB::connection('rmsnew')
    	->table('notification_groups')
    	->insert([
    		'name' => $data->name
        ]);
        
    }

    public static function getGroup(){
    	$query = DB::connection('rmsnew')
    	->table('notification_groups')
    	->get();

    	$data = array();

    	foreach($query as $val){
    		$obj = new \stdClass;

    		$obj->name = $val->name;
    		$obj->created_at = $val->created_at;
            $obj->id = $val->id;

    		$data[] = $obj;
    	}

    	$info = new Collection($data);
    	return Datatables::of($info)->make(true);
    }

    public static function getGroupMatino(){
        return $query = DB::connection('rmsnew')
        ->table('notification_groups')
        ->get();
    }

    public static function deleteRoles($id){
        return $query = DB::connection('rmsnew')
        ->table('notification_blocklists')
        ->where('id', $id)
        ->delete();
    }


    public static function deletegroup($id){
        return $query = DB::connection('rmsnew')
        ->table('notification_groups')
        ->where('id', $id)
        ->delete();
    }

    public static function getMemberInfo(){
        $query = DB::connection('rmsnew')
        ->table('notification_members as groupMember')
        ->select(
            'groupMember.id as primaryId',
            'notification_groups.name as groupName',
            'lists.role_id as role_id',
            'lists.id as listId',
            'roles.role as roleName'
        )

        ->join('notification_groups', 'groupMember.group_id', '=', 'notification_groups.id')
        ->join('notification_blocklists as lists', 'groupMember.role_id', '=', 'lists.id')
        ->join('roles', 'lists.role_id', '=', 'roles.id')
        ->get();

        $data = array();

        foreach($query as $val){
            $obj = new \stdClass;

            $obj->groupName = $val->groupName;
            $obj->listId = $val->listId;
            $obj->primaryId = $val->primaryId;
            $obj->roleName = $val->roleName;
            $obj->role_id = $val->role_id;

            $data[] = $obj;
        }

        $info = new Collection($data);
        return Datatables::of($info)->make(true);

    }

    public static function membershipRoles(){
        return $query = DB::connection('rmsnew')
        ->table('notification_blocklists as list')
        ->select(
            'list.id as listId',
            'roles.role as name'
        )
        ->join('roles', 'list.role_id', '=', 'roles.id')
        ->get();
    }

    public static function addMemberAdd($data){
        return $query = DB::connection('rmsnew')
        ->table('notification_members')
        ->insert([
            'group_id' => $data->group,
            'role_id' => $data->role,
            'created_at' => DB::raw("NOW()")
        ]);
    }

    public static function ValidateGroupName($data){

        $result = DB::connection('rmsnew')
        ->table('notification_groups')
        ->select(
            DB::raw("COUNT(*) AS 'groupcount'")
        )
        ->where('name', '=', $data->name)
        ->first();

        if($result->groupcount!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function LoadGroupInformation(){

        $result = DB::connection('rmsnew')
        ->table('notification_groups')
        ->select(
            '*'
        )
        ->get();

        return $result;

    }

    public static function LoadGroupData($data){

        $result = DB::connection('rmsnew')
        ->table('notification_members')
        ->select(
            'notification_members.id',
            'roles.role'
        )
        ->join('roles', 'roles.id', '=', 'notification_members.role_id')
        ->where('notification_members.group_id', '=', $data->groupid)
        ->get();

        return $result;


    }

    public static function LoadRoles(){

        $result = DB::connection('rmsnew')
        ->table('roles')
        ->select(
            '*'
        )
        ->get();

        return $result;

    }

    public static function ValidateGroupRole($data){

        $result = DB::connection('rmsnew')
        ->table('notification_members')
        ->select(
            DB::raw("COUNT(*) AS 'rolecount'")
        )
        ->where('group_id', '=', $data->groupid)
        ->where('role_id', '=', $data->roleid)
        ->first();

        if($result->rolecount!=0){

            return true;

        }
        else{

            return false;

        }


    }

    public static function AddRoles($data){

        DB::connection('rmsnew')
        ->table('notification_members')
        ->insert([
            "group_id"=>$data->groupid,
            "role_id"=>$data->roleid
        ]);
        
    }

    public static function RemoveRoles($data){

        DB::connection('rmsnew')
        ->table('notification_members')
        ->where('id', '=', $data->id)
        ->delete();

    }

}
