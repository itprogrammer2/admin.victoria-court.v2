<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class NotificationSchedule_Model extends Model
{
    
    public static function GetLocalCode(){

        $result = DB::connection('rmsnew')
        ->table('settings')
        ->select(
            'local_code'
        )
        ->first();

        return $result->local_code;

    }

    public static function LoadGroup(){

        $result = DB::connection('rmsnew')
        ->table('notification_groups')
        ->select(
            'id',
            'name'
        )
        ->get();

        return $result;

    }

    public static function SaveSchedule($data){

        $result = DB::connection('rmsnew')
        ->table('notification_schedule')
        ->insertGetId([
            "schedule_name"=>$data->name,
            "description"=>$data->description,
            "operation"=>$data->operation,
            "schedule_time"=>$data->time,
            "mon"=>$data->mon,
            "tue"=>$data->tue,
            "wed"=>$data->wed,
            "thu"=>$data->thu,
            "fri"=>$data->fri,
            "sat"=>$data->sat,
            "sun"=>$data->sun,
            "created_at"=>DB::raw("NOW()"),
            "enabled"=>"1"
        ]);

        return $result;

    }

    public static function SaveScheduleGroup($id, $groupid){

        DB::connection('rmsnew')
        ->table('notification_schedule_info')
        ->insert([
            "sched_id"=>$id,
            "group_id"=>$groupid,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadSchedules(){

        $result = DB::connection('rmsnew')
        ->table('notification_schedule')
        ->select(
            'id',
            'schedule_name',
            'description',
            'operation',
            DB::raw("TIME_FORMAT(schedule_time, '%h:%i %p') AS 'schedule_time'"),
            DB::raw("
                CONCAT(
                    IF(mon!=0, 'mon, ', ''), 
                    IF(tue!=0, 'tue, ', ''),
                    IF(wed!=0, 'wed, ', ''),
                    IF(thu!=0, 'thu, ', ''),
                    IF(fri!=0, 'fri, ', ''),
                    IF(sat!=0, 'sat, ', ''),
                    IF(sun!=0, 'sun', '')
                ) AS 'day'
            "),
            DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') AS 'created_at'")
        )
        ->get();

        return $result;

    }

    public static function LoadScheduleInformation($data){

        $result = DB::connection('rmsnew')
        ->table('notification_schedule')
        ->select(
            '*'
        )
        ->where('id', '=', $data->id)
        ->first();

        return $result;

    }

    public static function LoadScheduleGroup($data){

        $result = DB::connection('rmsnew')
        ->table('notification_schedule_info')
        ->select(
            '*'
        )
        ->where('sched_id', '=', $data->id)
        ->get();

        return $result;

    }

    
    public static function UpdateSchedule($data){

        DB::connection('rmsnew')
        ->table('notification_schedule')
        ->where('id', '=', $data->id)
        ->update([
            "schedule_name"=>$data->name,
            "description"=>$data->description,
            "operation"=>$data->operation,
            "schedule_time"=>$data->time,
            "mon"=>$data->mon,
            "tue"=>$data->tue,
            "wed"=>$data->wed,
            "thu"=>$data->thu,
            "fri"=>$data->fri,
            "sat"=>$data->sat,
            "sun"=>$data->sun,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function DeleteGroupSchedule($data){

        DB::connection('rmsnew')
        ->table('notification_schedule_info')
        ->where('sched_id', '=', $data->id)
        ->delete();

    }

    public static function DeleteSchedule($data){

        DB::connection('rmsnew')
        ->table('notification_schedule')
        ->where('id', '=', $data->id)
        ->delete();
        
    }

}
