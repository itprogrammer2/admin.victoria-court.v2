<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RMSLogs_Model extends Model
{
 
    public static function ValidateRMSLogs($roomid){

        $result = DB::connection('rmsnew')
        ->table('rms_logs')
        ->select(
            DB::raw("COUNT(*) AS rms_count")
        )
        ->whereNull('e_status')
        ->where('room_id', '=', $roomid)
        ->orderBy('id', 'DESC')
        ->first();

        if($result->rms_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function UpdateRMSLogs($status, $roomid){

        $result = DB::connection('rmsnew')
        ->table('rms_logs')
        ->select(
            'id'
        )
        ->whereNull('e_status')
        ->where('room_id', '=', $roomid)
        ->orderBy('id', 'DESC')
        ->first();

        DB::connection('rmsnew')
        ->table('rms_logs')
        ->where('id', '=', $roomid)
        ->update([
            "e_status"=>$status,
            "e_dateTime"=>DB::raw("NOW()"),
            "updated_at"=>DB::raw("NOW()")
        ]);


    }

}
