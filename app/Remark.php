<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Remark extends Model
{
    //
    protected $table = 'tbl_remarks';
    protected $connection = 'rmsnew';

    public function link(){
    	return $this->hasMany('App\InspectionLink');
    }

    // public function toArea(){
    // 	return $this->hasMany('App\EagleEye');
    // }

    public function toComponent(){
    	return $this->hasMany('App\InspectionComponent');
    }

    public static function LoadRemarks(){

        $result = DB::connection('rmsnew')
        ->table('tbl_remarks')
        ->select(
            'tbl_remarks.id', 
            DB::raw("tbl_standards.name AS 'standard_name'"),
            DB::raw("tbl_remarks.name AS 'remarks_name'"),
            DB::raw("(SELECT GROUP_CONCAT(tbl_findings_type.name) FROM tbl_deduction LEFT JOIN tbl_findings_type ON tbl_findings_type.id=tbl_deduction.tbl_findings_type_id WHERE tbl_deduction.remarks_id=tbl_remarks.id GROUP BY tbl_deduction.remarks_id) AS 'finding_name'"),
            DB::raw("(SELECT GROUP_CONCAT(tbl_deduction.points) FROM tbl_deduction WHERE tbl_deduction.remarks_id=tbl_remarks.id GROUP BY tbl_deduction.remarks_id) AS 'points'")
        )
        ->leftjoin('tbl_standards', 'tbl_standards.id', '=', 'tbl_remarks.standard_id')
        ->get();

        return $result;

    }

    public static function RemarksSeparateComma($standardid, $name){

        DB::connection('rmsnew')
        ->table('tbl_remarks')
        ->insert([
            "standard_id"=>$standardid,
            "name"=>$name,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function GetAllRemarks(){

        $result = DB::connection('rmsnew')
        ->table('tbl_remarks')
        ->select(
            '*'
        )
        ->get();

        return $result;

    }

    public static function TruncateRemarksData(){

        DB::connection('rmsnew')
        ->table('tbl_remarks')
        ->delete();

    }

}
