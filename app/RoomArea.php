<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomArea extends Model
{
    //
    protected $table = 'tblroomareas';
    protected $connection = 'rmsnew';
}
