<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Room_Model extends Model
{
    
    protected $table = "tblroom";
    protected $connection = "rmsnew";

    public static function GetRoomGroup(){

        $result = DB::connection('rmsnew')
        ->table('tbl_grp_room_type')
        ->select(
            'group_name',
            DB::raw("GROUP_CONCAT(room_type_id) AS roomtype")
        )
        ->groupBy('group_name')
        ->get();

        return $result;

    }

    public static function LoadINVItems(){

        $result = DB::connection('rmsnew')
        ->table('roominventory')
        ->select(
            'id',
            'name',
            'description'
        )
        ->get();

        return $result;

    }

    public static function LoadRoomInventory($id){

        $result = DB::connection('rmsnew')
        ->table('roominventorylist')
        ->select(
            'roominventorylist.id',
            'roominventory.name',
            'roominventory.description',
            'roominventorylist.itemcode',
            'roominventorylist.brand',
            'roominventorylist.brand_description',
            'roominventorylist.serialNumber',
            'roominventorylist.trackNumber',
            'roominventorylist.differentiateValue',
            'roominventorylist.actualCost',
            'roominventorylist.quantity'
        )
        // ->leftjoin('tblroom', 'tblroom.id', '=', 'roominventorylist.room_id')
        ->leftjoin('roominventory', 'roominventory.id', '=', 'roominventorylist.room_inventory_id')
        ->where('roominventorylist.room_id', '=', $id)
        ->get();

        return $result;

    }

    public static function AddRoomInventory($data){

        DB::connection('rmsnew')
        ->table('roominventorylist')
        ->insert([
            "room_id"=>$data->id,
            "room_inventory_id"=>$data->inventoryid,
            "itemcode"=>$data->itemcode,
            "brand"=>$data->brand,
            "brand_description"=>$data->branddescription,
            "serialNumber"=>$data->serialnumber,
            "trackNumber"=>$data->tracknumber,
            "differentiateValue"=>$data->diffvalue,
            "actualCost"=>$data->actualcost,
            "quantity"=>$data->qty,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function ValidateRoomInventory($data){

        $result = DB::connection('rmsnew')
        ->table('roominventorylist')
        ->select(
            DB::raw("COUNT(*) AS 'item_count'")
        )
        ->where('room_id', '=', $data->id)
        ->where('room_inventory_id', '=', $data->inventoryid)
        ->get();

        if($result[0]->item_count==0){
            return false;
        }
        else{
            return true;
        }


    }

    public static function RemoveRoomInventory($id){

        DB::connection('rmsnew')
        ->table('roominventorylist')
        ->where('id', '=', $id)
        ->delete();

    }

    public static function UploadRoomImage($id, $image){

        DB::connection('rmsnew')
        ->table('roomimages')
        ->insert([
            "room_id"=>$id,
            "image"=>$image,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadRoomImages($id){

        $result = DB::connection('rmsnew')
        ->table('roomimages')
        ->select(
            'id',
            'image'
        )
        ->where('room_id', '=', $id)
        ->get();

        return $result;

    }

    public static function GetRoomImage($id){

        $result = DB::connection('rmsnew')
        ->table('roomimages')
        ->select(
            'image'
        )
        ->where('id', '=', $id)
        ->get();

        return $result[0]->image;

    }

    public static function RemoveRoomImage($id){

        DB::connection('rmsnew')
        ->table('roomimages')
        ->where('id', '=', $id)
        ->delete();

    }

    public static function LoadStatusOveride(){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'id',
            'room_status'
        )
        ->get();

        return $result;

    }

    public static function LoadRoomRates($id){

        $result = DB::connection('rmsnew')
        ->select(DB::raw("
            SELECT 
            roomrates.ID, 
            roomrates.RateDesc,
            IFNULL(roomrates_count.rate_count, '0') AS 'rate_count',
            IFNULL(roomrates_count.updated_at, IFNULL(roomrates_count.created_at, 'No Data')) AS 'last_updated'
            FROM roomrates
            LEFT JOIN roomrates_count ON roomrates_count.room_id='".$id."' AND roomrates_count.rate_id=roomrates.ID
            WHERE roomrates.RoomType=(SELECT tblroomtype.room_type FROM tblroom INNER JOIN tblroomtype ON tblroomtype.id=tblroom.room_type_id WHERE tblroom.id='".$id."')
            ORDER BY rate_count DESC
        "));

        return $result;

    }

    public static function LoadRoomTypes(){

        $result = DB::connection('rmsnew')
        ->table('tblroomtype')
        ->select(
            'id',
            'room_type'
        )
        ->get();

        return $result;

    }

    public static function CheckRoomType($data){

        $result = DB::connection('rmsnew')
        ->table('tblroomtype')
        ->select(
            DB::raw("COUNT(*) AS 'roomtype_count'")
        )
        ->where('room_type', '=', $data->roomtype)
        ->first();

        if($result->roomtype_count==0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function SaveRoomTypeInformation($data){

        DB::connection('rmsnew')
        ->table('tblroomtype')
        ->insert([
            "room_type"=>$data->roomtype,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadGroupRoomType(){

        $result = DB::connection('rmsnew')
        ->table('tbl_grp_room_type')
        ->select(
            'tbl_grp_room_type.group_name',
            DB::raw("GROUP_CONCAT(tblroomtype.room_type) AS 'room_types'")
        )
        ->join('tblroomtype', 'tblroomtype.id', '=', 'tbl_grp_room_type.room_type_id')
        ->groupBy('group_name')
        ->get();

        return $result;

    }

    public static function GetRoomTypeInformation($data){

        $result = DB::connection('rmsnew')
        ->table('tblroomtype')
        ->select(
            'room_type'
        )
        ->where('id', '=', $data->roomtype)
        ->first();

        return $result->room_type;

    }

    public static function UpdateRoomType($data){

        DB::connection('rmsnew')
        ->table('tblroomtype')
        ->where('id', '=', $data->roomtypeid)
        ->update([
            "room_type"=>$data->roomtype,
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadAutoGroupRoomType(){

        $result = DB::connection('rmsnew')
        ->table('tbl_grp_room_type')
        ->select(
            'group_name'
        )
        ->groupBy('group_name')
        ->get();

        return $result;

    }

    public static function LoadRTypes(){

        $result = DB::connection('rmsnew')
        ->table('tblroomtype')
        ->select(
            'id',
            'room_type'
        )
        ->get();

        return $result;

    }

    public static function SaveGroupRoomTypeInformation($groupname, $roomtype){

        DB::connection('rmsnew')
        ->table('tbl_grp_room_type')
        ->insert([
            "group_name"=>$groupname,
            "room_type_id"=>$roomtype,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function DeleteGroupRoomTypeInformation($data){

        DB::connection('rmsnew')
        ->table('tbl_grp_room_type')
        ->where('group_name', '=', $data->groupname)
        ->delete();

    }

    public static function LoadGroupRoomTypeItems($data){

        $result = DB::connection('rmsnew')
        ->table('tbl_grp_room_type')
        ->select(
            'room_type_id'
        )
        ->where('group_name', '=', $data->groupname)
        ->get();

        return $result;

    }

    public static function ValidateGroupRoomType($groupname){

        $result = DB::connection('rmsnew')
        ->table('tbl_grp_room_type')
        ->select(
            DB::raw("COUNT(*) AS 'groupname_count'")
        )
        ->where('group_name', '=', $groupname)
        ->first();

        if($result->groupname_count==0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function LoadRoomType(){

        $result = DB::connection('rmsnew')
        ->table('tblroomtype')
        ->select(
            'id',
            'room_type'
        )
        ->get();

        return $result;

    }

    public static function GetAllHMSRooms(){

        $result = DB::connection('hms')
        ->table('tblroom')
        ->select(
            'RoomNo',
            'RoomType'
        )
        ->get();

        return $result;

    }

    public static function GetRoomTypeID($room_type){

        $result = DB::connection('rmsnew')
        ->table('tblroomtype')
        ->select(
            'id'
        )
        ->where('room_type', '=', $room_type)
        ->first();

        return $result->id;

    }

    public static function SyncInformations($room_no, $room_type_id){

        $result = DB::connection('rmsnew')
        ->table('tblroom')
        ->where('room_no', '=', $room_no)
        ->update([
            "room_type_id"=>$room_type_id
        ]);

    }

    public static function SyncRoomInformations($hms, $rms){

        DB::connection('rmsnew')
        ->select(DB::raw("
            INSERT INTO tblroom (
                room_no,
                room_name,
                room_description,
                room_area_id,
                room_status_id,
                from_room_status_id,
                room_type_id,
                last_check_in,
                checkout_count,
                last_general_cleaning,
                created_at,
                updated_at
            ) 
            SELECT 
            ".$hms.".tblroom.RoomNo,
            ".$hms.".tblroom.RoomNo,
            ".$hms.".tblroom.RoomNo, 
            ".$rms.".tblroomareas.id,
            1,
            1,
            ".$rms.".tblroomtype.id,
            NOW(),
            0,
            NOW(),
            NOW(),
            NOW()
            FROM ".$hms.".tblroom
            INNER JOIN ".$rms.".tblroomareas ON ".$rms.".tblroomareas.room_area=".$hms.".tblroom.RoomArea
            INNER JOIN ".$rms.".tblroomtype ON ".$rms.".tblroomtype.room_type=".$hms.".tblroom.RoomType
        "));
        
   }

   public static function CheckRoomCount(){

        $result = DB::connection('rmsnew')
        ->table('tblroom')
        ->select(
            DB::raw("COUNT(*) AS 'room_count'")
        )
        ->get();

        return $result[0]->room_count;

   }

   public static function ValidateCleanInfo($room_id, $room_status_id){

        $result = DB::connection('rmsnew')
        ->table('tblroomcleaningtime')
        ->select(
            DB::raw("COUNT(*) AS 'regcount'")
        )
        ->where('room_id', '=', $room_id)
        ->where('room_status_id', '=', $room_status_id)
        ->first();

        if($result->regcount!=0){

            return true;

        }
        else{

            return false;

        }
        
   }

   public static function UpdateCleanInfo($room_id, $room_status_id, $points, $hours, $mins){

        DB::connection('rmsnew')
        ->table('tblroomcleaningtime')
        ->where('room_id', '=', $room_id)
        ->where('room_status_id', '=', $room_status_id)
        ->update([
            "points"=>$points,
            "hours"=>$hours,
            "minutes"=>$mins,
            "updated_at"=>DB::raw("NOW()")
        ]);

   }

   public static function InserCleanInfo($room_id, $room_status_id, $points, $hours, $mins){

        DB::connection('rmsnew')
        ->table('tblroomcleaningtime')
        ->insert([
            "room_id"=>$room_id,
            "room_status_id"=>$room_status_id,
            "points"=>$points,
            "hours"=>$hours,
            "minutes"=>$mins,
            "created_at"=>DB::raw("NOW()")
        ]);

   }

   public static function LoadRoomCleanInfo($room_id){

        $result = DB::connection('rmsnew')
        ->table('tblroomcleaningtime')
        ->select(
            'id',
            'room_status_id',
            'points',
            'hours',
            'minutes'
        )
        ->where('room_id', '=', $room_id)
        ->get();

        return $result;

   }


}
