<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Setting_Model extends Model
{

    public static function LoadSettings(){

        $result = DB::connection('rmsnew')
        ->table('settings')
        ->select('*')
        ->get();

        return $result;

    }

    public static function SaveSettings($data){

        DB::connection('rmsnew')
        ->table('settings')
        ->update([
            "local_id"=>$data->localid,
            "local_code"=>$data->localcode,
            "reservation_locked_out_time"=>$data->reservationlocktime,
            "reservation_allowance"=>$data->reservationallowance,
            "automatic_general_cleaning_days"=>$data->autogc,
            "automatic_general_cleaning_checkout"=>$data->autogccheckout,
            "automatic_preventive_maintenance_days"=>$data->autopm,
            "automatic_preventive_maintance_checkout"=>$data->autopmcheckout,
            "automated_clean_reclean"=>$data->autocleanreclean,
            "automated_dirty_reclean"=>$data->autodirtyreclean,
            "staying_guest_12"=>$data->stayingguest12,
            "staying_guest_24"=>$data->stayingguest24,
            "window_time_general_cleaning_start"=>$data->windowtimegcstart,
            "window_time_general_cleaning_end"=>$data->windowtimegcend,
            "automatic_belo_percentage"=>$data->belopercentage,
            "automatic_belo_checkout"=>$data->belo,
            "recovery_time"=>$data->pest,
            "user_restricted"=>"0",
            "access_restricted"=>"0",
            "updated_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadRoomTypeGroup(){

        $result = DB::connection('rmsnew')
        ->table('tbl_grp_room_type')
        ->select(
            DB::raw("GROUP_CONCAT(id) AS 'group_id'"),
            'group_name'
        )
        ->groupBy('group_name')
        ->get();

        return $result;

    }

    public static function LoadRoomType($data){

        $result = DB::connection('rmsnew')
        ->table('tblroomtype')
        ->select(
            'id',
            'room_type'
        )
        ->whereIn('id', $data->group_id)
        ->get();

        return $result;

    }

    public static function LoadStatusPoints($data){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'roomstatus.id',
            'roomstatus.room_status',
            DB::raw("IFNULL(room_type_points.points, '0') AS 'points'")
        )
        ->leftjoin('room_type_points', function($join) use ($data){
            $join->on('room_type_points.room_status_id', '=', 'roomstatus.id');
            $join->where('room_type_points.room_type_id', '=', $data->id);
        })
        ->get();

        return $result;

    }

    public static function LoadStatusName($data){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'room_status'
        )
        ->where('id', '=', $data->id)
        ->first();

        return $result->room_status;

    }

    public static function CheckPointsInformation($data){

        $result = DB::connection('rmsnew')
        ->table('room_type_points')
        ->select(
            DB::raw("COUNT(*) AS 'point_count'")
        )
        ->where('room_status_id', '=', $data->room_status)
        ->where('room_type_id', '=', $data->room_type)
        ->first();

        if($result->point_count=="0"){
            return true;
        }
        else{
            return false;
        }

    }

    public static function InsertPointsInformation($data){

        DB::connection('rmsnew')
        ->table('room_type_points')
        ->insert([
            'room_type_id'=>$data->room_type,
            'room_status_id'=>$data->room_status,
            'points'=>$data->points,
            'created_at'=>DB::raw("NOW()")
        ]);

    }

    public static function UpdatePointInformation($data){

        DB::connection('rmsnew')
        ->table('room_type_points')
        ->where('room_status_id', '=', $data->room_status)
        ->where('room_type_id', '=', $data->room_type)
        ->update([
            'points'=>$data->points,
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function LoadPoints($data){

        $result = DB::connection('rmsnew')
        ->table('room_type_points')
        ->select(
            'points'
        )
        ->where('room_status_id', '=', $data->room_status)
        ->where('room_type_id', '=', $data->room_type)
        ->get();

        if(count($result)!=0){
            return $result[0]->points;
        }
        else{
            return 0;
        }

    }

    public static function LoadScoreRating(){

        $result = DB::connection('rmsnew')
        ->table('tbl_score_rating')
        ->select(
            'id',
            DB::raw("CONCAT(score_from, '%' , ' - ', score_to, '%') AS 'score'"),
            'rating'
        )
        ->get();

        return $result;

    }

    public static function SaveNewRating($data){


        DB::connection('rmsnew')
        ->table('tbl_score_rating')
        ->insert([
            'score_from'=>$data->scorefrom,
            'score_to'=>$data->scoreto,
            'rating'=>$data->rating,
            'created_at'=>DB::raw("NOW()")
        ]);


    }

    public static function LoadRatingInformation($data){

        $result = DB::connection('rmsnew')
        ->table('tbl_score_rating')
        ->select(
            'score_from',
            'score_to',
            'rating'
        )
        ->where('id', '=', $data->sid)
        ->first();

        return $result;

    }

    public static function UpdateRating($data){

        DB::connection('rmsnew')
        ->table('tbl_score_rating')
        ->where('id', '=', $data->sid)
        ->update([
            'score_from'=>$data->scorefrom,
            'score_to'=>$data->scoreto,
            'rating'=>$data->rating,
            'updated_at'=>DB::raw("NOW()")
        ]);

    }

    public static function DeleteRating($data){

        DB::connection('rmsnew')
        ->table('tbl_score_rating')
        ->where('id', '=', $data->sid)
        ->delete();

    }

    public static function LoadRoomBalancer(){

        $result = DB::connection('rmsnew')
        ->table('tbl_room_balancer')
        ->select(
            'tbl_room_balancer.id',
            'tbl_room_balancer.order',
            DB::raw("tblroomtype.room_type AS 'group_name'"),
            'tbl_room_balancer.percentage'
        )
        ->join('tblroomtype', 'tblroomtype.id', '=', 'tbl_room_balancer.grproomid')
        ->orderBy('order')
        ->get();

        return $result;

    }

    public static function LoadGRPType(){

        $result = DB::connection('rmsnew')
        ->table('tblroomtype')
        ->select(
            'id',
            'room_type'
        )
        ->whereRaw("id IN (SELECT room_type_id FROM tblroom GROUP BY room_type_id)")
        ->get();

        return $result;

    }

    public static function SaveRoomTypePercentage($grpid, $percentage, $order){

        DB::connection('rmsnew')
        ->table('tbl_room_balancer')
        ->insert([
            "grproomid"=>$grpid,
            "percentage"=>$percentage,
            "order"=>$order,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function DeleteRoomTypePercentage($id){

        DB::connection('rmsnew')
        ->table('tbl_room_balancer')
        ->where('id', '=', $id)
        ->delete();

    }

    public static function OrderValidation($order){

        $result = DB::connection('rmsnew')
        ->table('tbl_room_balancer')
        ->select(
            DB::raw("COUNT(*) AS 'ordercount'")
        )
        ->where('order', '=', $order)
        ->first();

        if($result->ordercount!=0){

            return true;

        }
        else{

            return false;

        }

    }

    public static function RoomTypeValidation($grpid){

        $result = DB::connection('rmsnew')
        ->table('tbl_room_balancer')
        ->select(
            DB::raw("COUNT(*) AS 'roomtypecount'")
        )
        ->where('grproomid', '=', $grpid)
        ->first();

        if($result->roomtypecount!=0){

            return true;

        }
        else{

            return false;

        }

    }

}
