<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class UserAccess_Model extends Model
{
    protected $table = "";
    protected $connetion = "";

    public static function LoadUserRoles(){

        $result = DB::connection('rmsnew')
        ->table('roles')
        ->select(
            'id',
            'role'
        )
        ->get();

        return $result;

    }

    public static function LoadRoleName($id){

        $result = DB::connection('rmsnew')
        ->table('roles')
        ->select(
            'role'
        )
        ->where('id', '=', $id)
        ->get();

        return $result;

    }

    public static function LoadAllowStatus($id){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'roomstatus.id',
            'roomstatus.room_status',
            DB::raw("IFNULL(GROUP_CONCAT(a.room_status), 'N/A') AS 'allow_status'")
        )
        ->leftjoin('access_levels', function($join) use ($id){
            $join->on('access_levels.room_status_id', '=', 'roomstatus.id')->where('access_levels.role_id', '=', $id);
        })
        ->leftjoin('roomstatus as a', 'a.id', '=', 'access_levels.allow_status_id')
        ->groupBy('roomstatus.id', 'roomstatus.room_status')
        ->get();

        return $result;

    }

    public static function LoadRoomStatus(){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'id',
            'room_status'
        )
        ->get();

        return $result;

    }

    public static function ValidateNextStatus($data){

        $result = DB::connection('rmsnew')
        ->table('access_levels')
        ->select(
            DB::raw("COUNT(*) AS 'nextstatus_count'")
        )
        ->where('room_status_id', '=', $data->targetstatus)
        ->where('allow_status_id', '=', $data->nextstatus)
        ->where('role_id', '=', $data->id)
        ->get();

        if($result[0]->nextstatus_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function SaveTargetStatusInfo($data){

        DB::connection('rmsnew')
        ->table('access_levels')
        ->insert([
            "role_id"=>$data->id,
            "room_status_id"=>$data->targetstatus,
            "allow_status_id"=>$data->nextstatus,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadAllowStatusProfile($id, $statusid){

        $result = DB::connection('rmsnew')
        ->table('access_levels')
        ->select(
            'access_levels.id',
            'roomstatus.room_status'
        )
        ->leftjoin('roomstatus', 'roomstatus.id', '=', 'access_levels.allow_status_id')
        ->where('access_levels.role_id', '=', $id)
        ->where('access_levels.room_status_id', '=', $statusid)
        ->get();

        return $result;

    }

    public static function RemoveAllowStatus($id){

        DB::connection('rmsnew')
        ->table('access_levels')
        ->where('id', '=', $id)
        ->delete();

    }

    public static function GetRoomStatus($id){

        $result = DB::connection('rmsnew')
        ->table('roomstatus')
        ->select(
            'room_status'
        )
        ->where('id', '=', $id)
        ->get();

        return $result;

    }

    public static function ValidateUserRole($role){

        $result = DB::connection('rmsnew')
        ->table('roles')
        ->select(
            DB::raw("COUNT(*) AS 'role_count'")
        )
        ->where('role', '=', $role)
        ->get();

        if($result[0]->role_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function AddUserRole($data){

        DB::connection('rmsnew')
        ->table('roles')
        ->insert([
            "role"=>$data->role
        ]);

    }

    public static function LoadResources(){

        $result = DB::connection('rmsnew')
        ->table('resources')
        ->select(
            'id',
            'name',
            'description'
        )
        ->get();

        return $result;
    }

    public static function ValidateResources($resources){

        $result = DB::connection('rmsnew')
        ->table('resources')
        ->select(
            DB::raw("COUNT(*) AS 'name_count'")
        )
        ->where('name', '=', $resources)
        ->get();

        if($result[0]->name_count!=0){
            return true;
        }
        else{
            return false;
        }

    }

    public static function SaveResources($data){

        DB::connection('rmsnew')
        ->table('resources')
        ->insert([
            "name"=>$data->resources,
            "description"=>$data->description,
            "created_at"=>DB::raw("NOW()")
        ]);

    }

    public static function LoadAllowResources($role){

        $result = DB::connection('rmsnew')
        ->table('resources')
        ->select(
            'resources.id', 
            'resources.name', 
            'resources.description',	
             DB::raw("IF(permission_levels.resources_id IS NULL, FALSE, TRUE) AS 'permission'")
        )
        ->leftjoin('permission_levels', function($join) use ($role){
            $join->on('permission_levels.resources_id', '=', 'resources.id')
            ->where('permission_levels.role_id', '=', $role);
        })
        ->get();

        return $result;

    }

    public static function ResourcesEnabled($data){

        DB::connection('rmsnew')
        ->table('permission_levels')
        ->insert([
            'role_id'=>$data->id,
            'resources_id'=>$data->resources,
            'created_at'=>DB::raw("NOW()")
        ]);
        
    }

    public static function ResourcesDisabled($data){

        DB::connection('rmsnew')
        ->table('permission_levels')
        ->where('role_id', '=', $data->id)
        ->where('resources_id', '=', $data->resources)
        ->delete();

    }

}
