<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Vehicle_Model extends Model
{
   
    protected $table = "vehicle";
    protected $connection = "rmsnew";

}
