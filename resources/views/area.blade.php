@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Area</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12">

                    <button id="btnnewarea" name="btnnewarea" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;" data-toggle="modal" data-target="#newarea"><i class="fa fa-plus"></i> New Area</button>

                </div>

                <div class="col-lg-12">

                    <table id="tblarea" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Area</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

    @include('modals.area.newarea')
    @include('modals.area.updatearea')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    //Variables
    var tblarea;
    var sareaid;

    $(document).ready(function(){

        LoadArea();

    });

    $('#btnnewarea').on('click', function(){

        ClearNewArea();

    });

    $('#btnsavearea').on('click', function(){

        var area = $('#txtnarea').val();

        if(area==""){

            toastr.error('Please input the area name.', '', { positionClass: 'toast-top-center' });
            $('#txtnarea').focus();

        }
        else{

            $.ajax({
                url: '{{ url("api/area/savearea") }}',
                type: 'post',
                data: {
                    area: area
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#newarea .close').click();
                        ReloadArea();

                    }

                }
            });

        }

    });

    $(document).on('click', '#btnedit', function(){

        sareaid = $(this).val();
        ClearUpdateArea();
        LoadAreaInformation();

    });

    $('#btnupdate').on('click', function(){

        UpdateArea();

    });

    function UpdateArea(){

        var area = $('#txtuarea').val();

        if(area==""){

            toastr.error('Please input the area name.', '', { positionClass: 'toast-top-center' });
            $('#txtuarea').focus();

        }
        else{

            $.ajax({
                url: '{{ url("api/area/updatearea") }}',
                type: 'post',
                data: {
                    id: sareaid,
                    area: area
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#updatearea .close').click();
                        ReloadArea();

                    }

                }
            });

        }

    }

    function LoadAreaInformation(){

        $.ajax({
            url: '{{ url("api/area/loadareainformation") }}',
            type: 'get',
            data: {
                id: sareaid
            },
            dataType: 'json',
            success: function(response){

                $('#txtuarea').val(response.area);

            }
        });

    }

    function ClearUpdateArea(){

        $('#txtuarea').val('');

    }

    function ClearNewArea(){

        $('#txtnarea').val('');

    }

    function LoadArea(){

        tblarea = $('#tblarea').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/area/loadarea") }}',
            },
            columns : [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });


    }

    function ReloadArea(){

        tblarea.ajax.reload();

    }


</script>
@endsection