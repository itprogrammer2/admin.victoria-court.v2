@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Event</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12">

                    <button id="btnnewevent" name="btnnewevent" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i> New Event</button>

                </div>

                <div class="col-lg-12">

                    <table id="tblevent" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Reason</th>
                                <th>Color</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

{{-- Modal --}}
@include('modals.event.newevent')
@include('modals.event.editevent')

@endsection

@section('scripts')
<script>

    //Variables
    var tblevent;
    var sid;

    $(document).ready(function(){

        //Load
        LoadEventReason();

    });

    $('#btnnewevent').on('click', function(){

        //Clear
        $('#txtnreason').val('');

        $('#newevent').modal('toggle');

    });

    $('#btnnsave').on('click', function(){

        SaveEventReason();

    });

    $(document).on('click', '#btnedit', function(){

        sid = $(this).val();

        LoadEventProfile(sid);
        $('#editevent').modal('toggle');

    });

    $('#btnupdate').on('click', function(){

        UpdateEventReason();

    });

    function UpdateEventReason(){

        var reason = $('#txtureason').val();
        var color = $('#txtucolor').val();

        //Validation
        if(reason==""){

            toastr.error('Please input a reason.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/event/updateeventreason") }}',
                type: 'post',
                data: {
                    id: sid,
                    reason: reason,
                    color: color
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#editevent .close').click();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadEventReason();

                    }

                }
            });

        }

    }

    function LoadEventProfile(id){

        $.ajax({
            url: '{{ url("api/event/loadeventprofile") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#txtureason').val(response.reason);
                $('#txtucolor').val(response.color);

            }
        });

    }

    function SaveEventReason(){

        var reason = $('#txtnreason').val();
        var color = $('#txtncolor').val();

        //Validation
        if(reason==""){

            toastr.error('Please input a reason.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/event/saveeventreason") }}',
                type: 'post',
                data: {
                    reason: reason,
                    color: color
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#newevent .close').click();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadEventReason();


                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

    }

    function LoadEventReason(){

        tblevent = $('#tblevent').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/event/loadeventreason") }}',
            },
            columns : [
                {data: 'id', name: 'id'},
                {data: 'reason', name: 'reason'},
                {data: 'color', name: 'color'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

    function ReloadEventReason(){

        tblevent.ajax.reload();

    }

</script>
@endsection
