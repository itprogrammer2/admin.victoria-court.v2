@foreach($roomgroup as $group)

<?php
$groupname = $group->group_name;
$rtypes = explode(',', $group->roomtype);
?>

<div class="panel panel-default">
        
        <div class="panel-heading">
            <h4>{{ $groupname }}</h4>
        </div>
        <ul class="list-inline">
        @foreach($room as $val)
            @if(array_search($val->room_type_id, $rtypes)!==false)
                   
                    
            <li class="list-inline-item" style="width: 90px; text-align: center;">
                <a href="{{ url("/rooms/roomprofile/".$val->id) }}"><button class="btn btn-flat" id="roomid" name="roomid[]" style="height: 90px; width: 100%; margin: 10px 10px; background: url({{ asset("images/no-image.jpg") }}); background-position: center;"></button></a>
                <strong style="padding-left: 15px;">{{ $val->room_no }}</strong>
            </li>
                        
            @endif
        @endforeach
        </ul>
</div>
@endforeach
