@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">
                    Room Type Group Settings
                    <a href="{{ url("/rooms") }}" style="float: right; font-size: 15px; margin-top: 10px;"><i class="fa fa-backward"></i></a>
                </h1>
                
                <button id="btnnewroomtype" name="btnnewroomtype" data-toggle="modal" data-target="#roomtype" class="btn btn-success" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Room Type</button>

                <button id="btnnewgrouproomtype" name="btnnewgrouproomtype" data-toggle="modal" data-target="#newgrouproomtype" class="btn btn-success" style="float: right; margin-bottom: 10px; margin-right: 10px;"><i class="fa fa-plus"></i> Add Group Room Type</button>

            </div>

            <div class="col-lg-12">

                <table id="tblroomgrouptype" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Group Name</th>
                            <th>Room Type</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

     {{-- Modal --}}
     @include('modals.rooms.roomtype')
     @include('modals.rooms.newgrouproomtype')
     @include('modals.rooms.updategrouproomtype')
     
</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    //Variables Room Type
    var tblroomtype;
    var sroomtypeid;
    var addroomtype = false;
    var updateroomtype = false;

    //Variables Group Room Type
    var tblroomgrouptype;
    var groupitemarray = new Array();
    var groupuitemarray = new Array();
    // var groupditemarray = new Array();
    var groupitemcount = 1;
    var groupuitemcount = 1;

    $(document).ready(function(){

        SetDatatable();
        LoadGroupRoomType();

    });

    $('#btnnewroomtype').on('click', function(){

        //Set
        addroomtype = false;
        updateroomtype = false;
        $('#newroomtype').hide();
        $('#updateroomtype').hide();
        $('#btnbackupdate').hide();
        $('#btnaddroomtype').show();
        $('#roomtypetable').show();
        ClearNewRoomType();

        //Load
        LoadRoomTypes();

    });

    $('#btnaddroomtype').on('click', function(){

        if(addroomtype){

            $('#newroomtype').hide();
            $('#roomtypetable').show();

            addroomtype = false;

        }
        else{

            ClearNewRoomType();
            $('#newroomtype').show();
            $('#roomtypetable').hide();

            addroomtype = true;

        }

    });

    $('#btnnsaveroomtype').on('click', function(){

        var roomtype = $('#txtnroomtype').val();

        if(roomtype==""){

            toastr.error('Please input room type.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/rooms/saveroomtypeinformation") }}',
                type: 'post',
                data: {
                    roomtype: roomtype
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ClearNewRoomType();
                        ReloadRoomType();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                        $('#newroomtype').hide();
                        $('#roomtypetable').show();

                        addroomtype = false;

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

    });

    $(document).on('click', '#btnroomtypeedit', function(){

        sroomtypeid = $(this).val();
        ClearUpdateRoomType();

        $.ajax({
            url: '{{ url("api/rooms/getroomtypeinformation") }}',
            type: 'get',
            data: {
                roomtype: sroomtypeid
            },
            dataType: 'json',
            success: function(response){

                $('#txturoomtype').val(response.roomtype);

            }
        });

        $('#btnaddroomtype').hide();
        $('#btnbackupdate').show();
        $('#updateroomtype').show();
        $('#roomtypetable').hide();

        updateroomtype = true;

    });

    $('#btnbackupdate').on('click', function(){

        $('#btnaddroomtype').show();
        $('#btnbackupdate').hide();
        $('#updateroomtype').hide();
        $('#roomtypetable').show();

        updateroomtype = false;

    });

    $('#btnuupdateroomtype').on('click', function(){

        var roomtype = $('#txturoomtype').val();

        if(roomtype==""){

            toastr.error('Please input room type.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/rooms/updateroomtype") }}',
                type: 'post',
                data: {
                    roomtypeid: sroomtypeid,
                    roomtype: roomtype
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadRoomType();
                        sroomtypeid = "";
                        ClearUpdateRoomType();

                        $('#btnaddroomtype').show();
                        $('#btnbackupdate').hide();
                        $('#updateroomtype').hide();
                        $('#roomtypetable').show();

                        updateroomtype = false;

                    }

                }
            });

        }

    });

    $('#btnnewgrouproomtype').on('click', function(){

        LoadGroupName();
        LoadRTypes($('#cmbnroomtype1').attr('id'));
        ClearNewGroupRoomType();

    });

    $('#btnaddgrouproomtype').on('click', function(){

        groupitemcount += 1;
        groupitemarray.push(groupitemcount);
        $('#newgrouproomtypecontent').append('<tr id="tr'+ groupitemcount +'"><td><select id="cmbnroomtype'+ groupitemcount +'" name="cmbnroomtype[]" class="form-control"></select><td><button id="btnremove" name="btnremove" class="btn btn-flat btn-danger" value="'+ groupitemcount +'"><i class="fa fa-trash"></i></button></td></tr>');

        LoadRTypes($('#cmbnroomtype'+groupitemcount).attr('id'));

    });

    $(document).on('click', '#btnremove', function(){

        var id = $(this).val();
        $('#tr'+id).remove();

    });

    $('#btnsavenewgrouproomtype').on('click', function(){

        var groupname = $('#txtngroupname').val();
        var roomtypes = $("[name='cmbnroomtype[]']").serializeArray();
        var isroomtypeblank = false;

        if(groupname==""){

            toastr.error('Please input the group name of the room types.', '', { positionClass: 'toast-top-center' });

        }
        else{

            for(var i=0;i<roomtypes.length;i++){
            
                if(roomtypes[i].value==""){
                    isroomtypeblank = true;
                    break;
                }

            }

            if(isroomtypeblank){

                toastr.error('Please select a room type.', '', { positionClass: 'toast-top-center' });

            }
            else{

                $.ajax({
                    url: '{{ url("api/rooms/savegrouproomtypeinformation") }}',
                    type: 'post',
                    data: {
                        groupname: groupname,
                        roomtypes: roomtypes
                    },
                    dataType: 'json',
                    success: function(response){

                        if(response.success){

                            $('#newgrouproomtype .close').click();
                            toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                            ReloadGroupRoomType();

                        }
                        else{

                            toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                        }

                    }
                });

            }

        }

    });

    $(document).on('click', '#btndeletegrouproomtype', function(){

        var groupname = $(this).val();
        
        $.ajax({
            url: '{{ url("api/rooms/deletegrouproomtypeinformation") }}',
            type: 'post',
            data: {
                groupname: groupname
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    ReloadGroupRoomType();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

    });

    $(document).on('click', '#btnupdategrouproomtype', function(){

        var groupname = $(this).val();

        groupuitemcount = 1;
        groupuitemarray = [];
        groupditemarray = [];

        $('#txtugroupname').val(groupname);
        $('#updategrouproomtypecontent').find('tr').remove();
        LoadGroupRoomTypeItems(groupname);

    });

    $('#btuaddgrouproomtype').on('click', function(){

        groupuitemcount += 1;
        groupuitemarray.push(groupuitemcount);
        $('#updategrouproomtypecontent').append('<tr id="tru'+ groupuitemcount +'"><td><select id="cmburoomtype'+ groupuitemcount +'" name="cmburoomtype[]" class="form-control"></select><td><button id="btnuremove" name="btnuremove" class="btn btn-flat btn-danger" value="'+ groupuitemcount +'"><i class="fa fa-trash"></i></button></td></tr>');

        LoadRTypes($('#cmburoomtype'+groupuitemcount).attr('id'));

    });

    $(document).on('click', '#btnuremove', function(){

        var id = $(this).val();
        var roomtype = $('#cmburoomtype'+id).val();
        
        // groupditemarray.push(roomtype);

        $('#tru'+id).remove();

    });

    $('#btnupdatenewgrouproomtype').on('click', function(){

       var groupname = $('#txtugroupname').val();
       var roomtypes = $("[name='cmburoomtype[]']").serializeArray();
       var isroomtypeblank = false;

       for(var i=0;i<roomtypes.length;i++){
            
            if(roomtypes[i].value==""){
                isroomtypeblank = true;
                break;
            }

       }

       if(isroomtypeblank){

        toastr.error('Please select a room type.', '', { positionClass: 'toast-top-center' });

       }
       else{

            $.ajax({
                url: '{{ url("api/rooms/updategrouproomtypeinformation") }}',
                type: 'post',
                data: {
                    groupname: groupname,
                    roomtypes: roomtypes  
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#updategrouproomtype .close').click();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadGroupRoomType();

                    }

                }
            });

       }

    });

    function LoadGroupRoomTypeItems(groupname){

        $.ajax({
            url: '{{ url("api/rooms/loadgrouproomtypeitems") }}',
            type: 'get',
            data: {
                groupname: groupname
            },
            dataType: 'json',
            beforeSend: function(){

                $('#modalupdatecontent').waitMe({

                    effect : 'roundBounce',
                    text : '',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000'

                });

            },
            success: function(response){

                
                for(var i=0;i<response.data.length;i++){

                  groupuitemarray.push(groupuitemcount);

                  if(groupuitemcount==1){
                    $('#updategrouproomtypecontent').append('<tr id="tru'+ groupuitemcount +'"><td><select id="cmburoomtype'+ groupuitemcount +'" name="cmburoomtype[]" class="form-control"></select><td></td></tr>');
                  }
                  else{
                    $('#updategrouproomtypecontent').append('<tr id="tru'+ groupuitemcount +'"><td><select id="cmburoomtype'+ groupuitemcount +'" name="cmburoomtype[]" class="form-control"></select><td><button id="btnuremove" name="btnuremove" class="btn btn-flat btn-danger" value="'+ groupuitemcount +'"><i class="fa fa-trash"></i></button></td></tr>');
                  }
                  
                  LoadUpdateRTypes($('#cmburoomtype'+groupuitemcount).attr('id'), response.data[i]["room_type_id"], i, response.data.length);
                  
                  groupuitemcount += 1;


                }

            }
        })

    }

    function LoadUpdateRTypes(id, room_type_id, itemload, itemcount){

        $.ajax({
            url: '{{ url("api/rooms/loadrtypes") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select A Room Type</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#'+id).append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["room_type"] +'</option>');

                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            },
            complete: function(){

                $('#'+id).val(room_type_id).trigger('change');
                if(itemload+1==itemcount){
                      $('#modalupdatecontent').waitMe('hide');
                }

            }
        })

    }

    function LoadRTypes(id){

        $.ajax({
            url: '{{ url("api/rooms/loadrtypes") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select A Room Type</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#'+id).append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["room_type"] +'</option>');

                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            }
        })

    }

    function LoadGroupName(){

        var availableTags = [];

        $.ajax({
            url: '{{ url("api/rooms/loadautogrouproomtype") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                for(var i=0;i<response.data.length;i++){
                    availableTags.push(response.data[i]["group_name"]);
                }

                $('#txtngroupname').autocomplete({
                    source: availableTags
                });

            }
        });

    }

    function LoadRoomTypes(){

        tblroomtype.destroy();
        tblroomtype = $('#tblroomtype').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/rooms/loadroomtypes") }}'
            },
            columns : [
                {data: 'roomtype', name: 'roomtype', width: '100%'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    function LoadGroupRoomType(){

        tblroomgrouptype = $('#tblroomgrouptype').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/rooms/loadgrouproomtype") }}'
            },
            columns : [
                {data: 'groupname', name: 'groupname'},
                {data: 'roomtype', name: 'roomtype'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    function ClearNewRoomType(){

        $('#txtnroomtype').val('');

    }

    function ClearUpdateRoomType(){

        $('#txturoomtype').val('');

    }

    function ClearNewGroupRoomType(){

        $('#txtngroupname').val('');
        groupitemcount = 1;

        for(var i=0;i<groupitemarray.length;i++){

            $('#tr'+groupitemarray[i]).remove();

        }

        groupitemarray = [];


    }

    function SetDatatable(){

         tblroomtype = $('#tblroomtype').DataTable();

    }

    function ReloadGroupRoomType(){

        tblroomgrouptype.ajax.reload();

    }

    function ReloadRoomType(){

        tblroomtype.ajax.reload();

    }

</script>
@endsection