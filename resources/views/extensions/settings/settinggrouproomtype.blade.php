@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">
                    Points Settings
                    <a href="{{ url("/settings") }}" style="float: right; font-size: 15px; margin-top: 10px;"><i class="fa fa-backward"></i></a>
                </h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-4 row">
                    <label for="cmbroomtype">Room Type</label>
                    <select name="cmbroomtype" id="cmbroomtype" class="form-control"></select>
                </div>
                <div class="col-lg-8 row">

                </div>
                
                <div class="col-lg-12 row">
                     
                    <br>
                     <table id="tblstatuspoints" class="table table-striped table-bordered" style="width: 100%;">
                             <thead>
                                 <tr>
                                     <th>Status</th>
                                     <th>Points</th>
                                     <th></th>
                                 </tr>
                             </thead>
                     </table>

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

    {{-- Modal --}}
    @include('modals.settings.points')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    var tblstatuspoints;
    var group_id = [@foreach($group_id as $val) {{ $val }}, @endforeach];
    var stypeid;
    var sstatusid;

    $(document).ready(function(){

        LoadRoomType();
        SetDatatable();

    });

    $('#cmbroomtype').on('change', function(){

        stypeid = $(this).val();

        tblstatuspoints.destroy();
        tblstatuspoints = $('#tblstatuspoints').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/settings/loadstatuspoints") }}',
                data: {
                    id: stypeid
                },
            },
            columns : [
                {data: 'status', name: 'status'},
                {data: 'points', name: 'points'},
                {data: 'panel', name: 'panel'},
            ]
        });

    });

    $(document).on('click', '#btnpoints', function(){

        sstatusid = $(this).val();

        LoadStatusName();
        LoadStatusPoints();

    });

    $('#btnnsave').on('click', function(){

        var points = $('#txtnpoints').val();

        $.ajax({
            url: '{{ url("api/settings/savepointsinformation") }}',
            type: 'post',
            data: {
                room_type: stypeid,
                room_status: sstatusid,
                points: points
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    $('#points .close').click();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadStatusPoints();

                }

            }
        });

    });

    function LoadStatusPoints(){

        $.ajax({
            url: '{{ url("api/settings/loadpoints") }}',
            type: 'get',
            data: {
                room_type: stypeid,
                room_status: sstatusid
            },
            dataType: 'json',
            success: function(response){

                $('#txtnpoints').val(response.points);

            }
        });

    }

    function LoadStatusName(){

        $.ajax({
            url: '{{ url("api/settings/loadstatusname") }}',
            type: 'get',
            data: {
                id: sstatusid
            },
            dataType: 'json',
            success: function(response){

                $('#modalheader').text('');
                $('#modalheader').text(response.status);

            }
        });

    }

    function LoadRoomType(){

        $.ajax({
            url: '{{ url("api/settings/loadroomtype") }}',
            type: 'get',
            data: {
                group_id: group_id
            },
            dataType: 'json',
            success: function(response){

                $('#cmbroomtype').find('option').remove();
                $('#cmbroomtype').append('<option value="">Select A Room Type</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#cmbroomtype').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["room_type"] +'</option>');

                }

            }
        });

        $('#cmbroomtype').select2({
            theme: 'bootstrap'
        });

    }

    function SetDatatable(){

        tblstatuspoints = $('#tblstatuspoints').DataTable();

    }

    function ReloadStatusPoints(){

        tblstatuspoints.ajax.reload();

    }

</script>
@endsection