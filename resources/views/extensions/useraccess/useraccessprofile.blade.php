@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">
                    Access Profile
                    <a href="{{ url("/useraccess") }}" style="float: right; font-size: 15px;"><i class="fa fa-backward"></i></a></div>
                </h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-6 row">
                    <label for="txtrolename">Role Name</label>
                    <input id="txtrolename" name="txtrolename" class="form-control" type="text" style="margin-bottom: 10px;">
                </div>

                <div class="col-lg-12 row">

                    <ul class="nav nav-tabs nav-justified">
                        <li class="nav-item active">
                          <a class="nav-link" data-toggle="tab" href="#resources"><strong>Resources List</strong></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#roomstatus"><strong>Room Status</strong></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="resources" class="container-fluid tab-pane active"><br>

                            <div class="col-lg-12">

                                <table id="tblresources" class="table table-striped table-bordered" style="width: 100%">
                                    <thead> 
                                        <tr>
                                            <th>Resources</th>
                                            <th>Description</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>

                            </div>

                        </div>
                        <div id="roomstatus" class="container-fluid tab-pane"><br>
                            <div class="form-group col-lg-12">
                                <button id="btnaddroomstatus" name="btnaddroomstatus" class="btn btn-flat btn-primary" style="float: right; margin-right: 15px;"><i class="fa fa-plus"></i></button>
                            </div>
                            <div id="addroomstatuscontent" class="col-md-12">

                                <div class="col-md-3">

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cmbtargetstatus">Target Status</label>
                                        <select id="cmbtargetstatus" name="cmbtargetstatus" class="form-control" style="width: 100%;"></select>
                                    </div>
                                    <div class="form-group">
                                        <label for="cmbnextstatus">Next Status</label>
                                        <select id="cmbnextstatus" name="cmbnextstatus" class="form-control" style="width: 100%;"></select>
                                    </div>
                                    <div class="form-group">
                                        <button id="btnaddallowstatus" name="btnaddallowstatus" class="btn btn-flat btn-primary" style="float: right;">Add</button>
                                    </div>
                                </div>
                                <div class="col-md-3">

                                </div>

                            </div>
                            <div id="roomstatuscontent" class="col-md-12">
                                <table id="tblroomstatus" class="table table-striped table-bordered" style="width: 100%">
                                    <thead> 
                                        <tr>
                                            <th>Status</th>
                                            <th>Allowed Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            
                                 
                        </div>
                    </div>

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

    {{-- Modal --}}
    @include('modals.useraccess.useraccessroomstatus')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    var id = "{{ $id }}";
    var tblresources;
    var tblroomstatus;
    var addallowstatus = false;
    
    $(document).ready(function(){

       SetSelect2();
       LoadRoleName();
       LoadResources();
       LoadRoomStatus();

       //Set
       $('#addroomstatuscontent').hide();

    });

    $('#btnaddroomstatus').on('click', function(){

        if(addallowstatus){
            $('#addroomstatuscontent').hide();
            $('#roomstatuscontent').show();
            addallowstatus = false;
        }
        else{

            //Load
            LoadSelectRoomStatus($('#cmbtargetstatus').attr('id'));

            //Set
            $('#addroomstatuscontent').show();
            $('#roomstatuscontent').hide();
            addallowstatus = true;

        }

    });

    $('#cmbtargetstatus').on('change', function(){

        var statusid = $(this).val();
        
        $.ajax({
            url: '{{ url("api/useraccessprofile/loadroomstatus") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbnextstatus').find('option').remove();
                $('#cmbnextstatus').append('<option value="">Select Next Status</option>');
                for(var i=0;i<response.data.length;i++){

                    if(response.data[i]["id"]!=statusid){
                        $('#cmbnextstatus').append('<option value="'+response.data[i]["id"]+'">'+response.data[i]["room_status"]+'</option>');
                    }
                   
                }

            }
        });

    });

    $('#btnaddallowstatus').on('click', function(){

        var targetstatus = $('#cmbtargetstatus').val();
        var nextstatus = $('#cmbnextstatus').val();

        if(targetstatus==null){
            toastr.error("Please select a target status.", '', { positionClass: 'toast-top-center' });
        }
        else if(nextstatus==null){
            toastr.error("Please select a next status.", '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/useraccessprofile/savetargetstatusinfo") }}',
                type: 'post',
                data: {
                    id: id,
                    targetstatus: targetstatus,
                    nextstatus: nextstatus,
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#btnaddroomstatus').click();
                        ReloadRoomStatus();
                        ClearNewTargetStatus();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        
                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

    });

    $(document).on('click', '#btnremoveroomstatus', function(){

        var statusid = $(this).val();

        $.ajax({
            url: '{{ url("api/useraccessprofile/loadallowstatusprofile") }}',
            type: 'get',
            data: {
                id: id,
                statusid: statusid
            },
            dataType: 'json',
            beforeSend: function(){

                $('#useraccessmodaldocument').waitMe({

                    effect : 'roundBounce',
                    text : '',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000'

                });

            },
            success: function(response){

                $('#allowstatusitems').find('tr').remove();
                $('#txturoomstatus').val(response.status);
                if(response.data.length==0){
                    $('#allowstatusitems').append('<tr style="text-align: center;"><td colspan="2">No Data</td></tr>');
                }
                else{

                    for(var i=0;i<response.data.length;i++){

                         $('#allowstatusitems').append('<tr id="'+response.data[i]["id"]+'"><td>'+response.data[i]["room_status"]+'</td><td><button id="btnremove" name="btnremove" class="btn btn-flat btn-danger" value="'+response.data[i]["id"]+'"><i class="fa fa-trash"></i></button></td></tr>');

                    }

                }

            },
            complete: function(){

                $('#useraccessmodaldocument').waitMe('hide');

            }
        });

    });

    $(document).on('click', '#btnremove', function(){

        var accessid = $(this).val();
        RemoveAllowStatus(accessid);
        
    });

    $(document).on('change', '[name="chkenabled[]"]', function(){

        var resources = $(this).val();
        
        if($('#' + resources).is(":checked")){
            
            $.ajax({
                url: '{{ url("api/useraccessprofile/resourcesenabled") }}',
                type: 'post',
                data: {
                    id: id,
                    resources: resources
                },
                dataType: 'json',
                success: function(){
                    
                }
            });

        }
        else{

            $.ajax({
                url: '{{ url("api/useraccessprofile/resourcesdisabled") }}',
                type: 'post',
                data: {
                    id: id,
                    resources: resources
                },
                dataType: 'json',
                success: function(){
                    
                }
            });
            
        }

    });

    function RemoveAllowStatus(accessid){

        $.ajax({
            url: '{{ url("api/useraccessprofile/removeallowstatus") }}',
            type: 'post',
            data: {
                id: accessid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    ReloadRoomStatus();
                    $('#'+accessid).remove();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

    }

    function LoadSelectRoomStatus(id){

        $.ajax({
            url: '{{ url("api/useraccessprofile/loadroomstatus") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select Target Status</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#'+id).append('<option value="'+response.data[i]["id"]+'">'+response.data[i]["room_status"]+'</option>');
                }

            }
        });

    }

    function LoadResources(){

        tblresources = $('#tblresources').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/useraccessprofile/loadallowresources") }}',
                data: {
                    id: id
                },
            },
            columns : [
                {data: 'resources', name: 'resources'},
                {data: 'description', name: 'description'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    function LoadRoomStatus(){

        tblroomstatus = $('#tblroomstatus').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/useraccessprofile/loadallowstatus") }}',
                data: {
                    id: id
                },
            },
            columns : [
                {data: 'status', name: 'status'},
                {data: 'allowstatus', name: 'allowstatus'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    function ClearNewTargetStatus(){

        $('#cmbtargetstatus').find('option').remove();
        $('#cmbnextstatus').find('option').remove();

    }

    function ReloadRoomStatus(){

        tblroomstatus.ajax.reload();

    }

    function LoadRoleName(){

        $.ajax({
            url: '{{ url("api/useraccessprofile/loadrolename") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#txtrolename').val(response.role);

            }
        });

    }

    function SetSelect2(){

        $('#cmbtargetstatus').select2({
            theme: 'bootstrap'
        });

        $('#cmbnextstatus').select2({
            theme: 'bootstrap'
        });

    }

</script>
@endsection