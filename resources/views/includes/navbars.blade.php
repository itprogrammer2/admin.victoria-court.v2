<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{-- <a class="navbar-brand" href="#"></a> --}}
        </div>
        <!-- /.navbar-header -->
        <div class="nav navbar-top-links navbar-left" style="margin-left: 10px;">
            <a href="{{ url("/main") }}"><img src="{{ asset("images/VCLogo.png") }}" alt="Logo" style="width: 250px; height: 45px;"></a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a class="nav-link" href="#" id="btnlogout"><i class="fa fa-sign-out"></i>Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <h5><strong>Navigation</strong></h5>
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="{{ url("/main") }}"><i class="fa fa-tasks"></i> Overview</a>
                    </li>
                    {{-- <li>
                        <a href="#"><i class="fa fa-money"></i> Expenses<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Upload Expenses</a>
                            </li>
                            <li>
                                <a href="#">Expenses Settings</a>
                            </li>
                        </ul>
                    </li> --}}
                    {{-- <li>
                        <a href="#"><i class="fa fa-money"></i> Budgets<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Upload Budgets</a>
                                </li>
                            </ul>
                    </li> --}}
                    <li>
                        <a href="{{ url("/useraccess") }}"><i class="fa fa-user"></i> User Access</a>
                    </li>
                    <li>
                        <a href="{{ url("/areas") }}"><i class="fa fa-location-arrow"></i> Area</a>
                    </li>
                    <li>
                        <a href="{{ url("/rooms") }}"><i class="fa fa-tasks"></i> Rooms</a>
                    </li>
                    <li>
                        <a href="{{ url("/roomstatus") }}"><i class="fa fa-tasks"></i> Room Status</a>
                    </li>
                    <li>
                        <a href="{{ url("/vehicle") }}"><i class="fa fa-truck"></i> Vehicles</a>
                    </li>
                    <li>
                        <a href="{{ url("/reservation") }}"><i class="fa fa-book"></i> Reservation</a>
                    </li>
                    <li>
                        <a href="{{ url("/inventory") }}"><i class="fa fa-dropbox"></i> Inventory</a>
                    </li>
                    <li>
                        <a href="{{ url("/nationality") }}"><i class="fa fa-users"></i> Nationality</a>
                    </li>
                    <li>
                        <a href="{{ url("/event") }}"><i class="fa fa-tasks"></i> Event</a>
                    </li>
                    <li>
                        <a href="{{ url('/kiosk') }}"><i class="fa fa-tasks"></i> Kiosk</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-eye"></i> Eagle Eye<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url("/eagleye") }}">Room Inspection</a>
                                </li>
                                <li>
                                    <a href="{{ url("/components") }}">Components</a>
                                </li>
                                <li>
                                    <a href="{{ url("/standard") }}">Standard</a>
                                </li>
                                <li>
                                    <a href="{{ url("/remarks") }}">Remarks</a>
                                </li>
                                <li>
                                    <a href="{{ url("/findings") }}">Findings</a>
                                </li>
                                <li>
                                    <a href="{{ url("/checklist") }}">Checklist</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    <li>
                    <li>
                        <a href="#"><i class="fa fa-bell"></i> Notifications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url("/blocklistadd") }}"> Blocklist</a>
                                </li>
                                <li>
                                    <a href="{{ url("/notifgroupadd") }}"> Group Add</a>
                                </li>
                                <li>
                                    <a href="{{ url("/notificationmembershipadd") }}"> Membership Add</a>
                                </li>
                                <li>
                                    <a href="{{ url("/notificationschedule") }}"> Schedule</a>
                                </li>
                            </ul>
                    </li>
                    <li>
                        <a href="{{ url("/settings") }}"><i class="fa fa-cog"></i> Settings</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

<script>

 $('#btnlogout').on('click', function(){

    $.ajax({
        url: '{{ url("api/login/logoutuser") }}',
        type: 'post',
        success: function(){
             window.location = "{{ url('/login') }}";
        }
     });

 });

</script>
