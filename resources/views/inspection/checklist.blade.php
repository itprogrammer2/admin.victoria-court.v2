@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Checklist</h1>
            </div>

            <div class="col-lg-12">
                <button id="btnnewchecklist" name="btnnewchecklist" class="btn btn-flat btn-success" style="margin-bottom: 15px;" data-toggle="modal" data-target="#newchecklist"><i class="fa fa-plus"></i> Add Checklist</button>
            </div>

            <div class="col-lg-12">

                <table id="tblchecklist" class="table table-striped table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Area</th>
                            <th>Component</th>
                            <th>Standard</th>
                            <th>Remarks</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

    {{-- Modal --}}
    @include('modals.checklist.newchecklist')
    @include('modals.checklist.updatechecklist')


</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>


    //Variables
    var tblchecklist;
    var slinkid;

    //Loading Variables
    // var standardisloading = false;
    // var remarksisloading = false;
    // var findingtypeisloading = false;

    $(document).ready(function(){

        LoadChecklist();
        SetSelect2();

    });

    $('#btnnewchecklist').on('click', function(){

        //Clear
        ClearNewChecklist();

        //Load
        LoadAreas();
        LoadComponents();
        LoadStandards();

    });

    $('#cmbnstandards').on('change', function(){

        var id = $(this).val();

        $.ajax({
            url: '{{ url("api/checklist/loadremarks") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#cmbnremarks').find('option').remove();
                $('#cmbnremarks').append('<option value="">Select A Remarks</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbnremarks').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                }

            }
        });

    });


    $('#cmbnremarks').on('change', function(){

        LoadFindingType();

    });


    $('#btnsavechecklist').on('click', function(){

        var area = $('#cmbnareas').val();
        var component = $('#cmbncomponents').val();
        var standard = $('#cmbnstandards').val();
        var remark = $('#cmbnremarks').val();
        var findingtype = $('#cmbnfindingtype').val();

        if(area==""){
            // toastr.error('Please select a area.', '', { positionClass: 'toast-top-center' });
            area = 0;
        }
        else if(component==""){
            component = 0;
            // toastr.error('Please select a component for the area', '', { positionClass: 'toast-top-center' });
        }
        else if(standard==""){
            toastr.error('Please select a standard.', '', { positionClass: 'toast-top-center' });
        }
        else if(remark==""){
            toastr.error('Please select a remarks for the standard', '', { positionClass: 'toast-top-center' });
        }
        else if(findingtype==""){
            toastr.error('Please select a finding type', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/checklist/savechecklistinformation") }}',
                type: 'post',
                data: {
                    area: area,
                    component: component,
                    standard: standard,
                    remark: remark,
                    findingtype: findingtype
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#newchecklist .close').click();
                        ReloadChecklist();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }


    });

    $(document).on('click', '#btnedit', function(){

        slinkid = $(this).val();
        GetChecklistInformation(slinkid);

    });

    $('#cmbustandards').on('change', function(){

        var id = $(this).val();

        $.ajax({
            url: '{{ url("api/checklist/loadremarks") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#cmburemarks').find('option').remove();
                $('#cmburemarks').append('<option value="">Select A Remarks</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmburemarks').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                }

            },
            complete: function(){

                $('#cmbufindingtype').find('option').remove();

            }
        });

    });

    $('#cmburemarks').on('change', function(){

        var remarksid = $(this).val();

        $.ajax({
            url: '{{ url("api/checklist/loadfindingtype") }}',
            type: 'get',
            data: {
                remarksid: remarksid
            },
            dataType: 'json',
            success: function(response){

                    $('#cmbufindingtype').find('option').remove();
                    $('#cmbufindingtype').append('<option value="">Select A Finding Type</option>');
                    for(var i=0;i<response.data.length;i++){
                        $('#cmbufindingtype').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                    }

                    $('#cmbufindingtype').select2({
                        theme: 'bootstrap'
                    });

            }
        });

    });

    $('#btnupdatechecklist').on('click', function(){

        var area = $('#cmbuareas').val();
        var components = $('#cmbucomponents').val();
        var standard = $('#cmbustandards').val();
        var remarks = $('#cmburemarks').val();
        var findingtype = $('#cmbufindingtype').val();

        if(area==""){
            area = 0;
        }
        else if(components==""){
            components = 0;
        }
        else if(standard==""){
            toastr.error('Please select a standard.', '', { positionClass: 'toast-top-center' });
        }
        else if(remarks==""){
            toastr.error('Please select a remarks for the standard', '', { positionClass: 'toast-top-center' });
        }
        else if(findingtype==""){
            toastr.error('Please select a finding type', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/checklist/updatechecklistinformation") }}',
                type: 'post',
                data: {
                    linkid: slinkid,
                    area: area,
                    components: components,
                    standard: standard,
                    remarks: remarks,
                    findingtype: findingtype
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#updatechecklist .close').click();
                        ReloadChecklist();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            })

        }



    });

    $(document).on('click', '#btndelete', function(){

        slinkid = $(this).val();

        $.confirm({
              title: 'Delete',
              content: 'Delete This Checklist Information?',
              type: 'blue',
              buttons: {
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        DeleteChecklist(slinkid);

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){



                      }
                  }
              }
          });

    });

    $('#btnupdatechecklist').on('click', function(){

        var area = $('#cmbuareas').val();
        var components = $('#cmbucomponents').val();
        var standard = $('#cmbustandards').val();
        var remarks = $('#cmburemarks').val();
        var findingtype = $('#cmbufindingtype').val();

        if(area==""){
            area = 0;
        }
        else if(components==""){
            components = 0;
        }
        else if(standard==""){
            toastr.error('Please select a standard.', '', { positionClass: 'toast-top-center' });
        }
        else if(remarks==""){
            toastr.error('Please select a remarks for the standard', '', { positionClass: 'toast-top-center' });
        }
        else if(findingtype==""){
            toastr.error('Please select a finding type', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/checklist/updatechecklistinformation") }}',
                type: 'post',
                data: {
                    linkid: slinkid,
                    area: area,
                    components: components,
                    standard: standard,
                    remarks: remarks,
                    findingtype: findingtype
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#updatechecklist .close').click();
                        ReloadChecklist();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            })

        }



    });

    function DeleteChecklist(linkid){

        $.ajax({
            url: '{{ url("api/checklist/deletechecklistinformation") }}',
            type: 'post',
            data: {
                linkid: linkid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    ReloadChecklist();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        })

    }

    function GetChecklistInformation(linkid){

        $.ajax({
            url: '{{ url("api/checklist/getchecklistinformation") }}',
            type: 'get',
            data: {
                linkid: linkid
            },
            dataType: 'json',
            success: function(response){

                console.log(response);
                SetAreaInfo(response.area_id);
                SetCompenentInfo(response.component_id);
                SetStandardInfo(response.standard_id, response.remarks_id, response.finding_type_id);

            }
        })

    }

    function LoadChecklist(){

        tblchecklist = $('#tblchecklist').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/checklist/loadchecklist") }}'
            },
            columns : [
                {data: 'area', name: 'area'},
                {data: 'component', name: 'component'},
                {data: 'standard', name: 'standard'},
                {data: 'remarks', name: 'remarks'},
                {data: 'panel', name: 'panel'}
            ]
        });

    }

    function LoadAreas(){

        $.ajax({
            url: '{{ url("api/checklist/loadareas") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbnareas').find('option').remove();
                $('#cmbnareas').append('<option value="">Select A Area</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbnareas').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                }

                $('#cmbnareas').select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function LoadComponents(){

        $.ajax({
            url: '{{ url("api/checklist/loadcomponents") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbncomponents').find('option').remove();
                $('#cmbncomponents').append('<option value="">Select A Components</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbncomponents').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                }

                $('#cmbncomponents').select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function LoadStandards(){

        $.ajax({
            url: '{{ url("api/checklist/loadstandards") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbnstandards').find('option').remove();
                $('#cmbnstandards').append('<option value="">Select A Standards</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbnstandards').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                }

                $('#cmbnstandards').select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function LoadFindingType(){

        var remarksid = $('#cmbnremarks').val();

        $.ajax({
           url: '{{ url("api/checklist/loadfindingtype") }}',
           type: 'get',
           data: {
            remarksid: remarksid
           },
           dataType: 'json',
           success: function(response){

                $('#cmbnfindingtype').find('option').remove();
                $('#cmbnfindingtype').append('<option value="">Select A Finding Type</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbnfindingtype').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +', '+ response.data[i]["points"] +'</option>');
                }

                $('#cmbnfindingtype').select2({
                    theme: 'bootstrap'
                });

           }
        });

    }

    function SetAreaInfo(areaid){

        $.ajax({
            url: '{{ url("api/checklist/loadareas") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbuareas').find('option').remove();
                $('#cmbuareas').append('<option value="">Select A Area</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbuareas').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                }

                $('#cmbuareas').select2({
                    theme: 'bootstrap'
                });

            },
            complete: function(){

                $('#cmbuareas').val(areaid).trigger('change');

            }
        });

    }

    function SetCompenentInfo(componentid){

        $.ajax({
            url: '{{ url("api/checklist/loadcomponents") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbucomponents').find('option').remove();
                $('#cmbucomponents').append('<option value="">Select A Components</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbucomponents').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                }

                $('#cmbucomponents').select2({
                    theme: 'bootstrap'
                });

            },
            complete: function(){

               $('#cmbucomponents').val(componentid).trigger('change');

            }
        });

    }

    function SetStandardInfo(standardid, remarksid, findingtypeid){

        $.ajax({
            url: '{{ url("api/checklist/loadstandards") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbustandards').find('option').remove();
                $('#cmbustandards').append('<option value="">Select A Standards</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbustandards').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                }



            },
            complete: function(){

                $('#cmbustandards').val(standardid);

                $('#cmbustandards').select2({
                    theme: 'bootstrap'
                });

                SetRemarksInfo(remarksid, standardid, findingtypeid);

            }
        });

    }

    function SetRemarksInfo(remarksid, id, fid){

        $.ajax({
            url: '{{ url("api/checklist/loadremarks") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#cmburemarks').find('option').remove();
                $('#cmburemarks').append('<option value="">Select A Remarks</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmburemarks').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');
                }

            },
            complete: function(){

                $('#cmburemarks').val(remarksid);

                $('#cmburemarks').select2({
                    theme: 'bootstrap'
                });

                SetFindingType(fid, remarksid);


            }
        });

    }

    function SetFindingType(findingtypeid, id){

        $.ajax({
           url: '{{ url("api/checklist/loadfindingtype") }}',
           type: 'get',
           data: {
            remarksid: id
           },
           dataType: 'json',
           success: function(response){

                $('#cmbufindingtype').find('option').remove();
                $('#cmbufindingtype').append('<option value="">Select A Finding Type</option>');
                for(var i=0;i<response.data.length;i++){
                    $('#cmbufindingtype').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +', '+ response.data[i]["points"] +'</option>');
                }


           },
           complete: function(){

            $('#cmbufindingtype').val(findingtypeid);

            $('#cmbufindingtype').select2({
                theme: 'bootstrap'
            });

           }
        });

    }

    function ClearNewChecklist(){

        $('#cmbnremarks').find('option').remove();

    }

    function SetSelect2(){

        $('#cmbnremarks').select2({
            theme: 'bootstrap'
        });

    }

    function ReloadChecklist(){

        tblchecklist.ajax.reload();

    }

</script>
@endsection
