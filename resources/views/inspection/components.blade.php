@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Components</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12 row">

                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#components" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Components</button>

                </div>

                <div class="col-lg-12 row">

                    <table id="roomComponents" class="table table-striped table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Component</th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

     {{-- Modal --}}
     @include('modals.inspections.components')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    var roomcomponents;
	
	$(document).ready(function(){
		RoomComponents();
	});

	$('#saveComponents').on('click', function(){
		var componentLocation = $('#componentLocation').val();
		// console.log(componentLocation);
		$.ajax({
			url: '{{ url("api/inspection/component") }}',
			method: 'POST',
			dataType: 'json',
			data:{
				'component': componentLocation,
			},
			success:function(r){

				if(r.success){

					ReloadRoomComponent();
					toastr.success(r.message, '', { positionClass: 'toast-top-center' });

				}
				else{

					toastr.error(r.message, '', { positionClass: 'toast-top-center' });

				}
				
			}
		});
	});

	function RoomComponents(){
		roomcomponents = $('#roomComponents').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/component/getInfo") }}',
			},
			columns : [
				{data: 'name', name: 'name'},
			]
		});
	}

    function ReloadRoomComponent(){

        roomcomponents.ajax.reload();

    }

</script>
@endsection