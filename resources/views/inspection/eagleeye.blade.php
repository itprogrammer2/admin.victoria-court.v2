@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Room Inspection</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12 row">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addRoomInspection" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Room</button>
                </div>

                <div class="col-lg-12 row">

                    <table id="roomInspection" class="table table-striped table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Room</th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

    {{-- Modal --}}
    @include('modals.inspections.room')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    var roomtbl;

    $(document).ready(function(){
		reloadInpsectionRoom();
	});

	$('#saveLocation').on('click', function(){

		var inspectionlocation = $('#inspectionlocation').val();

		if(inspectionlocation==""){

			toastr.error('Please input the area name.', '', { positionClass: 'toast-top-center' });
            $('#inspectionlocation').focus();

		}
		else{

			$.ajax({
				url: '{{ url("api/inspection/room") }}',
				method: 'POST',
				dataType: 'json',
				data:{
					'room': inspectionlocation,
				},
				success:function(r){

					toastr.success(r.message, '', { positionClass: 'toast-top-center' });
					ReloadRoomInspection();

				},
				error:function(r){
					console.log(r);
				}
			});

		}
		
		
	});

	function reloadInpsectionRoom(){

		roomtbl = $('#roomInspection').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/room/getInfo") }}',
			},
			columns: [
				{data: 'name', name: 'name'},
			]
		});
		
	}

    function ReloadRoomInspection(){

        roomtbl.ajax.reload();

    }

</script>
@endsection