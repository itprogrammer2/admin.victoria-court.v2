@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Findings</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12 row">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#finding" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Finding Type</button>
                </div>

                <div class="col-lg-12 row">
                    <table id="tblinspectionfinding" class="table table-striped table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                {{-- <th></th> --}}
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

    {{-- Modal --}}
    @include('modals.inspections.finding')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    var tblinspectionfinding;

	$(document).ready(function(){
		reloadFindings();
	});

	$('#saveFinding').on('click', function(){
		var inspectionFinding = $('#inspectionFinding').val();

		$.ajax({
			url: '{{ url("api/inspection/finding") }}',
			method: 'POST',
			dataType: 'json',
			data:{
				'inspectionFinding': inspectionFinding,
			},
			success: function(r){
                ReloadFindingsInformation();
				toastr.success(r.message, '', { positionClass: 'toast-top-center' });
			}
		});

	});

	function reloadFindings(){
		tblinspectionfinding = $('#tblinspectionfinding').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/finding/getInfo") }}'
			},
			columns: [
				{data: 'name', name: 'name'},
				// {data: 'description', name: 'description'},
			]
		});
	}

    function ReloadFindingsInformation(){

        tblinspectionfinding.ajax.reload();

    }

</script>
@endsection
