@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Remarks</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12 row">
                    <button id="btnaddremarks" name="btnaddremarks" type="button" class="btn btn-success" data-toggle="modal" data-target="#remarks" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Remarks</button>
                </div>

                <div class="col-lg-12 row">

                    <table id="tblinspectionremarks" class="table table-striped table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                {{-- <th>Area</th> --}}
                                <th>Standard</th>
                                <th>Remarks</th>
                                <th>Finding Type</th>
                                <th>Points</th>
                                <th></th>
                                {{-- <th></th> --}}
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

    {{-- Modal --}}
    @include('modals.inspections.remarks')
    @include('modals.inspections.remarksupdate')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    //Variables
    var tblinspectionremarks;
    var sremarksid;

    //Variables New
    var itemncount = 1;
    var itemnarray = new Array();

    //Variables Update
    var itemucount = 1;
    var itemuarray = new Array();
    var itemdarray = new Array();

	$(document).ready(function(){

		reloadRemarks();

	});

    $('#btnaddremarks').on('click', function(){

       LoadStandards($('#cmbstandard').attr('id'));
       LoadFindingTypes($('#cmbfindingtype1').attr('id'));
       ClearNewRemarks();
       ClearNewFindings();

    });

    $('#btnnadd').on('click', function(){

        itemncount += 1;
        itemnarray.push(itemncount);

        $('#findingtypenewcontent').append('<tr id="new'+itemncount+'"><td><select class="form-control" id="cmbfindingtype'+itemncount+'" name="cmbfindingtype[]"></select></td><td><input type="text" id="txtpoints'+itemncount+'" name="txtpoints[]" class="form-control"></td><td><button id="btnndelete" name="btnndelete" class="btn btn-flat btn-danger" value="'+itemncount+'"><i class="fa fa-trash"></i></button></td></tr>');

        LoadFindingTypes("cmbfindingtype"+itemncount);

    });

    $(document).on('click', '#btnndelete', function(){

        var id = $(this).val();
        $('#new'+id).remove();

    });

    function ClearNewFindings(){

        for(var i=0;i<itemnarray.length;i++){
            $('#new'+itemnarray[i]).remove();
        }
        itemncount = 1;
        itemnarray = [];

    }

    function ClearUpdateFindings(){

        for(var i=0;i<itemuarray.length;i++){
            $('#update'+itemuarray[i]).remove();
        }
        itemucount = 1;
        itemuarray = [];
        itemdarray = [];

    }

	$('#saveRemarks').on('click', function(){

        var standard = $('#cmbstandard').val();
		var inspectionremarks = $('#inspectionremarks').val();
        var findingtype = $('[name="cmbfindingtype[]"]').serializeArray();
        var points = $('[name="txtpoints[]"]').serializeArray();

		$.ajax({
			url: '{{ url("api/inspection/remarks") }}',
			method: 'post',
			dataType: 'json',
			data:{
				inspectionremarks: inspectionremarks,
                standard: standard,
                findingtype: findingtype,
                points: points
			},
			success: function(r){

                if(r.success){

                    ReloadRemarksInformation();
				    toastr.success(r.message, '', { positionClass: 'toast-top-center' });

                }
                else{

                    toastr.error(r.message, '', { positionClass: 'toast-top-center' });

                }

			}
		});

	});

    $(document).on('click', '#btnedit', function(){

        sremarksid = $(this).val();
        ClearUpdateFindings();
        LoadRemarksInformations();

    });

    $('#btnuadd').on('click', function(){


        itemuarray.push(itemucount);

        $('#findingtypeupdatecontent').append('<tr id="update'+itemucount+'"><td><input type="text" id="txtuitemid'+itemucount+'" name="txtuitemid[]" value="0" hidden="true"><select class="form-control" id="cmbufindingtype'+itemucount+'" name="cmbufindingtype[]"></select></td><td><input type="text" id="txtupoints'+itemucount+'" name="txtupoints[]" class="form-control"></td><td><button id="btnudelete" name="btnudelete" class="btn btn-flat btn-danger" value="'+itemucount+'"><i class="fa fa-trash"></i></button></td></tr>');

        LoadFindingTypes("cmbufindingtype"+itemucount);

        itemucount += 1;

    });

    $(document).on('click', '#btnudelete', function(){

        var id = $('#txtuitemid'+$(this).val()).val();
        itemdarray.push(id);

        $('#update'+$(this).val()).remove();

    });

    $('#btnupdate').on('click', function(){

        UpdateRemarksInformation();

    });

    function UpdateRemarksInformation(){

        var standard = $('#cmbustandard').val();
        var remarks = $('#inspectionuremarks').val();
        var findingtype = $('[name="cmbufindingtype[]"]').serializeArray();
        var points = $('[name="txtupoints[]"]').serializeArray();
        var updateitems = $('[name="txtuitemid[]"]').serializeArray();

        $.ajax({
            url: '{{ url("api/inspection/remarks/updateremarksinformation") }}',
            type: 'post',
            data: {
                id: sremarksid,
                standard: standard,
                inspectionremarks: remarks,
                findingtype: findingtype,
                points: points,
                updateitems: updateitems,
                itemdarray: itemdarray
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    ReloadRemarksInformation();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    $('#remarksupdate .close').click();

                }

            }
        });

    }

    function LoadRemarksInformations(){

        $.ajax({
            url: '{{ url("api/inspection/remarks/loadremarksinformation") }}',
            type: 'get',
            data: {
                id: sremarksid
            },
            dataType: 'json',
            success: function(response){

                SetInformationStandard($('#cmbustandard').attr('id'), response.standard_id);
                $('#inspectionuremarks').val(response.remarks_name);

                for(var i=0;i<response.deductioninfo.length;i++){

                    itemuarray.push(itemucount);

                    if(i==0){
                        $('#findingtypeupdatecontent').append('<tr id="update'+itemucount+'"><td><input type="text" id="txtuitemid'+itemucount+'" name="txtuitemid[]" hidden="true"><select class="form-control" id="cmbufindingtype'+itemucount+'" name="cmbufindingtype[]"></select></td><td><input type="text" id="txtupoints'+itemucount+'" name="txtupoints[]" class="form-control"></td><td></td></tr>');
                    }
                    else{
                        $('#findingtypeupdatecontent').append('<tr id="update'+itemucount+'"><td><input type="text" id="txtuitemid'+itemucount+'" name="txtuitemid[]" hidden="true"><select class="form-control" id="cmbufindingtype'+itemucount+'" name="cmbufindingtype[]"></select></td><td><input type="text" id="txtupoints'+itemucount+'" name="txtupoints[]" class="form-control"></td><td><button id="btnudelete" name="btnudelete" class="btn btn-flat btn-danger" value="'+itemucount+'"><i class="fa fa-trash"></i></button></td></tr>');
                    }


                    SetInformationFindingTypes("txtuitemid"+itemucount, "cmbufindingtype"+itemucount, "txtupoints"+itemucount, response.deductioninfo[i]["tbl_findings_type_id"], response.deductioninfo[i]["points"], response.deductioninfo[i]["id"]);

                    itemucount += 1;

                }


            }
        });

    }

    function SetInformationFindingTypes(itemid ,findingid, pointsid, finding, points, item){

        $.ajax({
            url: '{{ url("api/inspection/remarks/loadfindingtypes") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+findingid).find('option').remove();
                $('#'+findingid).append('<option value="">Select a finding type</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#'+findingid).append('<option value="'+response.data[i]["id"]+'">'+response.data[i]["name"]+'</option>');

                }

                $('#'+findingid).select2({
                    theme: 'bootstrap'
                });

            },
            complete: function(){

                $('#'+findingid).val(finding).trigger('change');
                $('#'+pointsid).val(points);
                $('#'+itemid).val(item);

            }
        })

    }

    function SetInformationStandard(id, data){

        $.ajax({
            url: '{{ url("api/inspection/remarks/getstandards") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select a standard</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#'+id).append('<option value="'+response.data[i]["id"]+'">'+response.data[i]["name"]+'</option>');

                }

            },
            complete: function(){

                $("#"+id).val(data).trigger('change');

            }
        });

        $('#'+id).select2({
            theme: 'bootstrap'
        });

    }

	function reloadRemarks(){

		tblinspectionremarks = $('#tblinspectionremarks').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/remarks/getInfo") }}'
			},
			columns: [
                // {data: 'area', name: 'area'},
				{data: 'standard', name: 'standard'},
                {data: 'remarks', name: 'remarks'},
                {data: 'findingtype', name: 'findingtype'},
                {data: 'points', name: 'points'},
                {data: 'panel', name: 'panel'}
				// {data: 'description', name: 'description'},
			]
		});

	}

    function LoadFindingTypes(id){

        $.ajax({
            url: '{{ url("api/inspection/remarks/loadfindingtypes") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select a finding type</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#'+id).append('<option value="'+response.data[i]["id"]+'">'+response.data[i]["name"]+'</option>');

                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            }
        })

    }

    function LoadStandards(id){

        $.ajax({
            url: '{{ url("api/inspection/remarks/getstandards") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select a standard</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#'+id).append('<option value="'+response.data[i]["id"]+'">'+response.data[i]["name"]+'</option>');

                }

            }
        });

        $('#'+id).select2({
            theme: 'bootstrap'
        });

    }

    function ClearNewRemarks(){

        $('#inspectionremarks').val('');

    }

    function ReloadRemarksInformation(){

        tblinspectionremarks.ajax.reload();

    }

</script>
@endsection
