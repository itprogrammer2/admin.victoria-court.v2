@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Standard</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12 row">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#standard" style="margin-bottom: 10px;"><i class="fa fa-plus"></i> Add Standard</button>
                </div>

                <div class="col-lg-12 row">

                    <table id="inpectionStandard" class="table table-striped table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                    </table>

                </div>
    
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

     {{-- Modal --}}
     @include('modals.inspections.standards')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    var standardTable;

	$(document).ready(function(){
		reloadStandard();
	});

	$('#saveStandard').on('click', function(){
		var standardName = $('#standardName').val();
		var standardDescription = $('#standardDescription').val();
		// console.log(standardName + " " + standardDescription);
		$.ajax({
			url: '{{ url("/api/inspection/standard") }}',
			method: 'POST',
			dataType: 'json',
			data:{
				'standardName': standardName,
				'standardDescription': standardDescription,
			},
			success: function(r){
				ReloadStandardInformation();
				toastr.success(r.message, '', { positionClass: 'toast-top-center' });
				console.log(r);
			}	
		});
	});

	function reloadStandard(){
		standardTable = $('#inpectionStandard').DataTable({
			processing: true,
			serverSide: true,
			ajax:{
				type: 'get',
				url: '{{ url("api/inspection/standard/getInfo") }}'
			},
			columns: [
				{data: 'name', name: 'name'},
				{data: 'description', name: 'description'},
			]
		});
	}

    function ReloadStandardInformation(){

        standardTable.ajax.reload();

    }

</script>
@endsection