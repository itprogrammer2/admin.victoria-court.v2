@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Kiosk Restriction</h1>
            </div>

            <div class="col-lg-12">

                <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a data-toggle="tab" href="#web">Web</a></li>
                    <li><a data-toggle="tab" href="#mobile">Mobile</a></li>
                </ul>
                  
                <div class="tab-content">
                    <div id="web" class="tab-pane fade in active">
                            
                        <br>
                        <div class="col-lg-12">
                            <table id="tblkioskrestrictionweb" class="table table-striped table-bordered" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Role</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                            </table>
                        </div>

                    </div>
                    <div id="mobile" class="tab-pane fade">

                        <br>
                        <div class="col-lg-12">
                            <table id="tblkioskrestrictionmobile" class="table table-striped table-bordered" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Role</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    //Variables
    var tblkioskrestrictionweb;
    var tblkioskrestrictionmobile;

    $(document).ready(function(){

        LoadKioskRestrictionWeb();
        LoadKioskRestrictionMobile();

    });

    $(document).on('change', '[name="chkenabledweb[]"]', function(){

        var role = $(this).val();
        
        if($('#web' + role).is(":checked")){
          
          $.ajax({
              url: '{{ url("api/kiosk/restrictionenabledweb") }}',
              type: 'post',
              data: {
                  role: role
              },
              dataType: 'json',
              success: function(){

              }
          });

        }
        else{

          $.ajax({
              url: '{{ url("api/kiosk/restrictiondisabledweb") }}',
              type: 'post',
              data: {
                  role: role
              },
              dataType: 'json',
              success: function(){
                    
              }
          });
            
        }

    });

    $(document).on('change', '[name="chkenabledmobile[]"]', function(){

        var role = $(this).val();
        
        if($('#mobile' + role).is(":checked")){
          
          $.ajax({
              url: '{{ url("api/kiosk/restrictionenabledmobile") }}',
              type: 'post',
              data: {
                  role: role
              },
              dataType: 'json',
              success: function(){

              }
          });

        }
        else{

          $.ajax({
              url: '{{ url("api/kiosk/restrictiondisabledmobile") }}',
              type: 'post',
              data: {
                  role: role
              },
              dataType: 'json',
              success: function(){
                    
              }
          });
            
        }

    });

    function LoadKioskRestrictionWeb(){

         tblkioskrestrictionweb = $('#tblkioskrestrictionweb').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/kiosk/loadrestrictionsweb") }}',
            },
            columns : [
                {data: 'role', name: 'role'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

    function LoadKioskRestrictionMobile(){

        tblkioskrestrictionmobile = $('#tblkioskrestrictionmobile').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/kiosk/loadrestrictionsmobile") }}',
            },
            columns : [
                {data: 'role', name: 'role'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

</script>
@endsection