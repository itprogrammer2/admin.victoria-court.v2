<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <title>Victoria Court Admin</title>

    <link rel="stylesheet" href="{{ asset("css/bootstrap.css") }}">
    <link rel="stylesheet" href="{{ asset("css/metisMenu.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/dataTables.bootstrap.css") }}">
    <link rel="stylesheet" href="{{ asset("css/dataTables.responsive.css") }}">
    <link rel="stylesheet" href="{{ asset("css/toastr.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/waitMe.css") }}">
    <link rel="stylesheet" href="{{ asset("css/card.css") }}">
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset("css/select2-bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/lightbox.css") }}">
    <link rel="stylesheet" href="{{ asset("css/sb-admin-2.css") }}">
    <link rel="stylesheet" href="{{ asset("css/font-awesome/css/font-awesome.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/jquery-confirm.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/jquery.ui.css") }}">

    <script src="{{ asset("js/jquery-3.3.1.js") }}"></script>
    <script src="{{ asset("js/jquery.ui.js") }}"></script>
    <script src="{{ asset("js/bootstrap.min.js") }}"></script>
    <script src="{{ asset("js/metisMenu.min.js") }}"></script>
    <script src="{{ asset("js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("js/dataTables.bootstrap.min.js") }}"></script>
    <script src="{{ asset("js/dataTables.responsive.js") }}"></script>
    <script src="{{ asset("js/select2.min.js") }}"></script>
    <script src="{{ asset("js/toastr.min.js") }}"></script>
    <script src="{{ asset("js/waitMe.js") }}"></script>
    <script src="{{ asset("js/jquery-confirm.min.js") }}"></script>
    <script src="{{ asset("js/lightbox.js") }}"></script>
    <script src="{{ asset("js/socket.io.js") }}"></script>
    <script src="{{ asset("js/sb-admin-2.js") }}"></script>
    
</head>
<body id="body">
    
    <div id="wrapper">

        @include('includes.navbars')

        @section('content')
        @show

    </div> <!-- /#wrapper -->

    @section('scripts')
    @show

</body>
</html>