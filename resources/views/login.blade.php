<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    <title>Victoria Court Admin</title>

    {{-- CSS --}}
    <link rel="stylesheet" href="{{ asset("css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/metisMenu.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/sb-admin-2.css") }}">
    <link rel="stylesheet" href="{{ asset("css/font-awesome/css/font-awesome.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/toastr.min.css") }}">

    {{-- JS --}}
    <script src="{{ asset("js/jquery-3.3.1.js") }}"></script>
    <script src="{{ asset("js/toastr.min.js") }}"></script>

</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="text-align: center;"><img src="{{ asset("images/VCLogoLogin.png") }}" alt="" height="200px"></h3>
                    </div>
                    <div class="panel-body">
                            <form id="frmlogin" name="frmlogin" action="{{ url("api/login/loginuser") }}" method="POST" style="margin-top: 10px;">
                            <fieldset>
                                <div class="form-group">
                                    <label for="txtusername">Username</label>
                                    <input class="form-control" placeholder="Username" id="txtusername" name="txtusername" type="text" autocomplete="off" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="txtpassword">Password</label>
                                    <input class="form-control" placeholder="Password" id="txtpassword" name="txtpassword" type="password" value="">
                                </div>
                                <button id="btnlogin" name="btnlogin" class="btn btn-success btn-block">Sign in</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
        
        $(document).ready(function() {
    
            var msg = "{{ Session::get('message') }}";
            if(msg!=""){
                toastr.error(msg);
            }

        });

    </script>

</body>
</html>