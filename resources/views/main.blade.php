@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Overview</h1>
            </div>

            <div class="col-lg-12">

                <table id="tblsystemactivities" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Activity</th>
                            <th>Modified By</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                </table>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>


    //Variables
    var tblsystemactivities;

    $(document).ready(function() {
    
        var msg = "{{ Session::get('message') }}";
        if(msg!=""){
            toastr.success(msg);
        }

        LoadSystemActivities();

    });

    function LoadSystemActivities(){

        tblsystemactivities = $('#tblsystemactivities').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/systemactivities/loadsystemactivities") }}',
            },
            columns : [
                {data: 'activity', name: 'activity'},
                {data: 'modifiedby', name: 'modifiedby'},
                {data: 'date', name: 'date'},
                {data: 'time', name: 'time'},
            ]
        });

    }

</script>
@endsection