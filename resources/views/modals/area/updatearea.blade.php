<!-- Modal -->
<div class="modal fade" id="updatearea" role="dialog" aria-labelledby="updatearea" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Area</h4>
         </button>
        </div>
        <div class="modal-body container-fluid">

            <div class="col-md-12">

                <div class="form-group">
                    <label for="txtuarea">Area</label>
                    <input type="text" name="txtuarea" id="txtuarea" class="form-control" placeholder="Area">
                </div>

            </div>
  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btnupdate" name="btnupdate">Update Information</button>
        </div>
      </div>
    </div>
  </div>