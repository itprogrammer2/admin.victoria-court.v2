<!-- Modal -->
<div class="modal fade" id="newchecklist" role="dialog" aria-labelledby="newchecklist" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Checklist Information</h4>
         </button>
        </div>
        <div class="modal-body container-fluid">

            <div class="col-md-12">

                <div class="form-group">
                    <label for="cmbnareas">Area</label>
                    <select name="cmbnareas" id="cmbnareas" class="form-control"></select>
                </div>

                <div class="form-group">
                    <label for="cmbncomponents">Component</label>
                    <select name="cmbncomponents" id="cmbncomponents" class="form-control"></select>
                </div>

                <div class="form-group">
                    <label for="cmbnstandards">Standard</label>
                    <select name="cmbnstandards" id="cmbnstandards" class="form-control"></select>
                </div>

                <div class="form-group">
                    <label for="cmbnremarks">Remark</label>
                    <select name="cmbnremarks" id="cmbnremarks" class="form-control"></select>
                </div>

                <div class="form-group">
                    <label for="cmbnfindingtype">Finding Type</label>
                    <select name="cmbnfindingtype" id="cmbnfindingtype" class="form-control"></select>
                </div>

            </div>
  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btnsavechecklist" name="btnsavechecklist">Save Information</button>
        </div>
      </div>
    </div>
  </div>