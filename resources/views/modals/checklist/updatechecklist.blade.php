<!-- Modal -->
<div class="modal fade" id="updatechecklist" role="dialog" aria-labelledby="updatechecklist" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Checklist Information</h4>
         </button>
        </div>
        <div class="modal-body container-fluid">

            <div class="col-md-12">

                <div class="form-group">
                    <label for="cmbuareas">Area</label>
                    <select name="cmbuareas" id="cmbuareas" class="form-control"></select>
                </div>

                <div class="form-group">
                    <label for="cmbucomponents">Component</label>
                    <select name="cmbucomponents" id="cmbucomponents" class="form-control"></select>
                </div>

                <div class="form-group">
                    <label for="cmbustandards">Standard</label>
                    <select name="cmbustandards" id="cmbustandards" class="form-control"></select>
                </div>

                <div class="form-group">
                    <label for="cmburemarks">Remark</label>
                    <select name="cmburemarks" id="cmburemarks" class="form-control"></select>
                </div>

                <div class="form-group">
                    <label for="cmbufindingtype">Finding Type</label>
                    <select name="cmbufindingtype" id="cmbufindingtype" class="form-control"></select>
                </div>

            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btnupdatechecklist" name="btnupdatechecklist">Update Information</button>
        </div>
      </div>
    </div>
  </div>
