<div class="modal fade" id="editevent" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
           <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Event Information</h4>
                 </button>
            </div>
               <div class="modal-body">

                <div class="container-fluid">

                    <div class="form-group">
                        <label for="txtureason">Reason</label>
                        <input id="txtureason" name="txtureason" class="form-control" type="text" placeholder="Reason">
                    </div>

                    <div class="form-group">
                        <label for="txtucolor">Color</label>
                        <input type="color" id="txtucolor" name="txtucolor" class="form-control">
                    </div>

                </div>

              </div>
             <div class="modal-footer">
                <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary">Update Information</button>
            </div>
        </div>
     </div>
</div>



