<div class="modal fade" id="newevent" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
           <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Event Information</h4>
                 </button>
            </div>
               <div class="modal-body">

                <div class="container-fluid">

                    <div class="form-group">
                        <label for="txtnreason">Reason</label>
                        <input id="txtnreason" name="txtnreason" class="form-control" type="text" placeholder="Reason">
                    </div>

                    <div class="form-group">
                        <label for="txtncolor">Color</label>
                        <input type="color" id="txtncolor" name="txtncolor" class="form-control">
                    </div>

                </div>

              </div>
             <div class="modal-footer">
                <button id="btnnsave" name="btnnsave" type="button" class="btn btn-primary">Save Information</button>
            </div>
        </div>
     </div>
</div>



