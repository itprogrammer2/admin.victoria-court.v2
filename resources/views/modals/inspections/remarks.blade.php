<!-- Modal -->
<div class="modal fade" id="remarks" role="dialog" aria-labelledby="roomInspection" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Remarks For Inspection</h4>
       </button>
      </div>
      <div class="modal-body container-fluid">
        <div class="col-md-12">
          <div class="col-md-5">

              <br>
              <div class="form-group">
                  <label for="cmbstandard">Standard</label>
                  <select name="cmbstandard" id="cmbstandard" class="form-control"></select>
              </div>

              <div class="form-group">
                  <label for="inspectionremarks">Remarks</label>
                  <input type="text" class="form-control" id="inspectionremarks" name="inspectionremarks"/>
              </div>

          </div>
          <div class="col-md-7">

            <div class="col-md-12 row">
              <table class="table .table-condensed">
                <thead>
                  <tr>
                    <th>Finding Type</th>
                    <th>Points</th>
                    <th><button id="btnnadd" name="btnnadd" class="btn btn-flat btn-primary"><i class="fa fa-plus"></i></button></th>
                  </tr>
                  <tbody id="findingtypenewcontent">
                    <tr>
                      <td><select class="form-control" id="cmbfindingtype1" name="cmbfindingtype[]"></select></td>
                      <td><input type="text" id="txtpoints1" name="txtpoints[]" class="form-control"></td>
                      <td></td>
                    </tr>
                  </tbody>
                </thead>
              </table>
            </div>

          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="saveRemarks" data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>
