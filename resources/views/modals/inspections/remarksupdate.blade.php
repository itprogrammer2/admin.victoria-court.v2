<!-- Modal -->
<div class="modal fade" id="remarksupdate" role="dialog" aria-labelledby="roomInspection" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Remarks For Inspection</h4>
       </button>
      </div>
      <div class="modal-body container-fluid">
        <div class="col-md-12">
          <div class="col-md-5">
            
              <br>
              <div class="form-group">
                  <label for="cmbustandard">Standard</label>
                  <select name="cmbustandard" id="cmbustandard" class="form-control"></select>
              </div>
              
              <div class="form-group">
                  <label for="inspectionuremarks">Remarks</label>
                  <input type="text" class="form-control" id="inspectionuremarks" name="inspectionuremarks"/>
              </div>

          </div>
          <div class="col-md-7">

            <div class="col-md-12 row">
                <table class="table .table-condensed">
                  <thead>
                    <tr>
                      <th>Finding Type</th>
                      <th>Points</th>
                      <th><button id="btnuadd" name="btnuadd" class="btn btn-flat btn-primary"><i class="fa fa-plus"></i></button></th>
                    </tr>
                    <tbody id="findingtypeupdatecontent">
                      {{-- <tr> FORMAT
                        <td><select class="form-control" id="cmbfindingtype1" name="cmbfindingtype[]"></select></td>
                        <td><input type="text" id="txtpoints1" name="txtpoints[]" class="form-control"></td>
                        <td></td>
                      </tr> --}}
                    </tbody>
                  </thead>
                </table>
              </div>
            
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnupdate" name="btnupdate">Update</button>
      </div>
    </div>
  </div>
</div>