<!-- Modal -->
<div class="modal fade" id="standard" tabindex="-1" role="dialog" aria-labelledby="roomInspection" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Components For Inspection</h4>
       </button>
      </div>
      <div class="modal-body container-fluid">
        <div class="col-md-12">
            <label for="standardName">Component</label><br/>
            <input type="text" class="form-control" id="standardName"/><br/>

            <label for="standardDescription">Description</label><br/>
            <input type="text" class="form-control" id="standardDescription"/><br/>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="saveStandard" data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>