<div class="modal fade" id="newnationality" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
               <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">New Nationality</h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div id="nationalityncontent" class="container-fluid">
    
                        <div class="form-group">

                            <label for="txtnnationality">Nationality</label>
                            <input id="txtnnationality" name="txtnnationality" class="form-control" type="text">
                            
                        </div>
    
                    </div>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="btnnsave" name="btnnsave" type="button" class="btn btn-success">Save Information</button>
                    <button id="btnnclose" name="btnnclose" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
         </div>
    </div>