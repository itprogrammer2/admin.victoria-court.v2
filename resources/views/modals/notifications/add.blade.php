<div class="modal fade" id="addnotificationblocklist" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
               <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Notification Blocklist Add</h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div id="nationalityncontent" class="container-fluid">
    
                        <div class="form-group">

                            <label for="txtnnationality">Add Role</label>
                            <select name="roles" id="roles" class="form-control">
                                
                            </select>
                            
                        </div>
    
                    </div>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="btnnsave" name="btnnsave" type="button" class="btn btn-success">Add</button>
                    <button id="btnnclose" name="btnnclose" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
         </div>
    </div>

<div class="modal fade" id="deleteBlockList" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
               <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Delete Blocklist Record</h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <span>Delete This Record?</span>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="deleteRecord" name="btnnsave" type="button" class="btn btn-success">Delete</button>
                    <button id="btnnclose" name="btnnclose" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
         </div>
    </div>