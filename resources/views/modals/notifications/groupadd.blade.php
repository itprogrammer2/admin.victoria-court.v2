<div class="modal fade" id="addnotificationgroupadd" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
               <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Notification Group Add</h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div id="nationalityncontent" class="container-fluid">
    
                        <div class="form-group">

                            <label for="txtnnationality">Add Group</label>
                            <input type="text" name="groupname" id="groupname" class="form-control" />
                            
                        </div>
    
                    </div>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="btnnsave" name="btnnsave" type="button" class="btn btn-success">Add</button>
                    <button id="btnnclose" name="btnnclose" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
         </div>
    </div>

<div class="modal fade" id="deletenotificatongroup" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
               <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Notification Group Delete</h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <span>Are you sure to delete this record?</span>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="deleterecrod" name="btnnsave" type="button" class="btn btn-success">Delete</button>
                    <button id="btnnclose" name="btnnclose" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
         </div>
    </div>