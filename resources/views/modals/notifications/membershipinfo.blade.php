<div class="modal fade" id="membershipinfo" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
               <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="groupinfo" class="modal-title"></h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div id="groupcontent" class="container-fluid">

                        <div class="col-md-12 row">
                            <button id="btnaddrole" name="btnaddrole" class="btn btn-flat btn-success" style="margin-bottom: 10px; float: right;"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <div class="col-md-12 row">
                            
                            <div id="grouptable">
                                <table class="table table-bordered">
                                    <thead>
                                      <tr>
                                        <th>Id</th>
                                        <th>Role</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody id="groupdata">
                                        {{-- Data Here --}}
                                    </tbody>
                                </table>
                            </div>
                            <div id="addrole">
                                
                                <div class="col-md-3">

                                </div>
                                <div class="col-md-6">
                                    {{-- <h3>Add Role</h3> --}}
                                    <div class="form-group">
                                        <label for="cmbnroles">Role</label>
                                        <select name="cmbnroles" id="cmbnroles" class="form-control"></select>
                                    </div>
                                    <button id="btnsaverole" name="btnsaverole" class="btn btn-primary btn-flat" style="float: right;">Save</button>
                                    <br>    
                                </div>
                                <div class="col-md-3">
                                    
                                </div>
                                
                            </div>

    
                        </div>

                    </div>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="btnnclose" name="btnnclose" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
         </div>
    </div>
