<div class="modal fade" id="newnotificationsched" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
           <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="groupinfo" class="modal-title">New Notification Schedule</h4>
                 </button>
            </div>
               <div class="modal-body container-fluid">

                <div class="col-md-12">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="cmbnoperation">Operation</label>
                            <select name="cmbnoperation" id="cmbnoperation" class="form-control input-sm">
                                <option value="">Select a operation</option>
                                <option value="Occupied">Occupied</option>
                                <option value="Shift">Shift Sales</option>
                            </select>
                        </div>
                        <hr>
                    </div>

                    <div id="schedcontent">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtnschedulename">Schedule Name</label>
                                <input type="text" id="txtnschedulename" name="txtnschedulename" class="form-control input-sm" placeholder="Schedule Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtndescription">Description</label>
                                <input type="text" id="txtndescription" name="txtndescription" class="form-control input-sm" placeholder="Description">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtntime">Time</label>
                                <input type="time" id="txtntime" name="txtntime" class="form-control input-sm">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <h3>Schedule Day</h3>
                            <div class="col-md-6 row">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chknmon" name="chknmon">
                                    <label class="form-check-label" for="chknmon">
                                    Monday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chknwed" name="chknwed">
                                    <label class="form-check-label" for="chknwed">
                                    Wednesday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chknfri" name="chknfri">
                                    <label class="form-check-label" for="chknfri">
                                    Friday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chknsat" name="chknsat">
                                    <label class="form-check-label" for="chknsat">
                                    Saturday
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chkntue" name="chkntue">
                                    <label class="form-check-label" for="chkntue">
                                    Tuesday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chknthu" name="chknthu">
                                    <label class="form-check-label" for="chknthu">
                                    Thursday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chknsun" name="chknsun">
                                    <label class="form-check-label" for="chknsun">
                                    Sunday
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <table id="tblngroup" class="table table-bordered" style="margin-top: 5px;">
                                <thead>
                                <tr>
                                    <th style="vertical-align: middle;" style="width: 100%;">Group</th>
                                    <th><button id="btnnadd" name="btnnadd" class="btn btn-flat btn-success" style="float: right;"><i class="fa fa-plus"></i>
                                    </button></th>
                                </tr>
                                </thead>
                                <tbody id="tblngroupcontent">
                                    <tr>
                                        <td><select name="cmbngroup[]" id="cmbngroup1" class="form-control input-sm" style="width: 350px;"></select></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>

                            <button id="btnnsave" name="btnnsave" class="btn btn-success btn-flat" style="float: right;">Save Information</button>
                        </div>

                    </div>


                </div>


              </div>
             <div class="modal-footer">
                <button id="btnnclose" name="btnnclose" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
</div>
