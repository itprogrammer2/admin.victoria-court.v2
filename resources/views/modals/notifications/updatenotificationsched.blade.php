<div class="modal fade" id="updatenotificationsched" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
           <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="groupinfo" class="modal-title">Update Notification Information</h4>
                 </button>
            </div>
               <div class="modal-body container-fluid">

                <div class="col-md-12">

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="cmbuoperation">Operation</label>
                            <select name="cmbuoperation" id="cmbuoperation" class="form-control input-sm">
                                <option value="">Select a operation</option>
                                <option value="Occupied">Occupied</option>
                                <option value="Shift">Shift Sales</option>
                            </select>
                        </div>
                        <hr>
                    </div>

                    <div id="uschedcontent">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtuschedulename">Schedule Name</label>
                                <input type="text" id="txtuschedulename" name="txtuschedulename" class="form-control input-sm" placeholder="Schedule Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtudescription">Description</label>
                                <input type="text" id="txtudescription" name="txtudescription" class="form-control input-sm" placeholder="Description">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtutime">Time</label>
                                <input type="time" id="txtutime" name="txtutime" class="form-control input-sm">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <h3>Schedule Day</h3>
                            <div class="col-md-6 row">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chkumon" name="chkumon">
                                    <label class="form-check-label" for="chkumon">
                                    Monday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chkuwed" name="chkuwed">
                                    <label class="form-check-label" for="chkuwed">
                                    Wednesday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chkufri" name="chkufri">
                                    <label class="form-check-label" for="chkufri">
                                    Friday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chkusat" name="chkusat">
                                    <label class="form-check-label" for="chkusat">
                                    Saturday
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chkutue" name="chkutue">
                                    <label class="form-check-label" for="chkutue">
                                    Tuesday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chkuthu" name="chkuthu">
                                    <label class="form-check-label" for="chkuthu">
                                    Thursday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="chkusun" name="chkusun">
                                    <label class="form-check-label" for="chkusun">
                                    Sunday
                                    </label>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <table id="tblngroup" class="table table-bordered" style="margin-top: 5px;">
                                <thead>
                                <tr>
                                    <th style="vertical-align: middle;" style="width: 100%;">Group</th>
                                    <th><button id="btnuadd" name="btnuadd" class="btn btn-flat btn-success" style="float: right;"><i class="fa fa-plus"></i>
                                    </button></th>
                                </tr>
                                </thead>
                                <tbody id="tblugroupcontent">
                                    {{-- Data Here --}}
                                </tbody>
                            </table>

                            <button id="btnusave" name="btnusave" class="btn btn-success btn-flat" style="float: right;">Update Information</button>
                        </div>

                    </div>


                </div>


              </div>
             <div class="modal-footer">
                <button id="btnuclose" name="btnuclose" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
</div>
