<div class="modal fade" id="newreservation" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
           <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Reservation Information</h4>
                 </button>
            </div>
               <div class="modal-body">

                <div class="container-fluid">

                    <div class="form-group">
                        <label for="txtnname">Name</label>
                        <input id="txtnname" name="txtnname" class="form-control" type="text" placeholder="Name">
                    </div>

                    <div class="form-group">
                        <label for="txtndescription">Description</label>
                        <input id="txtndescription" name="txtndescription" class="form-control" type="text" placeholder="Description">
                    </div>

                </div>
                
              </div>
             <div class="modal-footer">
                <button id="btnnsave" name="btnnsave" type="button" class="btn btn-primary">Save Information</button>
            </div>
        </div>
     </div>
</div>