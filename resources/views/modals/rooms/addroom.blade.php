<!-- Modal -->
<div class="modal fade" id="addroom" tabindex="-1" role="dialog" aria-labelledby="roomInspection" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Room Information</h4>
       </button>
      </div>
      <div class="modal-body container-fluid">
        <div class="col-md-12 row">
          <div class="col-md-6">

            <div class="form-group">
              <label for="roomNo" class="form-label">Room No.</label>
              <input type="text" class="form-control" id="roomNo"/>
            </div>

            <div class="form-group">
              <label for="roomArea" class="form-label">Room Area</label>
              <select name="roomArea" id="roomArea" class="form-control"></select>
            </div>

          </div>
          <div class="col-md-6">

            <div class="form-group">
              <label for="roomName" class="form-label">Room Name</label>
              <input type="text" class="form-control" id="roomName"/>
            </div>

            <div class="form-group">
              <label for="roomType" class="form-label">Room Type</label>
              <select name="roomType" id="roomType" class="form-control"></select>
            </div>

          </div>
          
          <div class="col-md-12">
              <div class="form-group">
                  <label for="roomDesc" class="form-label">Room Description</label>
                  <textarea class="form-control" id="roomDesc"></textarea>
              </div>
          </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="saveRoom" data-dismiss="modal">Save</button>
      </div>
    </div>
  </div>
</div>