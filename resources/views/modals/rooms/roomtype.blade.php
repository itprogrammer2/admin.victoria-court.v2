<!-- Modal -->
<div class="modal fade" id="roomtype" tabindex="-1" role="dialog" aria-labelledby="roomtype" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Room Type Information</h4>
       </button>
      </div>
      <div class="modal-body container-fluid">

        <div class="col-md-12">
         
            <div class="col-md-12">
                    <button id="btnbackupdate" name="btnbackupdate" class="btn btn-flat btn-primary" style="float: right; margin-bottom: 10px;"><i class="fa fa-backward"></i></button>
                    <button id="btnaddroomtype" name="btnaddroomtype" class="btn btn-flat btn-primary" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i></button>
            </div>
            
            <div id="newroomtype">

                <div class="col-md-12">

                    <div class="form-group">

                        <label for="txtnroomtype">Room Type</label>
                        <input class="form-control" type="text" name="txtnroomtype" id="txtnroomtype">

                    </div>

                    <button id="btnnsaveroomtype" name="btnnsaveroomtype" class="btn btn-flat btn-success" style="float: right;">Save Information</button>

                </div>

            </div>

            <div id="updateroomtype">

                    <div class="col-md-12">
         
                        <div class="form-group">
                
                            <label for="txturoomtype">Room Type</label>
                            <input class="form-control" type="text" name="txturoomtype" id="txturoomtype">
                
                        </div>

                        <button id="btnuupdateroomtype" name="btnuupdateroomtype" class="btn btn-flat btn-success" style="float: right;">Update Information</button> 
                   
                    </div>

            </div>

            <div id="roomtypetable">

                <div class="col-md-12">
                    <table id="tblroomtype" class="table table-striped table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Room Type</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                
            </div>
            
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>