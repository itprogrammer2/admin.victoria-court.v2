<style>
/* For AutoComplete     */
ul.ui-autocomplete { 
    z-index: 1100;
}
</style>
<!-- Modal -->
<div class="modal fade" id="updategrouproomtype" role="dialog" aria-labelledby="roomtype" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">New Group Room Type</h4>
         </button>
        </div>
        <div id="modalupdatecontent" class="modal-body container-fluid">
  
          <div class="col-md-12">
           
            <div class="col-md-6 row">

               <div class="form-group ui-widget">
                    <label for="txtugroupname">Group Name</label>
                    <input type="text" id="txtugroupname" name="txtugroupname" class="form-control" readonly>
               </div>

            </div>
            <div class="col-md-12 row">

                <table id="tblroomgrouptype" class="table table-striped table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th style="vertical-align: middle;">Room Type</th>
                            <th style="width: 10%;"><button id="btuaddgrouproomtype" name="btuaddgrouproomtype" class="btn btn-flat btn-primary"><i class="fa fa-plus"></i></button></th>
                        </tr>
                    </thead>
                    <tbody id="updategrouproomtypecontent">
                    </tbody>
                </table>

            </div>
                   
          </div>
  
        </div>
        <div class="modal-footer">
          <button id="btnupdatenewgrouproomtype" name="btnupdatenewgrouproomtype" class="btn btn-flat btn-success">Update Information</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>