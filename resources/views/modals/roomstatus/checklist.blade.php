<div class="modal fade" id="checklist" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
           <div class="modal-content">
            <div class="modal-header">
                    {{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
                    <h4 class="modal-title">Checklist Standard</h4>
                 </button>
            </div>
               <div class="modal-body">

                <div id="divstandardcontent" class="container-fluid">

                    <table id="tblcheckliststandard" class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width: 350px;">Standard</th>
                                <th style="width: 100px;"></th>
                            </tr>
                        </thead>
                        <tbody id="tblcheckliststandardcontent">
                            {{-- Content Here --}}
                        </tbody>
                    </table>

                </div>

              </div>
             <div class="modal-footer">
                 {{-- Nothing --}}
                 <button id="btnclosechecklist" name="btnclosechecklist" class="btn btn-flat btn-danger">Close</button>
            </div>
        </div>
     </div>
</div>
