<div class="modal fade" id="uroomstatus" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
           <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Room Status Information</h4>
                 </button>
            </div>
               <div class="modal-body">

                <div class="form-group">

                    <label for="txturoomstatus">Status</label>
                    <input id="txturoomstatus" name="txturoomstatus" type="text" class="form-control" />
                    
                </div>

                <div class="form-group">
                    
                    <input id="txtucolor" name="txtucolor" type="color" class="form-control" style="width: 100px; height: 50px; float: right;"/>
                        
                </div>
                
              </div>
             <div class="modal-footer">
                <button id="btnupdate" name="btnupdate" type="button" class="btn btn-primary">Update</button>
            </div>
        </div>
     </div>
</div>