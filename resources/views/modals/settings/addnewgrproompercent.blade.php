<div class="modal fade" id="addnewgrproompercent" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
           <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 id="modalheader" class="modal-title">New Room Balancer Group</h4>
                 </button>
            </div>
               <div class="modal-body">

                <div class="container-fluid">

                    <div class="form-group">
                        <label for="txtordergrp">Order</label>
                        <input id="txtordergrp" name="txtordergrp" class="form-control" type="text" placeholder="1">
                    </div>

                    <div class="form-group">
                        <label for="cmbngrpname">Group Name</label>
                        <select name="cmbngrpname" id="cmbngrpname" class="form-control">
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="txtgrppercentage">Percentage</label>
                        <input id="txtgrppercentage" name="txtgrppercentage" class="form-control allow_decimal" type="text" placeholder="0.8">
                    </div>

                </div>

              </div>
             <div class="modal-footer">
                <button id="btnsavegrproompercent" name="btnsavegrproompercent" type="button" class="btn btn-primary">Save Information</button>
                <button id="btnnclose" name="btnnclose" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
     </div>
</div>
