<div class="modal fade" id="newrating" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
               <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="modalheader" class="modal-title">New Rating</h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div class="container-fluid">
    
                        <div class="form-group">
                            <label for="txtnscorefrom">Score From</label>
                            <input id="txtnscorefrom" name="txtnscorefrom" class="form-control allow_decimal" type="text" placeholder="From">
                        </div>

                        <div class="form-group">
                            <label for="txtnscoreto">Score To</label>
                            <input id="txtnscoreto" name="txtnscoreto" class="form-control allow_decimal" type="text" placeholder="To">
                        </div>

                        <div class="form-group">
                            <label for="txtnrating">Rating</label>
                            <input id="txtnrating" name="txtnrating" class="form-control" type="text" placeholder="Rating">
                        </div>
    
                    </div>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="btnnsave" name="btnnsave" type="button" class="btn btn-primary">Save Information</button>
                    <button id="btnnclose" name="btnnclose" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
         </div>
    </div>