<div class="modal fade" id="points" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
               <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="modalheader" class="modal-title"></h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div class="container-fluid">
    
                        <div class="form-group">
                            <label for="txtnpoints">Points</label>
                            <input id="txtnpoints" name="txtnpoints" class="form-control" type="text" placeholder="Points">
                        </div>
    
                    </div>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="btnnsave" name="btnnsave" type="button" class="btn btn-primary">Save Information</button>
                </div>
            </div>
         </div>
    </div>