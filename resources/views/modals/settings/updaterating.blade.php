<div class="modal fade" id="updaterating" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
               <div class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 id="modalheader" class="modal-title">Update Rating</h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div class="container-fluid">
    
                        <div class="form-group">
                            <label for="txtuscorefrom">Score From</label>
                            <input id="txtuscorefrom" name="txtuscorefrom" class="form-control allow_decimal" type="text" placeholder="From">
                        </div>

                        <div class="form-group">
                            <label for="txtuscoreto">Score To</label>
                            <input id="txtuscoreto" name="txtuscoreto" class="form-control allow_decimal" type="text" placeholder="To">
                        </div>

                        <div class="form-group">
                            <label for="txturating">Rating</label>
                            <input id="txturating" name="txturating" class="form-control" type="text" placeholder="Rating">
                        </div>
    
    
                    </div>
                    
                  </div>
                 <div class="modal-footer">
                    <button id="btnuupdate" name="btnuupdate" type="button" class="btn btn-primary">Update Information</button>
                    <button id="btnuclose" name="btnuclose" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
         </div>
    </div>