<div class="modal fade" id="addresources" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
           <div id="useraccessmodaldocument" class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">New Resources Information</h4>
                 </button>
            </div>
               <div class="modal-body">

                <div class="container-fluid">

                    <div class="form-group col-md-12">
                        <label for="txtaresources">Resources</label>
                        <input id="txtaresources" name="txtaresources" class="form-control" type="text" placeholder="Resources">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="txtadescription">Description</label>
                        <input id="txtadescription" name="txtadescription" class="form-control" type="text" placeholder="Description">
                    </div>

                </div>
                
              </div>
             <div class="modal-footer">
                 <button id="btnaaddresources" name="btnaaddresources" class="btn btn-flat btn-info">Save Information</button>
            </div>
        </div>
     </div>
</div>