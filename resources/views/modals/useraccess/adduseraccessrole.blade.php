<div class="modal fade" id="adduseraccessrole" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
               <div id="useraccessmodaldocument" class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">New Role Information</h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div class="container-fluid">
    
                        <div class="form-group col-md-12">
                            <label for="txtauserrole">Role</label>
                            <input id="txtauserrole" name="txtauserrole" class="form-control" type="text" placeholder="Role">
                        </div>

                    </div>
                    
                  </div>
                 <div class="modal-footer">
                     <button id="btnaadduserrole" name="btnaadduserrole" class="btn btn-flat btn-info">Save Information</button>
                </div>
            </div>
         </div>
    </div>