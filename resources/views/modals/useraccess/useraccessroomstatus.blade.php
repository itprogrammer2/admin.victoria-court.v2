<div class="modal fade" id="useraccessroomstatusinfo" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
               <div id="useraccessmodaldocument" class="modal-content">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Allow Status Information</h4>
                     </button>
                </div>
                   <div class="modal-body">
    
                    <div class="container-fluid">
    
                        <div class="form-group col-md-6">
                            <label for="txturoomstatus">Role</label>
                            <input id="txturoomstatus" name="txturoomstatus" class="form-control" type="text" placeholder="Role" readonly>
                        </div>
    
                        <div class="col-md-12">
                            <table id="tblallowstatus" class="table table-striped table-bordered" style="width: 100%">
                                <thead> 
                                    <tr>
                                        <th>Allowed Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="allowstatusitems">

                                </tbody>
                            </table>
                        </div>

                    </div>
                    
                  </div>
                 <div class="modal-footer">
                </div>
            </div>
         </div>
    </div>