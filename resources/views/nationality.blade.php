@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Nationality</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12 row">
                    <button id="btnnewnationality" name="btnnewnationality" data-toggle="modal" data-target="#newnationality" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i> New Nationality</button>
                </div>
                
                <div class="col-lg-12 row">
                    <table id="tblnationality" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nationality</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        {{-- Modal --}}
        @include('modals.nationality.newnationality')
        @include('modals.nationality.updatenationality')

    </div>
     <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    //Variables
    var tblnationality;
    var sid;

    $(document).ready(function(){

        LoadNationality();

    });

    $('#btnnewnationality').on('click', function(){

        ClearNewNationality();

    });

    $('#btnnsave').on('click', function(){

        var nationality = $('#txtnnationality').val();

        if(nationality==""){
            toastr.error('Please Input The Nationality.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/nationality/savenationality") }}',
                type: 'post',
                data: {
                    nationality: nationality
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){
                        ReloadNationality();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#newnationality .close').click();
                    }
                    else{
                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                        $('#txtnnationality').val('');
                        $('#txtnnationality').focus();
                    }

                }
            });

        }

    });

    $(document).on('click', '#btnedit', function(){

        sid = $(this).val();

        $.ajax({
            url: '{{ url("api/nationality/getnationalityinfo") }}',
            type: 'get',
            data: {
                id: sid
            },
            dataType: 'json',
            success: function(response){

                $('#txtunationality').val(response.nationality);

            }
        });

    });

    $('#btnuupdate').on('click', function(){

        UpdateNationality();

    });

    function UpdateNationality(){

        var nationality = $('#txtunationality').val();

        if(nationality==""){
            toastr.error('Please Input The Nationality.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/nationality/updatenationalityinfo") }}',
                type: 'post',
                data: {
                    id: sid,
                    nationality: nationality
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){
                        ReloadNationality();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#updatenationality .close').click();
                    }

                }
            });

        }

    }

    function ClearNewNationality(){

        $('#txtnnationality').val('');

    }

    function LoadNationality(){

        tblnationality = $('#tblnationality').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/nationality/loadnationality") }}',
            },
            columns : [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

    function ReloadNationality(){

        tblnationality.ajax.reload();

    }


</script>
@endsection