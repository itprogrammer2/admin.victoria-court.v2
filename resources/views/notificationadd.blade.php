@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Notification Blocklist Add</h1>
                <button id="btnaddinventory" name="btnaddinventory" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;" data-toggle="modal" data-target="#addnotificationblocklist"><i class="fa fa-plus"></i> Notification Blocklist Add</button>
            </div>

            <div class="col-lg-12">

                <table id="tblnotification" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Roles</th>
                            <th>Date Created</th>
                            <!-- <th>Remove</th> -->
                        </tr>
                    </thead>
                </table>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

{{-- Modal --}}
@include('modals.notifications.add')

@endsection

@section('scripts')
<script>

  //Variables
  var tblnotification;
  var deleteid;

  $(document).ready(function(){
    getRoles();
    getAdded();
  });

  $(document).on('click', '#btnnsave', function(){

    var roles = $('#roles').val();

    $.ajax({

      url: "{{ url('api/notifications/add') }}",
      method: "POST",
      datatype: "JSON",
      data:{
        role_id: roles
      },
      success:function(r){

        toastr.success(r.message);
        $('#addnotificationblocklist').modal('toggle');
        ReloadBlockListInformation();

      },
      error:function(r){
        console.log(r);
        toastr.error("This Position is already recorded");
      }
    });

  });

  $('#tblnotification').on('click', 'tbody td', function(){
    // console.log('API row values: ', tblnotification.row(this).data());
    $('#deleteBlockList').modal("show");
    var data = tblnotification.row(this).data();

    deleteid = data.id;

  });

  $(document).on('click', '#deleteRecord', function(){

      $.ajax({
        url: "{{ url('api/notification/deleteRole') }}",
        method: 'POST',
        datatype: 'JSON',
        data:{
          data: deleteid
        },
        success:function(r){

          ReloadBlockListInformation();
          $('#deleteBlockList .close').click();

        },
        error:function(r){
          console.log(r);
        }

      });

  });

  $(document).on('click', '#btnnclose', function(){

    $('#deleteBlockList .close').click();

  });

  function getRoles(){

    $.ajax({
      url: "{{ url('api/notifications/getroles') }}",
      method: "POST",
      datatype : "JSON",
      success:function(r){

        $.each(r.response, function(i, items){
          $('#roles').append($('<option>', {
            value: items.id,
            text: items.role
          }));
        });

        $('#roles').select2({
            theme: 'bootstrap'
        });

      },
      error:function(r){

      }
    });

  }

  function getAdded(){

    tblnotification = $('#tblnotification').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'get',
            url: '{{ url("api/notification/getadded") }}',
        },
        columns : [
            {data: 'role', name: 'role'},
            {data: 'created_at', name: 'created_at'},
        ]
    });

  }

  function ReloadBlockListInformation(){

    tblnotification.ajax.reload();

  }

</script>
@endsection