@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Notification Group Add</h1>
                <button id="btnaddinventory" name="btnaddinventory" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;" data-toggle="modal" data-target="#addnotificationgroupadd"><i class="fa fa-plus"></i> Notification Group Add</button>
            </div>

            <div class="col-lg-12">

                <table id="tblnotificationgroup" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Group</th>
                            <th>Date Created</th>
                        </tr>
                    </thead>
                </table>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
{{-- Modal --}}
@include('modals.notifications.groupadd')
@endsection

@section('scripts')
<script>

  //Variables
  var tblnotificationgroup;
  var deletedid;

  $(document).ready(function(){

    getGroup();

  });

  $('#tblnotificationgroup').on('click', 'tbody td', function(){

      $('#deletenotificatongroup').modal("show");
      var data = tblnotificationgroup.row(this).data();
      
      deletedid = data.id;

  });

  $(document).on('click', '#deleterecrod', function(){
        
      $.ajax({
        url: "{{ url('api/notification/deletegroup') }}",
        method: 'POST',
        datatype: 'JSON',
        data:{
          data: deletedid
        },
        success:function(r){

          ReloadGroupInformation();
          $('#deletenotificatongroup .close').click();

        }
      });

  });

  $(document).on('click', '#btnnsave', function(){
  
    var groupname = $('#groupname').val();
    
    $.ajax({
      url: '{{ url("api/notification/groupadd") }}',
      type: "post",
      datatype: "json",
      data:{
        name: groupname
      },
      success:function(r){

        if(r.success){

          toastr.success(r.message);
          $('#addnotificationgroupadd .close').click();
          ReloadGroupInformation();

        }
        else{

          toastr.error("Group information already exist.");

        }

        
      }
    });

  });

  function getGroup(){

    tblnotificationgroup = $('#tblnotificationgroup').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'get',
            url: '{{ url("api/notification/getGroup") }}',
        },
        columns : [
            {data: 'name', name: 'name'},
            {data: 'created_at', name: 'created_at'},
        ]
    });

  }

  function ReloadGroupInformation(){

    tblnotificationgroup.ajax.reload();

  }

</script>
@endsection