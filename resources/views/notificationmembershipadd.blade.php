@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Notification Membership Add</h1>
                {{-- <button id="btnaddinventory" name="btnaddinventory" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;" data-toggle="modal" data-target="#addnotificationmembership"><i class="fa fa-plus"></i> Notification Membership Add</button> --}}
            </div>

            <div class="col-lg-12">

                <table id="tblnotificationgroup" class="table table-striped table-bordered" style="width: 100%">
                    <thead>
                        <tr>
                            <th>Group</th>
                            <th>Date Created</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

{{-- Modal --}}
@include('modals.notifications.membershipinfo')

@endsection

@section('scripts')
<script>

  //Variables
  var tblnotificationgroup;
  var sgroupid;
  var addrole = false;

  $(document).ready(function(){

    //Load
    LoadGroupInformation();

    //Set
    SetSelect2();

  });

  $(document).on('click', '#btngroupinfo', function(){

    sgroupid = $(this).val();

    $('#membershipinfo').modal('toggle');

    //Set
    addrole = false;
    $('#grouptable').show();
    $('#addrole').hide();
    
    LoadGroupData();

  });

  $('#btnaddrole').on('click', function(){

    if(addrole){

        addrole = false;
        $('#grouptable').show();
        $('#addrole').hide();

    }
    else{

        //Load
        LoadRoles();

        //Set
        addrole = true;
        $('#grouptable').hide();
        $('#addrole').show();

    }

  });

  $('#btnsaverole').on('click', function(){

    var role = $('#cmbnroles').val();

    //Validation
    if(role==""){

        toastr.error('Please select a role.', '', { positionClass: 'toast-top-center' });

    }
    else{

        $.ajax({
            url: '{{ url("api/notification/group/addroles") }}',
            type: 'post',
            data: {
                groupid: sgroupid,
                roleid: role
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    $('#btnaddrole').click();

                    LoadGroupData();

                }
                else{

                    toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

    }

  });

  $(document).on('click', '#btnnremove', function(){

    var id = $(this).val();

    $.ajax({
        url: '{{ url("api/notification/group/removeroles") }}',
        type: 'post',
        data: {
            id: id
        },
        dataType: 'json',
        success: function(response){

            if(response.success){

                toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                LoadGroupData();

            }

        }
    });

  });

  function LoadRoles(){

    $.ajax({
        url: '{{ url("api/notification/group/loadroles") }}',
        type: 'get',
        dataType: 'json',
        success: function(response){

            $('#cmbnroles').find('option').remove();
            $('#cmbnroles').append('<option value="">Select a role</option>');
            for(var i=0;i<response.data.length;i++){

                $('#cmbnroles').append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["role"] +'</option>');

            }

        }
    });


  }

  function LoadGroupData(){

    $.ajax({
      url: '{{ url("api/notification/group/loadgroupdata") }}',
      type: 'get',
      data: {
        groupid: sgroupid
      },
      dataType: 'json',
      success: function(response){

          $('#groupdata').find('tr').remove();

          if(response.data.length==0){

            $('#groupdata').append('<tr><td colspan="3" style="text-align: center;"><strong>No Data</strong></td></tr>');

          }
          else{

            for(var i=0;i<response.data.length;i++){

              $('#groupdata').append('<tr><td>'+ response.data[i]["id"] +'</td><td>'+ response.data[i]["role"] +'</td><td><button id="btnnremove" name="btnnremove" class="btn btn-flat btn-danger" value="'+ response.data[i]["id"] +'"><i class="fa fa-trash"></i></button></td></tr>');

            }

          }

      }
    });

  }

  function SetSelect2(){

    $('#cmbnroles').select2({
        theme: 'bootstrap'
    });

  }

  function LoadGroupInformation(){

    tblnotificationgroup = $('#tblnotificationgroup').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            type: 'get',
            url: '{{ url("api/notification/group/loadgroupinformation") }}',
        },
        columns : [
            {data: 'group', name: 'group'},
            {data: 'created_at', name: 'created_at'},
            {data: 'panel', name: 'panel'},
        ]
    });

  }

  function ReloadGroupInformation(){

    tblnotificationgroup.ajax.reload();

  }



</script>
@endsection