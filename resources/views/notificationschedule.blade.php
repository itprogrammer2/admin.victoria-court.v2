@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Notification Schedule</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-md-12 row">
                    <button id="btnnewschedule" name="btnnewschedule" class="btn btn-success btn-flat" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i> New Schedule</button>
                </div>
                <div class="col-md-12 row">


                    <table id="tblnotificationschedule" class="table table-striped table-bordered" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Operation</th>
                                <th>Time</th>
                                <th>Day</th>
                                <th>Date Created</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

{{-- Modal --}}
@include('modals.notifications.newnotificationsched')
@include('modals.notifications.updatenotificationsched')

@endsection

@section('scripts')
<script>

    //Variables
    var tblnotificationschedule;
    var localcode = '{{ $local_code }}';

    //New Variables
    var nitemarray = new Array();
    var nitemcount = 1;
    var mon = 0;
    var tue = 0;
    var wed = 0;
    var thu = 0;
    var fri = 0;
    var sun = 0;
    var sat = 0;

    //Update Variables
    var sid;
    var uitemarray = new Array();
    var uitemcount = 1;
    var ditemarray = new Array();

    $(document).ready(function(){

        //Set
        SetSelect2();

        //Load
        LoadSchedules();

    });

    $('#btnnewschedule').on('click', function(){

        $('#newnotificationsched').modal('toggle');
        $('#schedcontent').hide();

    });

    $('#cmbnoperation').on('change', function(){

        //Clear
        ClearNewSchedule();
        ClearNewGroup();

        //Set
        nitemarray = [];
        nitemcount = 1;
        mon = 0;
        tue = 0;
        wed = 0;
        thu = 0;
        fri = 0;
        sun = 0;
        sat = 0;

        $('#schedcontent').show();
        LoadGroups('cmbngroup'+nitemcount);


    });

    $('#btnnadd').on('click', function(){

        nitemcount += 1;
        nitemarray.push(nitemcount);
        $('#tblngroupcontent').append('<tr id="new'+nitemcount+'"><td><select name="cmbngroup[]" id="cmbngroup'+nitemcount+'" class="form-control" style="width: 350px;"></select></td><td><button id="btnnremove" name="btnnremove" class="btn btn-flat btn-danger" style="float: right;" value="'+nitemcount+'"><i class="fa fa-trash"></i></button></td></tr>');
        LoadGroups('cmbngroup'+nitemcount);

    });

    $(document).on('click', '#btnnremove', function(){

        var val = $(this).val();
        $('#new'+val).remove();

    });

    $(document).on('click', '#btnedit', function(){

        //Get Value
        sid = $(this).val();

        //Clear
        ClearUpdateGroup();

        //Set
        uitemarray = [];
        uitemcount = 1;
        ditemarray = [];
        mon = 0;
        tue = 0;
        wed = 0;
        thu = 0;
        fri = 0;
        sun = 0;
        sat = 0;

        $('#updatenotificationsched').modal('toggle');
        LoadScheduleInformation();

    });

    //New Checkbox
    $('#chknmon').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            mon = 1;
        }else{
            mon = 0;
        }

    });

    $('#chknwed').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            wed = 1;
        }else{
            wed = 0;
        }

    });

    $('#chknfri').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            fri = 1;
        }else{
            fri = 0;
        }

    });

    $('#chknsat').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            sat = 1;
        }else{
            sat = 0;
        }

    });

    $('#chkntue').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            tue = 1;
        }else{
            tue = 0;
        }

    });

    $('#chknthu').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            thu = 1;
        }else{
            thu = 0;
        }

    });

    $('#chknsun').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            sun = 1;
        }else{
            sun = 0;
        }

    });
    //End New Checkbox

    //Update Checkbox
    $('#chkumon').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            mon = 1;
        }else{
            mon = 0;
        }

    });

    $('#chkuwed').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            wed = 1;
        }else{
            wed = 0;
        }

    });

    $('#chkufri').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            fri = 1;
        }else{
            fri = 0;
        }

    });

    $('#chkusat').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            sat = 1;
        }else{
            sat = 0;
        }

    });

    $('#chkutue').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            tue = 1;
        }else{
            tue = 0;
        }

    });

    $('#chkuthu').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            thu = 1;
        }else{
            thu = 0;
        }

    });

    $('#chkusun').on('click', function(){

        var val = $(this).is(":checked");
        if(val){
            sun = 1;
        }else{
            sun = 0;
        }

    });
    //End Update Checkbox

    $('#btnnsave').on('click', function(){

        var operation = $('#cmbnoperation').val();
        var name = $('#txtnschedulename').val();
        var description = $('#txtndescription').val();
        var time = $('#txtntime').val();
        var groups = $('[name="cmbngroup[]"]').serializeArray();

        $.ajax({
            url: '{{ url("api/schedule/saveschedule") }}',
            type: 'post',
            data: {
                operation: operation,
                name: name,
                description: description,
                time: time,
                groups: groups,
                mon: mon,
                tue: tue,
                wed: wed,
                thu: thu,
                fri: fri,
                sun: sun,
                sat: sat
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadSchedules();
                    $('#newnotificationsched .close').click();

                }

            }
        });


    });

    $('#btnuadd').on('click', function(){

        uitemarray.push(uitemcount);
        $('#tblugroupcontent').append('<tr id="update'+uitemcount+'"><td><select name="cmbugroup[]" id="cmbugroup'+uitemcount+'" class="form-control" style="width: 350px;"></select></td><td><button id="btnuremove" name="btnuremove" class="btn btn-flat btn-danger" style="float: right;" value="'+uitemcount+'"><i class="fa fa-trash"></i></button></td></tr>');
        LoadGroups('cmbugroup'+uitemcount);
        uitemcount += 1;

    });

    $(document).on('click', '#btnuremove', function(){

        var val = $(this).val();
        $('#update'+val).remove();

    });

    $('#btnusave').on('click', function(){

        var operation = $('#cmbuoperation').val();
        var name = $('#txtuschedulename').val();
        var description = $('#txtudescription').val();
        var time = $('#txtutime').val();
        var groups = $('[name="cmbugroup[]"]').serializeArray();

        $.ajax({
            url: '{{ url("api/schedule/updateschedule") }}',
            type: 'post',
            data: {
                id: sid,
                operation: operation,
                name: name,
                description: description,
                time: time,
                groups: groups,
                mon: mon,
                tue: tue,
                wed: wed,
                thu: thu,
                fri: fri,
                sun: sun,
                sat: sat
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadSchedules();
                    $('#updatenotificationsched .close').click();

                }

            }
        });

    });

    $(document).on('click', '#btndelete',function(){

        var val = $(this).val();

        $.ajax({
            url: '{{ url("api/schedule/removeschedule") }}',
            type: 'post',
            data: {
                id: val
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadSchedules();

                }

            }
        });

    });

    function LoadScheduleInformation(){

        $.ajax({
            url: '{{ url("api/schedule/loadscheduleinformation") }}',
            type: 'get',
            data: {
                id: sid
            },
            dataType: 'json',
            success: function(response){

                SetScheduleInformation(response);

            }
        });

    }

    function SetScheduleInformation(data){

        $('#cmbuoperation').val(data.operation).trigger('change');
        $('#txtuschedulename').val(data.name);
        $('#txtudescription').val(data.description);
        $('#txtutime').val(data.time);

        //Set Day Schedule
        if(data.mon!=0){
            $('#chkumon').prop('checked', true);
            mon = 1;
        }
        else{
            $('#chkumon').prop('checked', false);
            mon = 0;
        }

        if(data.wed!=0){
            $('#chkuwed').prop('checked', true);
            wed = 1;
        }
        else{
            $('#chkuwed').prop('checked', false);
            wed = 0;
        }

        if(data.fri!=0){
            $('#chkufri').prop('checked', true);
            fri = 1;
        }
        else{
            $('#chkufri').prop('checked', false);
            fri = 0;
        }

        if(data.sat!=0){
            $('#chkusat').prop('checked', true);
            sat = 1;
        }
        else{
            $('#chkusat').prop('checked', false);
            sat = 0;
        }

        if(data.tue!=0){
            $('#chkutue').prop('checked', true);
            tue = 1;
        }
        else{
            $('#chkutue').prop('checked', false);
            tue = 0;
        }

        if(data.thu!=0){
            $('#chkuthu').prop('checked', true);
            thu = 1;
        }
        else{
            $('#chkuthu').prop('checked', false);
            thu = 0;
        }

        if(data.sun!=0){
            $('#chkusun').prop('checked', true);
            sun = 1;
        }
        else{
            $('#chkusun').prop('checked', false);
            sun = 0;
        }

        SetScheduleGroup(data.group);

    }

    function SetScheduleGroup(data){

        for(var i=0;i<data.length;i++){

            if(i==0){

                uitemarray.push(uitemcount);
                $('#tblugroupcontent').append('<tr id="update'+uitemcount+'"><td><select name="cmbugroup[]" id="cmbugroup'+uitemcount+'" class="form-control" style="width: 350px;"></select></td><td></td></tr>');
                LoadGroupUpdate('cmbugroup'+uitemcount, data[i]["group_id"]);

            }
            else{

                uitemarray.push(uitemcount);
                $('#tblugroupcontent').append('<tr id="update'+uitemcount+'"><td><select name="cmbugroup[]" id="cmbugroup'+uitemcount+'" class="form-control" style="width: 350px;"></select></td><td><button id="btnuremove" name="btnuremove" class="btn btn-flat btn-danger" style="float: right;" value="'+uitemcount+'"><i class="fa fa-trash"></i></button></td></tr>');
                LoadGroupUpdate('cmbugroup'+uitemcount, data[i]["group_id"]);

            }


            uitemcount += 1;

        }

    }

    function LoadSchedules(){

        tblnotificationschedule = $('#tblnotificationschedule').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/schedule/loadschedules") }}',
            },
            columns : [
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'operation', name: 'operation'},
                {data: 'time', name: 'time'},
                {data: 'day', name: 'day'},
                {data: 'createdat', name: 'createdat'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    function ReloadSchedules(){

        tblnotificationschedule.ajax.reload();

    }


    function LoadGroups(id){

        $.ajax({
            url: '{{ url("api/schedule/loadgroup") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                $('#'+id).append('<option value="">Select a group</option>');
                for(var i=0;i<response.data.length;i++){

                    $('#'+id).append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');

                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function LoadGroupUpdate(id, val){

        $.ajax({
            url: '{{ url("api/schedule/loadgroup") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#'+id).find('option').remove();
                for(var i=0;i<response.data.length;i++){

                    $('#'+id).append('<option value="'+ response.data[i]["id"] +'">'+ response.data[i]["name"] +'</option>');

                }

                $('#'+id).select2({
                    theme: 'bootstrap'
                });

            },
            complete: function(){

                $('#'+id).val(val).trigger('change');

            }
        });

    }

    function SetSelect2(){

        $('#cmbnoperation').select2({
            theme: 'bootstrap'
        });

    }

    function ClearNewSchedule(){

        $('#txtnschedulename').val('');
        $('#txtndescription').val('');
        $('#txtntime').val('');
        $('#chknmon').prop('checked', false);
        $('#chknwed').prop('checked', false);
        $('#chknfri').prop('checked', false);
        $('#chknsat').prop('checked', false);
        $('#chkntue').prop('checked', false);
        $('#chknthu').prop('checked', false);
        $('#chknsun').prop('checked', false);

    }

    function ClearNewGroup(){

        for(var i=0;i<nitemarray.length;i++){
            $('#new'+nitemarray[i]).remove();
        }

    }

    function ClearUpdateGroup(){

        for(var i=0;i<uitemarray.length;i++){
            $('#update'+uitemarray[i]).remove();
        }

    }


</script>
@endsection
