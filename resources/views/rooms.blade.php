@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Rooms</h1>
                <button id="btnsyncrooms" name="btnsyncrooms" class="btn btn-primary" style="margin-bottom: 10px; float: right; margin-left: 10px;" title="Sync Room Information"><i class="fa fa-download"></i></button>
                <button id="btnsync" name="btnsync" class="btn btn-primary" style="margin-bottom: 10px; float: right;" title="Sync Information"><i class="fa fa-download"></i></button>
                <a href="{{ url("/rooms/roomtypegroup") }}" class="btn btn-primary" style="margin-bottom: 10px; margin-right: 10px; float: right;" title="Room Group"><i class="fa fa-gear"></i></a>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addroom" style="margin-bottom: 10px; margin-right: 10px; float: right;"><i class="fa fa-plus"></i> Add Rooms</button>
                <input type="text" id="txtsearch" name="txtsearch" class="form-control" placeholder="Room #" style="float: right; margin-bottom: 10px;">
            </div>

            <div class="col-lg-12">

                <div id="roomcontent">

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
{{-- Modal --}}
@include('modals.rooms.addroom')
@endsection

@section('scripts')
<script>

    $(document).ready(function(){

        LoadRooms();
        getDD();
        CheckRoomCount();

    });

    $('#saveRoom').on('click', function(){

        var roomNo = $('#roomNo').val();
        var roomArea = $('#roomArea').val();
        var roomName = $('#roomName').val();
        var roomType = $('#roomType').val();
        var roomDesc = $('#roomDesc').val();
        $.ajax({
            url: '{{ url("api/room/addRoom") }}',
            method: 'POST',
            dataType: 'json',
            data:{
                'roomNo': roomNo,
                'roomArea': roomArea,
                'roomName': roomName,
                'roomType': roomType,
                'roomDesc': roomDesc
            },
            success:function(response){
                
                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }
                else{

                    toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

    });

    $('#txtsearch').on('keyup', function(){
        
        var search = $(this).val();
        SearchRoom(search);

    });

    $('#btnsync').on('click', function(){

        $.confirm({
              title: 'Sync',
              content: 'Sync The HMS Room Information?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        Sync();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    $('#btnsyncrooms').on('click', function(){

        $.confirm({
              title: 'Sync',
              content: 'Sync The HMS Rooms?',
              type: 'blue',
              buttons: {   
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SyncRooms();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){
                          
                        

                      }
                  } 
              }
          });

    });

    function SyncRooms(){

        $.ajax({
            url: '{{ url("api/room/syncroominformations") }}',
            type: 'post',
            dataType: 'json',
            beforeSend: function(){

                $('#body').waitMe({

                    effect : 'roundBounce',
                    text : '',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000'

                });

            },
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    CheckRoomCount();
                    LoadRooms();

                }

            },
            complete: function(){

                $('#body').waitMe('hide');

            }
        });

    }

    function Sync(){

        $.ajax({
            url: '{{ url("api/room/syncinformations") }}',
            type: 'post',
            dataType: 'json',
            beforeSend: function(){

                $('#body').waitMe({

                    effect : 'roundBounce',
                    text : '',
                    bg : 'rgba(255,255,255,0.7)',
                    color : '#000'

                });

            },
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    LoadRooms();

                }

            },
            complete: function(){

                $('#body').waitMe('hide');

            }
        });

    }

    function CheckRoomCount(){

        $.ajax({
            url: '{{ url("api/room/checkroomcount") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                if(response.count!=0){

                    $('#btnsyncrooms').prop('disabled', true);

                }
                else{

                    $('#btnsyncrooms').prop('disabled', false);

                }

            }
        });

    }

    function SearchRoom(search){

        $.ajax({
            url: '{{ url("api/room/loadroomsinformation") }}',
            type: 'get',
            data: {
                search: search
            },
            beforeSend: function(){
                $('#roomcontent').hide();
            },
            success: function(response){

                $('#roomcontent').html(response);

            },
            complete: function(){
                $('#roomcontent').fadeIn("slow");
            }
        });        


    }

    function LoadRooms(){

        $.ajax({
            url: '{{ url("api/room/loadroomsinformation") }}',
            type: 'get',
            beforeSend: function(){
                $('#roomcontent').hide();
            },
            success: function(response){

                $('#roomcontent').html(response);

            },
            complete: function(){
                $('#roomcontent').fadeIn("slow");
            }
        });        

    }

    function getDD(){
        $.ajax({
            url: '{{ url('api/room/loadRoomSelection') }}',
            type: 'get',
            dataType: 'json',
            success: function(r){
                console.log(r);
                $.each(r.roomArea, function(i, items){
                    $('#roomArea').append($('<option>', {
                        value: items.id,
                        text: items.room_area
                    }));
                });

                $.each(r.roomType, function(i, items){
                    $('#roomType').append($('<option>', {
                        value: items.id,
                        text: items.room_type
                    }));    
                });
            },
            error: function(r){
                console.log(r);
            }
        });
    }

</script>
@endsection