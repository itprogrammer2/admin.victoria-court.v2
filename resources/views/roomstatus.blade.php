@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Room Status</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12 row">
                    <a href="{{ url("roomstatus/grouproomstatus") }}"><button style="float: right; margin-bottom: 10px;" class="btn btn-flat btn-primary" title="Settings"><i class="fa fa-cog"></i></button></a>
                </div>

                <div class="col-lg-12 row">
                    <table id="tblroomstatus" class="table table-striped table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Room Status</th>
                                <th>Color</th>
                                <th>Blink</th>
                                <th>Timer</th>
                                <th>Name</th>
                                <th>Buddy</th>
                                <th>Cancelable</th>
                                <th>For Inspection</th>
                                {{-- <th></th> --}}
                            </tr>
                        </thead>
                    </table>
                </div>



            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

{{-- Modal --}}
@include('modals.roomstatus.checklist')


@endsection

@section('scripts')
<script>

    //Variables
    var tblroomstatus;
    var scheckliststatusid;
    var schkid;

    $(document).ready(function() {

        RoomStatusInformation();

    });

    $('#btnupdate').on('click', function(){

        var id = $(this).val();
        var room_status = $('#txturoomstatus').val();
        var color = $('#txtucolor').val();

        if(room_status==""){
            toastr.error('Please input the room status.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/roomstatus/updateroomstatus") }}',
                type: 'post',
                data: {
                    id: id,
                    room_status: room_status,
                    color: color
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadRoomStatus();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                        $('#uroomstatus .close').click();


                    }


                }
            });

        }

    });

    $(document).on('click', '#btnedit', function(){

        var id = $(this).val();
        $('#btnupdate').val(id);

        $.ajax({
            url: '{{ url("api/roomstatus/roomstatusprofile") }}',
            type: 'get',
            data: {
                id: id,
            },
            dataType: 'json',
            success: function(response){

                $('#txturoomstatus').val(response.room_status);
                $('#txtucolor').val(response.color);

            },
        });

    });

    $(document).on('change', '[name="chkblink[]"]', function(){

        var id = $(this).val();

        if($('#blink' + id).is(":checked")){

            $.ajax({
                url: '{{ url("api/roomstatus/enabledblink") }}',
                type: 'post',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(){

                }
            });

        }
        else{

            $.ajax({
                url: '{{ url("api/roomstatus/disabledblink") }}',
                type: 'post',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(){

                }
            });

        }

    });

    $(document).on('change', '[name="chktimer[]"]', function(){

        var id = $(this).val();

        if($('#timer' + id).is(":checked")){

            $.ajax({
                url: '{{ url("api/roomstatus/enabledtimer") }}',
                type: 'post',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(){

                }
            });

        }
        else{

            $.ajax({
                url: '{{ url("api/roomstatus/disabledtimer") }}',
                type: 'post',
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(){

                }
            });

        }

    });

    $(document).on('click', '[name="chkname[]"]', function(){

        var id = $(this).val();

        if($('#name' + id).is(":checked")){

           $.ajax({
               url: '{{ url("api/roomstatus/enabledname") }}',
               type: 'post',
               data: {
                   id: id
               },
               dataType: 'json',
               success: function(){

               }
           });

       }
       else{

           $.ajax({
               url: '{{ url("api/roomstatus/disabledname") }}',
               type: 'post',
               data: {
                   id: id
               },
               dataType: 'json',
               success: function(){

               }
           });

       }


    });

    $(document).on('click', '[name="chkbuddy[]"]', function(){

        var id = $(this).val();

        if($('#buddy' + id).is(":checked")){

           $.ajax({
               url: '{{ url("api/roomstatus/enabledbuddy") }}',
               type: 'post',
               data: {
                   id: id
               },
               dataType: 'json',
               success: function(){

               }
           });

       }
       else{

           $.ajax({
               url: '{{ url("api/roomstatus/disabledbuddy") }}',
               type: 'post',
               data: {
                   id: id
               },
               dataType: 'json',
               success: function(){

               }
           });

       }

    });

    $(document).on('click', '[name="chkcancel[]"]', function(){

        var id = $(this).val();

        if($('#cancel' + id).is(":checked")){

           $.ajax({
               url: '{{ url("api/roomstatus/enabledcancel") }}',
               type: 'post',
               data: {
                   id: id
               },
               dataType: 'json',
               success: function(){

               }
           });

       }
       else{

           $.ajax({
               url: '{{ url("api/roomstatus/disabledcancel") }}',
               type: 'post',
               data: {
                   id: id
               },
               dataType: 'json',
               success: function(){

               }
           });

       }

    });

    $(document).on('change', '[name="chkchecklist[]"]', function(){

        if($(this).is(':checked')){

            scheckliststatusid = $(this).val();
            schkid = $(this).data("id");

            console.log(schkid);

            //Load Standards
            LoadStandards();

            //Update
            UpdateChecklist();

            //Show Modal
            $('#checklist').modal({
                backdrop: 'static',
                keyboard: false
            });

        }
        else{

            scheckliststatusid = $(this).val();
            schkid = $(this).data("id");

            //Update
            DiUpdateChecklist();

        }


    });

    $('#btnclosechecklist').on('click', function(){

        ValidateChecklistID();
        $('#checklist').modal('toggle');

    });

    $(document).on('change', '[name="chkstd[]"]', function(){

        var stddata = $('[name="chkstd[]"]').serializeArray();

        $.ajax({
            url: '{{ url("api/roomstatus/updatecheckliststd") }}',
            type: 'post',
            data: {
                id: scheckliststatusid,
                stddata: stddata
            },
            dataType: 'json',
            success: function(response){



            }
        });

    });

    function ValidateChecklistID(){

        $.ajax({
            url: '{{ url("api/roomstatus/validatechecklistid") }}',
            type: 'get',
            data: {
                id: scheckliststatusid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){
                    // Do Nothing
                }
                else{
                    DiUpdateChecklist();
                    $('#checklist'+schkid).prop('checked', false);
                }

            }
        });

    }

    function LoadStandards(){

        $.ajax({
            url: '{{ url("api/roomstatus/loadstandards") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#tblcheckliststandardcontent').find('tr').remove();
                for(var i=0;i<response.data.length;i++){

                    $('#tblcheckliststandardcontent').append('<tr> <td style="vertical-align: middle;">'+ response.data[i]["name"] +'</td> <td><div class="checkbox"><label><input type="checkbox" id="chkstd'+ response.data[i]["id"] +'" name="chkstd[]" data-id="'+ response.data[i]["id"] +'" value="'+ response.data[i]["id"] +'"></label></div></td> </tr>');

                }

            }
        });

    }

    function UpdateChecklist(){

        $.ajax({
            url: '{{ url("api/roomstatus/updateforchecklist") }}',
            type: 'post',
            data: {
                id: scheckliststatusid
            },
            dataType: 'json',
            success: function(response){



            }
        });

    }

    function DiUpdateChecklist(){

        $.ajax({
            url: '{{ url("api/roomstatus/diupdateforchecklist") }}',
            type: 'post',
            data: {
                id: scheckliststatusid
            },
            dataType: 'json',
            success: function(response){



            }
        });

    }

    function RoomStatusInformation(){

        tblroomstatus = $('#tblroomstatus').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/roomstatus/roomstatusinformation") }}',
            },
            columns : [
                {data: 'roomstatus', name: 'roomstatus'},
                {data: 'color', name: 'color'},
                {data: 'blink', name: 'blink'},
                {data: 'timer', name: 'timer'},
                {data: 'name', name: 'name'},
                {data: 'buddy', name: 'buddy'},
                {data: 'close', name: 'close'},
                {data: 'checklist', name: 'checklist'},
                // {data: 'panel', name: 'panel'},
            ]
        });

    }

    function ReloadRoomStatus(){

        tblroomstatus.ajax.reload();

    }



</script>
@endsection
