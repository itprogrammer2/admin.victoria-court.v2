@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Settings</h1>
            </div>

            <div class="col-lg-12">

                <div class="container-fluid row">

                    <ul id="navigation" class="nav nav-tabs nav-justified">
                        <li class="active"><a data-toggle="tab" href="#main">Main</a></li>
                        <li><a data-toggle="tab" href="#auto">Auto</a></li>
                        <li><a data-toggle="tab" href="#points">Points</a></li>
                        <li><a data-toggle="tab" href="#grade">Grading</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="main" class="tab-pane fade in active">

                            <br>
                            <div class="col-md-12 row">
                                <label for="">Local Setting</label>
                                <hr style="border: 1px solid black; margin-top: 0px;">
                            </div>
                            <div class="form-group col-md-12 row">
                                <label for="txtlocalid">Local ID</label>
                                <input id="txtlocalid" name="txtlocalid" type="text" class="form-control" placeholder="Local ID">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtlocalcode">Local Code</label>
                                <input id="txtlocalcode" name="txtlocalcode" type="text" class="form-control" placeholder="Local Code">
                            </div>

                            <div class="col-md-12 row">
                                <label for="">Room Balancer Setting</label>
                                <hr style="border: 1px solid black; margin-top: 0px;">
                            </div>

                            <div class="form-group col-md-12 row">
                                <table id="tblroombalance" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="vertical-align: middle; width: 25%;">#</th>
                                            <th style="vertical-align: middle; width: 25%;">Group Name</th>
                                            <th style="vertical-align: middle; width: 25%;">Percentage</th>
                                            <th style="vertical-align: middle; width: 10%; text-align: center;"><button id="btnaddroombalance" name="btnaddroombalance" class="btn btn-success"><i class="fa fa-plus"></i></button></th>
                                        </tr>
                                    </thead>
                                    <tbody id="tblroombalancecontent">
                                    </tbody>
                                </table>
                            </div>


                        </div>
                        <div id="auto" class="tab-pane fade">

                            <br>
                            <div class="col-md-12 row">
                                <label for="">Auto Setting</label>
                                <hr style="border: 1px solid black; margin-top: 0px;">
                            </div>
                            <div class="form-group col-md-12 row">
                                <label for="txtreservationlocktime">Reservation Locked Out Time (No. of Hour)</label>
                                <input id="txtreservationlocktime" name="txtreservationlocktime" type="text" class="form-control" placeholder="Reservation Locked Out Time">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtreservationallowance">Reservation Allowance (No. of Hour)</label>
                                <input id="txtreservationallowance" name="txtreservationallowance" type="text" class="form-control" placeholder="Reservation Allowance">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtautogc">Automatic General Cleaning (No. of Days)</label>
                                <input id="txtautogc" name="txtautogc" type="text" class="form-control" placeholder="Automatic GC">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtautogccheckout">Automatic General Cleaning (No. of Checkout)</label>
                                <input id="txtautogccheckout" name="txtautogccheckout" type="text" class="form-control" placeholder="Automatic GC Checkout">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtautopm">Automatic Preventive Maintenance (No. of Days)</label>
                                <input id="txtautopm" name="txtautopm" type="text" class="form-control" placeholder="Automatic PM">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtautopmcheckout">Automatic Preventive Maintenance (No. of Checkout)</label>
                                <input id="txtautopmcheckout" name="txtautopmcheckout" type="text" class="form-control" placeholder="Automatic PM Checkout">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtautocleanreclean">Automated Clean to Re-Clean (No. of Hours Remain Clean)</label>
                                <input id="txtautocleanreclean" name="txtautocleanreclean" type="text" class="form-control" placeholder="Automated Clean To Re-Clean">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtautodirtyreclean">Automated Dirty to Re-Clean (No. of Hours Remain Dirty)</label>
                                <input id="txtautodirtyreclean" name="txtautodirtyreclean" type="text" class="form-control" placeholder="Automated Dirty To Re-Clean">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtlongstaying12">Long Staying Guest (No. Concurrent Staying of Guest for 12 Hours)</label>
                                <input id="txtlongstaying12" name="txtlongstaying12" type="text" class="form-control" placeholder="Long Staying Guest 12 Hours">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtlongstaying24">Long Staying Guest (No. Concurrent Staying of Guest for 24 Hours)</label>
                                <input id="txtlongstaying24" name="txtlongstaying24" type="text" class="form-control" placeholder="Long Staying Guest 24 Hours">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtwindowtimegcstart">Window Time for General Cleaning Start (Military Time)</label>
                                <input id="txtwindowtimegcstart" name="txtwindowtimegcstart" type="text" class="form-control" placeholder="Window Time GC Start">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtwindowtimegcend">Window Time for General Cleaning End (Military Time)</label>
                                <input id="txtwindowtimegcend" name="txtwindowtimegcend" type="text" class="form-control" placeholder="Window Time GC End">
                            </div>


                            <div class="form-group col-md-12 row">
                                <label for="txtbelopercentage">Automatic BELO (Percentage)</label>
                                <input id="txtbelopercentage" name="txtbelopercentage" type="text" class="form-control" placeholder="Automatic BELO Percentage">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtbelo">Automatic BELO (No. Checkout)</label>
                                <input id="txtbelo" name="txtbelo" type="text" class="form-control" placeholder="Automatic BELO">
                            </div>

                            <div class="form-group col-md-12 row">
                                <label for="txtpest">Recovery Duration (No. Hours)</label>
                                <input id="txtpest" name="txtpest" type="text" class="form-control" placeholder="Recovery Duration">
                            </div>

                        </div>
                        <div id="points" class="tab-pane fade">

                            <br>
                            <div class="col-md-12">

                                <table id="tblgrouproomtype" class="table table-striped table-bordered" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Room Type</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                </table>

                            </div>

                        </div>
                        <div id="grade" class="tab-pane fade">
                            <br>
                            <div class="col-md-12">

                                <div class="col-md-12 row">
                                    <button id="btnaddscore" name="btnaddscore" class="btn btn-flat btn-success" style="float: right; margin-bottom: 10px;"><i class="fa fa-plus"></i> New Rating</button>
                                </div>
                                <div class="col-md-12 row">
                                    <table id="tblscorerating" class="table table-striped table-bordered" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Score</th>
                                                    <th>Rating</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 row">
                            <button id="btnsave" name="btnsave" class="btn btn-flat btn-success" style="float: right;"><i class="fa fa-save"></i> Save</button>
                    </div>

                </div>


            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

     @include('modals.settings.newrating')
     @include('modals.settings.updaterating')
     @include('modals.settings.addnewgrproompercent')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    //Variables
    var tblgrouproomtype;
    var tblscorerating;
    var tblroombalance;
    var sid;

    $(document).ready(function(){

        LoadSettings();
        LoadRoomTypeGroup();
        LoadScoreRating();
        LoadGRPRoom();

    });

    $('#btnsave').on('click', function(){

       var localid = $('#txtlocalid').val();
       var localcode = $('#txtlocalcode').val();
       var reservationlocktime = $('#txtreservationlocktime').val();
       var reservationallowance = $('#txtreservationallowance').val();
       var autogc = $('#txtautogc').val();
       var autogccheckout = $('#txtautogccheckout').val();
       var autopm = $('#txtautopm').val();
       var autopmcheckout = $('#txtautopmcheckout').val();
       var autocleanreclean = $('#txtautocleanreclean').val();
       var autodirtyreclean = $('#txtautodirtyreclean').val();
       var stayingguest12 = $('#txtlongstaying12').val();
       var stayingguest24 = $('#txtlongstaying24').val();
       var windowtimegcstart = $('#txtwindowtimegcstart').val();
       var windowtimegcend = $('#txtwindowtimegcend').val();
       var belopercentage = $('#txtbelopercentage').val();
       var belo = $('#txtbelo').val();
       var pest = $('#txtpest').val();

       $.ajax({
           url: '{{ url("api/settings/savesettings") }}',
           type: 'post',
           data: {
            localid: localid,
            localcode: localcode,
            reservationlocktime: reservationlocktime,
            reservationallowance: reservationallowance,
            autogc: autogc,
            autogccheckout: autogccheckout,
            autopm: autopm,
            autopmcheckout: autopmcheckout,
            autocleanreclean: autocleanreclean,
            autodirtyreclean: autodirtyreclean,
            stayingguest12: stayingguest12,
            stayingguest24: stayingguest24,
            windowtimegcstart: windowtimegcstart,
            windowtimegcend: windowtimegcend,
            belopercentage: belopercentage,
            belo: belo,
            pest: pest
           },
           dataType: 'json',
           success: function(response){

               if(response.success){

                   toastr.success(response.message, '', { positionClass: 'toast-top-center' });

               }

           }
       });

    });

    $('#navigation').on('click', "li", function(event){

        var activeTab = $(this).find('a').attr('href').replace('#', '');

        if(activeTab=="points" || activeTab=="grade"){
            $('#btnsave').hide();
        }
        else{
            $('#btnsave').fadeIn();
        }

    });

    $('#btnaddscore').on('click', function(){

        ClearNewRating();
        $('#newrating').modal('toggle');

    });

    $('#btnaddroombalance').on('click', function(){

        ClearGRP();
        LoadGRPType();
        $('#addnewgrproompercent').modal('toggle');

    });

    $('#btnnsave').on('click', function(){

        var scorefrom = $('#txtnscorefrom').val();
        var scoreto = $('#txtnscoreto').val();
        var rating = $('#txtnrating').val();

        if(scorefrom==""){
            toastr.error('Please input the from score.', '', { positionClass: 'toast-top-center' });
        }
        else if(scoreto==""){
            toastr.error('Please input the to score.', '', { positionClass: 'toast-top-center' });
        }
        else if(rating==""){
            toastr.error('Please input the rating score.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/settings/savenewrating") }}',
                type: 'post',
                data: {
                    scorefrom: scorefrom,
                    scoreto: scoreto,
                    rating: rating
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#newrating .close').click();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadScoreRating();

                    }

                }
            });

        }

    });

    $(".allow_decimal").on("input", function(evt) { //Numbers And Decimal Only

        var self = $(this);
        self.val(self.val().replace(/[^0-9\.]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
        {
            evt.preventDefault();
        }

    });

    $(document).on('click', '#btnupdaterating', function(){

        sid = $(this).val();
        ClearUpdateRating();
        $('#updaterating').modal('toggle');

        LoadRatingInformation();

    });

    $('#btnsavegrproompercent').on('click', function(){

        $.confirm({
              title: 'Save',
              content: 'Save The Room Type Percentage?',
              type: 'blue',
              buttons: {
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        SaveRoomTypePercentage();

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){



                      }
                  }
              }
          });

    });

    $('#btnuupdate').on('click', function(){

       var scorefrom = $('#txtuscorefrom').val();
       var scoreto = $('#txtuscoreto').val();
       var rating = $('#txturating').val();

        if(scorefrom==""){
            toastr.error('Please input the from score.', '', { positionClass: 'toast-top-center' });
        }
        else if(scoreto==""){
            toastr.error('Please input the to score.', '', { positionClass: 'toast-top-center' });
        }
        else if(rating==""){
            toastr.error('Please input the rating score.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/settings/updaterating") }}',
                type: 'post',
                data: {
                    sid: sid,
                    scorefrom: scorefrom,
                    scoreto: scoreto,
                    rating: rating
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#updaterating .close').click();
                        ReloadScoreRating();

                    }

                }
            });


        }

    });

    $(document).on('click', '#btndelete', function(){

        sid = $(this).val();

        $.ajax({
            url: '{{ url("api/settings/deleterating") }}',
            type: 'post',
            data: {
                sid: sid
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadScoreRating();

                }

            }
        });

    });

    $(document).on('click', '#btnremovegrpbalancer', function(){

        var id = $(this).val();

        $.confirm({
              title: 'Delete',
              content: 'Delete The Room Type Percentage?',
              type: 'blue',
              buttons: {
                  ok: {
                      text: "Yes",
                      btnClass: 'btn-info',
                      keys: ['enter'],
                      action: function(){

                        DeleteRoomTypePercentage(id);

                      }
                  },
                  cancel: {
                      text: "No",
                      btnClass: 'btn-info',
                      action: function(){



                      }
                  }
              }
          });


    });

    function DeleteRoomTypePercentage(id){

        $.ajax({
            url: '{{ url("api/settings/deleteroomtypepercentage") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    RefreshGRPRoom();
                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });

                }

            }
        });

    }

    function SaveRoomTypePercentage(){

        var order = $('#txtordergrp').val();
        var grpid = $('#cmbngrpname').val();
        var percentage = $('#txtgrppercentage').val();

        //Validation
        if(grpid==""){

            toastr.error('Please select group name.', '', { positionClass: 'toast-top-center' });

        }
        else if(percentage==""){

            toastr.error('Please input the percentage of the group name.', '', { positionClass: 'toast-top-center' });

        }
        else{

            $.ajax({
                url: '{{ url("api/settings/saveroomtypepercentage") }}',
                type: 'post',
                data: {
                    order: order,
                    grpid: grpid,
                    percentage: percentage
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        $('#addnewgrproompercent .close').click();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        RefreshGRPRoom();

                    }

                }
            });

        }

    }

    function LoadRatingInformation(){

        $.ajax({
            url: '{{ url("api/settings/loadratinginformation") }}',
            type: 'get',
            data: {
                sid: sid
            },
            dataType: 'json',
            success: function(response){

                $('#txtuscorefrom').val(response.score_from);
                $('#txtuscoreto').val(response.score_to);
                $('#txturating').val(response.rating);

            }
        });


    }

    function ClearNewRating(){

        $('#txtnscorefrom').val('');
        $('#txtnscoreto').val('');
        $('#txtnrating').val('');

    }

    function ClearUpdateRating(){

        $('#txtuscorefrom').val('');
        $('#txtuscoreto').val('');
        $('#txturating').val('');

    }

    function LoadRoomTypeGroup(){

        tblgrouproomtype = $('#tblgrouproomtype').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/settings/loadroomtypegroup") }}'
            },
            columns : [
                {data: 'roomtype', name: 'roomtype', width: '50'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    function LoadScoreRating(){

        tblscorerating = $('#tblscorerating').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/settings/loadscorerating") }}'
            },
            columns : [
                {data: 'id', name: 'id', width: '50'},
                {data: 'score', name: 'score'},
                {data: 'rating', name: 'rating'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    function ReloadScoreRating(){

        tblscorerating.ajax.reload();

    }

    function LoadSettings(){

        $.ajax({
            url: '{{ url("api/settings/loadsettings") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                SetMain(response.settings);
                SetAuto(response.settings);

            }
        });

    }

    function LoadGRPType(){

        $.ajax({
            url: '{{ url("api/settings/loadgrptype") }}',
            type: 'get',
            dataType: 'json',
            success: function(response){

                $('#cmbngrpname').find('option').remove();
                $('#cmbngrpname').append('<option value="">Select Room Type</option>' + response.content);
                $('#cmbngrpname').select2({
                    theme: 'bootstrap'
                });

            }
        });

    }

    function ClearGRP(){

        $('#txtordergrp').val('');
        $("#txtgrppercentage").val('');

    }

    function LoadGRPRoom(){

        tblroombalance = $('#tblroombalance').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            ajax: {
                type: 'get',
                url: '{{ url("api/settings/loadroombalancer") }}'
            },
            columns : [
                {data: 'order', name: 'order'},
                {data: 'groupname', name: 'groupname'},
                {data: 'percentage', name: 'percentage'},
                {data: 'panel', name: 'panel'},
            ]
        });

    }

    function RefreshGRPRoom(){

        tblroombalance.ajax.reload();

    }

    function SetMain(data){

        $('#txtlocalid').val(data[0]["local_id"]);
        $('#txtlocalcode').val(data[0]["local_code"]);

    }

    function SetAuto(data){

        $('#txtreservationlocktime').val(data[0]["reservation_locked_out_time"]);
        $('#txtreservationallowance').val(data[0]["reservation_allowance"]);
        $('#txtautogc').val(data[0]["automatic_general_cleaning_days"]);
        $('#txtautogccheckout').val(data[0]["automatic_general_cleaning_checkout"]);
        $('#txtautopm').val(data[0]["automatic_preventive_maintenance_days"]);
        $('#txtautopmcheckout').val(data[0]["automatic_preventive_maintance_checkout"]);
        $('#txtautocleanreclean').val(data[0]["automated_clean_reclean"]);
        $('#txtautodirtyreclean').val(data[0]["automated_dirty_reclean"]);
        $('#txtlongstaying12').val(data[0]["staying_guest_12"]);
        $('#txtlongstaying24').val(data[0]["staying_guest_24"]);
        $('#txtwindowtimegcstart').val(data[0]["window_time_general_cleaning_start"]);
        $('#txtwindowtimegcend').val(data[0]["window_time_general_cleaning_end"]);
        $('#txtbelopercentage').val(data[0]["automatic_belo_percentage"]);
        $('#txtbelo').val(data[0]["automatic_belo_checkout"]);
        $('#txtpest').val(data[0]["recovery_time"]);

        //access_restricted: 0
        //user_restricted: 0

    }


</script>
@endsection
