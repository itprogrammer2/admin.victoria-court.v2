@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">User Access</h1>
            </div>

            <div class="col-lg-12">

                <div class="panel panel-default">
        
                        <div class="panel-heading">
                            <h4>User Role</h4>
                        </div>

                        <div class="panel-body">
                            <div class="col-lg-12">
                                <button id="btnaddrole" name="btnaddrole" class="btn btn-flat btn-success" style="float: right; margin-bottom: 20px;" data-toggle="modal" data-target="#adduseraccessrole"><i class="fa fa-plus"></i> Add User Role</button>
                            </div>
                            <div class="col-lg-12">
                                <table id="tbluserrole" class="table table-striped table-bordered" style="width: 100%">
                                       <thead>
                                          <tr>
                                               <th>Id</th>
                                               <th>Role</th>
                                               <th></th>
                                           </tr>
                                       </thead>
                                </table>
                            </div>
                            
                        </div>
                      

                </div>

                <div class="panel panel-default">
        
                        <div class="panel-heading">
                            <h4>Resources</h4>
                        </div>

                        <div class="panel-body">

                            <div class="col-lg-12">
                                <button id="btnaddresources" name="btnaddresources" class="btn btn-flat btn-success" style="float: right; margin-bottom: 20px;" data-toggle="modal" data-target="#addresources"><i class="fa fa-plus"></i> Add Resources</button>
                            </div>

                            <div class="col-lg-12">
                                <table id="tblresources" class="table table-striped table-bordered" style="width: 100%">
                                       <thead>
                                          <tr>
                                               <th>Id</th>
                                               <th>Resources</th>
                                               <th>Description</th>
                                               <th></th>
                                           </tr>
                                       </thead>
                                </table>
                            </div>

                        </div>
                      

                </div>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

    {{-- Modal --}}
    @include('modals.useraccess.adduseraccessrole')
    @include('modals.useraccess.addresources')

</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    var tbluserrole;
    var tblresources;

    $(document).ready(function(){

        LoadUserRoles();
        LoadResources();

    });

    $('#btnaadduserrole').on('click', function(){

        var userrole = $('#txtauserrole').val();

        if(userrole==""){
            toastr.error('Please input the role name.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/useraccess/adduserrole") }}',
                type: 'post',
                data: {
                    role: userrole
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadUserRole();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#adduseraccessrole .close').click();

                    }
                    else{

                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });

                    }

                }
            });

        }

    });

    $('#btnaddrole').on('click', function(){

        ClearNewUserRole();

    });

    $('#btnaddresources').on('click', function(){

        ClearNewResources();

    });

    $('#btnaaddresources').on('click', function(){
        
        var resources = $('#txtaresources').val();
        var description = $('#txtadescription').val();

        if(resources==""){
            toastr.error('Please input resources name.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/useraccess/saveresources") }}',
                type: 'post',
                data: {
                    resources: resources,
                    description: description,
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        ReloadResources();
                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        $('#addresources .close').click();

                    }
                    else{
                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                    }                

                }
            });

        }

    });

    

    function LoadUserRoles(){

        tbluserrole = $('#tbluserrole').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/useraccess/loaduserroles") }}',
            },
            columns : [
                {data: 'id', name: 'id'},
                {data: 'role', name: 'role'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

    function LoadResources(){

        tblresources = $('#tblresources').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/useraccess/loadresources") }}',
            },
            columns : [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

    function ClearNewUserRole(){

        $('#txtauserrole').val('');

    }

    function ClearNewResources(){

        $('#txtaresources').val('');
        $('#txtadescription').val('');
  
    }

    function ReloadUserRole(){

        tbluserrole.ajax.reload();

    }

    function ReloadResources() {
        
        tblresources.ajax.reload();

    }

</script>
@endsection