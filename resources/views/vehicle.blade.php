@extends('layout.index')

@section('content')

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Vehicles</h1>
            </div>

            <div class="col-lg-12">

                <div class="col-lg-12 row">
                    <button id="btnaddvehicle" name="btnaddvehicle" class="btn btn-success btn-flat" style="float: right; margin-bottom: 10px;" data-toggle="modal" data-target="#newvehicle"><i class="fa fa-plus"></i> New Vehicle</button>
                </div>

                <div class="col-lg-12 row">
                    <table id="tblvehicle" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Vechile Name</th>
                                <th>Plate Number</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
     <!-- /.container-fluid -->

     {{-- Modal --}}
    @include('modals.vehicle.newvehicle')
    @include('modals.vehicle.updatevehicle')
    
</div>
<!-- /#page-wrapper -->

@endsection

@section('scripts')
<script>

    //Variables
    var tblvehicle;

    $(document).ready(function(){

        LoadVehicleInformation();

    });

    $('#btnaddvehicle').on('click', function(){

        ClearNewVehicleInformation();

    });

    $('#btnnsave').on('click',async function(clickEvent){

       var name = $('#txtnname').val();
       var platenumber = $('#cmbnplatenumber').val();
       
       if(name==""){
            toastr.error('Please Input The Vehicle Name', '', { positionClass: 'toast-top-center' });
       }
       else if(platenumber==""){
            toastr.error('Please Select If The Plate Number Is Enable/Disable.', '', { positionClass: 'toast-top-center' });
       }
       else{
            
           $.ajax({
               url: '{{ url("api/vehicle/newvehicle") }}',
               type: 'post',
               data: {
                   name: name,
                   platenumber: platenumber
               },
               dataType: 'json',
               success: function(response){

                   if(response.success){

                       toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                       ReloadVehicleInformation();
                       $('#newvehicle .close').click();                      

                   }
                   else{
                        toastr.error(response.message, '', { positionClass: 'toast-top-center' });
                   }

               }
           });

       }        
 
    });

    $(document).on('click', '#btnremove', function(){

        var id = $(this).val();

        $.ajax({
            url: '{{ url("api/vehicle/removevehicle") }}',
            type: 'post',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                if(response.success){

                    toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                    ReloadVehicleInformation();

                }

            }
        });

    });

    $(document).on('click', '#btnedit', function(){

        var id = $(this).val();
        $('#btnuupdate').val(id);
        
        $.ajax({
            url: '{{ url("api/vehicle/profilevehicle") }}',
            type: 'get',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(response){

                $('#txtuname').val(response.name);
                $('#cmbuplatenumber').val(response.platenumber);

            }
        });

    });

    $('#btnuupdate').on('click', function(){

        var id = $(this).val();
        var name = $('#txtuname').val();
        var platenumber = $('#cmbuplatenumber').val();

        if(name==""){
                toastr.error('Please Input The Vehicle Name', '', { positionClass: 'toast-top-center' });
        }
        else if(platenumber==""){
                toastr.error('Please Select If The Plate Number Is Enable/Disable.', '', { positionClass: 'toast-top-center' });
        }
        else{

            $.ajax({
                url: '{{ url("api/vehicle/updatevehicle") }}',
                type: 'post',
                data: {
                    id: id,
                    name: name,
                    platenumber: platenumber
                },
                dataType: 'json',
                success: function(response){

                    if(response.success){

                        toastr.success(response.message, '', { positionClass: 'toast-top-center' });
                        ReloadVehicleInformation();
                        $('#updatevehicle .close').click();

                    }

                }
            });

        }
        

    });

    function LoadVehicleInformation(){

        tblvehicle = $('#tblvehicle').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'get',
                url: '{{ url("api/vehicle/loadvehicleinformation") }}',
            },
            columns : [
                {data: 'name', name: 'name'},
                {data: 'platenumber', name: 'platenumber'},
                {data: 'panel', name: 'panel', width: '15%'},
            ]
        });

    }

    function ClearNewVehicleInformation(){

        $('#txtnname').val('');
        $('#cmbnplatenumber').val('');

    }

    function ReloadVehicleInformation(){

        tblvehicle.ajax.reload();

    }


</script>
@endsection