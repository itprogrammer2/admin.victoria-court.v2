<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Login API
Route::post('login/loginuser', 'LoginController@Login');
Route::post('login/logoutuser', 'LoginController@Logout');

//Main API
Route::get('systemactivities/loadsystemactivities', 'MainController@LoadSystemActivities');

//Room Status API
Route::get('roomstatus/roomstatusinformation', 'RoomStatusController@RoomStatusInformation');
Route::get('roomstatus/roomstatusprofile', 'RoomStatusController@RoomStatusProfile');
Route::post('roomstatus/updateroomstatus', 'RoomStatusController@UpdateRoomStatus');
Route::get('roomstatus/grouproomstatusinformation', 'RoomStatusController@GroupRoomStatusInformation');
Route::get('roomstatus/loadroomstatus', 'RoomStatusController@LoadRoomStatus');
Route::post('roomstatus/savegrouproomstatus', 'RoomStatusController@SaveGroupRoomStatus');
Route::get('roomstatus/loadroomstatusprofile', 'RoomStatusController@LoadRoomStatusProfile');
Route::post('roomstatus/deleteroomgroupstatus', 'RoomStatusController@DeleteRoomGroupStatus');
Route::post('roomstatus/updateroomgroupstatus', 'RoomStatusController@UpdateRoomGroupStatus');
Route::post('roomstatus/enabledblink', 'RoomStatusController@EnabledBlink');
Route::post('roomstatus/disabledblink', 'RoomStatusController@DisabledBlink');
Route::post('roomstatus/enabledtimer', 'RoomStatusController@EnabledTimer');
Route::post('roomstatus/disabledtimer', 'RoomStatusController@DisabledTimer');
Route::post('roomstatus/enabledname', 'RoomStatusController@EnabledName');
Route::post('roomstatus/disabledname', 'RoomStatusController@DisabledName');
Route::post('roomstatus/enabledbuddy', 'RoomStatusController@EnabledBuddy');
Route::post('roomstatus/disabledbuddy', 'RoomStatusController@DisabledBuddy');
Route::post('roomstatus/enabledcancel', 'RoomStatusController@EnabledCancel');
Route::post('roomstatus/disabledcancel', 'RoomStatusController@DisabledCancel');
Route::get('roomstatus/loadstandards', 'RoomStatusController@LoadStandards');
Route::post('roomstatus/updateforchecklist', 'RoomStatusController@UpdateForChecklist');
Route::post('roomstatus/diupdateforchecklist', 'RoomStatusController@DiUpdateForChecklist');
Route::post('roomstatus/updatecheckliststd', 'RoomStatusController@UpdateChecklistSTD');
Route::get('roomstatus/validatechecklistid', 'RoomStatusController@ValidateChecklistID');

//Room API
Route::get('room/loadRoomSelection', 'RoomController@loadRoomSelection');
Route::post('room/addRoom', 'RoomController@addRooms');
Route::get('room/loadroomsinformation', 'RoomController@LoadRoomInformation');
Route::get('room/loadroomprofile', 'RoomController@LoadRoomProfile');
Route::post('room/updateroomdetails', 'RoomController@UpdateRoomDetails');
Route::get('room/loadinvitems', 'RoomController@LoadINVItems');
Route::get('room/loadroominventory', 'RoomController@LoadRoomInventory');
Route::post('room/addroominventory', 'RoomController@AddRoomInventory');
Route::post('room/removeroominventory', 'RoomController@RemoveRoomInventory');
Route::post('room/uploadroomimage', 'RoomController@UploadRoomImage');
Route::get('room/loadroomimages', 'RoomController@LoadRoomImages');
Route::post('room/removeroomimage', 'RoomController@RemoveRoomImage');
Route::get('room/loadstatusoveride', 'RoomController@LoadStatusOveride');
Route::post('room/overideroomstatus', 'RoomController@OverideRoomStatus');
Route::get('room/loadroomrates', 'RoomController@LoadRoomRates');
Route::get('rooms/loadroomtypes', 'RoomController@LoadRoomTypes');
Route::post('rooms/saveroomtypeinformation', 'RoomController@SaveRoomTypeInformation');
Route::get('rooms/loadgrouproomtype', 'RoomController@LoadGroupRoomType');
Route::get('rooms/getroomtypeinformation', 'RoomController@GetRoomTypeInformation');
Route::post('rooms/updateroomtype', 'RoomController@UpdateRoomType');
Route::get('rooms/loadautogrouproomtype', 'RoomController@LoadAutoGroupRoomType');
Route::get('rooms/loadrtypes', 'RoomController@LoadRTypes');
Route::post('rooms/savegrouproomtypeinformation', 'RoomController@SaveGroupRoomTypeInformation');
Route::post('rooms/deletegrouproomtypeinformation', 'RoomController@DeleteGroupRoomTypeInformation');
Route::get('rooms/loadgrouproomtypeitems', 'RoomController@LoadGroupRoomTypeItems');
Route::post('rooms/updategrouproomtypeinformation', 'RoomController@UpdateGroupRoomTypeInformation');
Route::get('room/loadroomtype', 'RoomController@LoadRoomType');
Route::post('room/syncinformations', 'RoomController@SyncInformations');
Route::post('room/syncroominformations', 'RoomController@SyncRoomInformations');
Route::get('room/checkroomcount', 'RoomController@CheckRoomCount');


// Inspection API
Route::get('inspection/room/getInfo', 'EagleEyeController@getRoomInspection');
Route::post('inspection/room', 'EagleEyeController@saveRoomForInspection');
Route::get('inspection/component/getInfo', 'EagleEyeController@getComponentInspection');
Route::post('inspection/component', 'EagleEyeController@saveComponentForInspection');
Route::get('inspection/standard/getInfo', 'EagleEyeController@getStandardInspection');
Route::post('inspection/standard', 'EagleEyeController@saveStandardForInspection');
Route::get('inspection/remarks/getInfo', 'EagleEyeController@getRemarksInspection');
Route::post('inspection/remarks', 'EagleEyeController@saveRemarksForInspection');
Route::get('inspection/finding/getInfo', 'EagleEyeController@getFindingsInspection');
Route::post('inspection/finding', 'EagleEyeController@saveFindingsForInspection');
Route::post('inspection/save', 'TransactionController@saveInspection');
Route::post('inspection/loginmobile', 'TransactionController@login');
Route::post('inspection/changeRoomStatus', 'TransactionController@changeRoomStatus');
Route::post('inspection/endStatus', 'TransactionController@endStatus');
Route::post('inspection/checklist', 'TransactionController@checklist');
Route::get('inspection/remarks/getstandards', 'EagleEyeController@GetStandards');
Route::get('inspection/remarks/loadfindingtypes', 'EagleEyeController@LoadFindingTypes');
Route::get('inspection/remarks/loadremarksinformation', 'EagleEyeController@LoadRemarksInformation');
Route::post('inspection/remarks/updateremarksinformation', 'EagleEyeController@UpdateRemarksInformation');

//Remarks Separator API
//Route::post('inspection/remarks/remarksseparatecomma', 'EagleEyeController@RemarksSeparateComma');
//===============================================================================================//

//Inspection For Mobile
Route::get('getDetails/', 'EagleEyeController@forTblLink');
//Area
Route::get('getArea/area/{areaId?}', 'EagleEyeController@getAreas');
//Components
Route::get('getComponent/area/{areaId}/component/{componentId}/remarks/{remarks?}', 'EagleEyeController@getComponents');

//Inventory API
Route::get('inventory/loadinventoryinformation', 'InventoryController@LoadInventoryInformation');
Route::post('inventory/newinventory', 'InventoryController@NewInventory');
Route::post('inventory/removeinventory', 'InventoryController@RemoveInventory');
Route::get('inventory/profileinventory', 'InventoryController@ProfileInventory');
Route::post('inventory/updateinventory', 'InventoryController@UpdateInventory');

//Vehicle API
Route::get('vehicle/loadvehicleinformation', 'VehiclesController@LoadVehicleInformation');
Route::post('vehicle/newvehicle', 'VehiclesController@NewVehicle');
Route::post('vehicle/removevehicle', 'VehiclesController@RemoveVehicle');
Route::get('vehicle/profilevehicle', 'VehiclesController@ProfileVehicle');
Route::post('vehicle/updatevehicle', 'VehiclesController@UpdateVehicle');
Route::get('vehicle/validatevehiclename', 'VehiclesController@ValidateVehicleName');

//Reservation API
Route::get('reservation/loadreservationinformation', 'ReservationController@LoadReservationInformation');
Route::post('reservation/newreservation', 'ReservationController@NewReservation');
Route::post('reservation/removereservation', 'ReservationController@RemoveReservation');
Route::post('reservation/updatereservation', 'ReservationController@UpdateReservation');
Route::get('reservation/getreservationprofile', 'ReservationController@GetReservationProfile');

//User Access And User Access Profile API
Route::get('useraccess/loaduserroles', 'UserAccessController@LoadUserRoles');
Route::post('useraccess/adduserrole', 'UserAccessController@AddUserRole');
Route::get('useraccessprofile/loadrolename', 'UserAccessController@LoadRoleName');
Route::get('useraccessprofile/loadallowstatus', 'UserAccessController@LoadAllowStatus');
Route::get('useraccessprofile/loadroomstatus', 'UserAccessController@LoadRoomStatus');
Route::post('useraccessprofile/savetargetstatusinfo', 'UserAccessController@SaveTargetStatusInfo');
Route::get('useraccessprofile/loadallowstatusprofile', 'UserAccessController@LoadAllowStatusProfile');
Route::post('useraccessprofile/removeallowstatus', 'UserAccessController@RemoveAllowStatus');
Route::get('useraccess/loadresources', 'UserAccessController@LoadResources');
Route::post('useraccess/saveresources', 'UserAccessController@SaveResources');
Route::get('useraccessprofile/loadallowresources', 'UserAccessController@LoadAllowResources');
Route::post('useraccessprofile/resourcesenabled', 'UserAccessController@ResourcesEnabled');
Route::post('useraccessprofile/resourcesdisabled', 'UserAccessController@ResourcesDisabled');

//Kiosk API
Route::get('kiosk/loadrestrictionsweb', 'KioskController@LoadRestrictionsWeb');
Route::post('kiosk/restrictionenabledweb', 'KioskController@RestrictionEnabledWeb');
Route::post('kiosk/restrictiondisabledweb', 'KioskController@RestrictionDisabledWeb');
Route::get('kiosk/loadrestrictionsmobile', 'KioskController@LoadRestrictionsMobile');
Route::post('kiosk/restrictionenabledmobile', 'KioskController@RestrictionEnabledMobile');
Route::post('kiosk/restrictiondisabledmobile', 'KioskController@RestrictionDisabledMobile');


//Settings API
Route::get('settings/loadsettings', 'SettingController@LoadSettings');
Route::post('settings/savesettings', 'SettingController@SaveSettings');
Route::get('settings/loadroomtypegroup', 'SettingController@LoadRoomTypeGroup');
Route::get('settings/loadroomtype', 'SettingController@LoadRoomType');
Route::get('settings/loadstatuspoints', 'SettingController@LoadStatusPoints');
Route::get('settings/loadstatusname', 'SettingController@LoadStatusName');
Route::post('settings/savepointsinformation', 'SettingController@SavePointsInformation');
Route::get('settings/loadpoints', 'SettingController@LoadPoints');
Route::get('settings/loadscorerating', 'SettingController@LoadScoreRating');
Route::post('settings/savenewrating', 'SettingController@SaveNewRating');
Route::get('settings/loadratinginformation', 'SettingController@LoadRatingInformation');
Route::post('settings/updaterating', 'SettingController@UpdateRating');
Route::post('settings/deleterating', 'SettingController@DeleteRating');
Route::get('settings/loadroombalancer', 'SettingController@LoadRoomBalancer');
Route::get('settings/loadgrptype', 'SettingController@LoadGRPType');
Route::post('settings/saveroomtypepercentage', 'SettingController@SaveRoomTypePercentage');
Route::post('settings/deleteroomtypepercentage', 'SettingController@DeleteRoomTypePercentage');


//Checklist API
Route::get('checklist/loadchecklist', 'ChecklistController@LoadChecklist');
Route::get('checklist/loadareas', 'ChecklistController@LoadAreas');
Route::get('checklist/loadcomponents', 'ChecklistController@LoadComponents');
Route::get('checklist/loadstandards', 'ChecklistController@LoadStandards');
Route::get('checklist/loadremarks', 'ChecklistController@LoadRemarks');
Route::post('checklist/savechecklistinformation', 'ChecklistController@SaveChecklistInformation');
Route::get('checklist/loadfindingtype', 'ChecklistController@LoadFindingType');
Route::get('checklist/getchecklistinformation', 'ChecklistController@GetChecklistInformation');
Route::post('checklist/updatechecklistinformation', 'ChecklistController@UpdateChecklistInformation');
Route::post('checklist/deletechecklistinformation', 'ChecklistController@DeleteChecklistInformation');

//Nationality API
Route::get('nationality/loadnationality', 'NationalityController@LoadNationality');
Route::post('nationality/savenationality', 'NationalityController@SaveNationality');
Route::get('nationality/getnationalityinfo', 'NationalityController@GetNationalityInfo');
Route::post('nationality/updatenationalityinfo', 'NationalityController@UpdateNationalityInfo');

//Area API
Route::get('area/loadarea', 'AreaController@LoadArea');
Route::post('area/savearea', 'AreaController@SaveArea');
Route::get('area/loadareainformation', 'AreaController@LoadAreaInformation');
Route::post('area/updatearea', 'AreaController@UpdateArea');

//Notification API
Route::post('notifications/getroles', 'NotificationsController@getRoles');
Route::get('notification/getadded', 'NotificationsController@getAdded');
Route::post('notifications/add', 'NotificationsController@addRole');
Route::get('notification/getGroup', 'NotificationsController@getGroup');
Route::get('notification/getGroupMatino', 'NotificationsController@matinonagroup');
Route::post('notification/groupadd', 'NotificationsController@groupadd2');
Route::post('notification/deleteRole', 'NotificationsController@removeRole');
Route::post('notification/deletegroup', 'NotificationsController@removegroup');
Route::get('notification/group/roles', 'NotificationsController@getRolesMembership');
Route::post('notification/group/member/add', 'NotificationsController@memberadd');
Route::post('notification/group/getinfomember', 'NotificationsController@getMemberInfo');
Route::get('notification/group/loadgroupinformation', 'NotificationsController@LoadGroupInformation');
Route::get('notification/group/loadgroupdata', 'NotificationsController@LoadGroupData');
Route::get('notification/group/loadroles', 'NotificationsController@LoadRoles');
Route::post('notification/group/addroles', 'NotificationsController@AddRoles');
Route::post('notification/group/removeroles', 'NotificationsController@RemoveRoles');

//Notification Schedule API
Route::get('schedule/loadgroup', 'NotificationScheduleController@LoadGroup');
Route::post('schedule/saveschedule', 'NotificationScheduleController@SaveSchedule');
Route::get('schedule/loadschedules', 'NotificationScheduleController@LoadSchedules');
Route::get('schedule/loadscheduleinformation', 'NotificationScheduleController@LoadScheduleInformation');
Route::post('schedule/updateschedule', 'NotificationScheduleController@UpdateSchedule');
Route::post('schedule/removeschedule', 'NotificationScheduleController@RemoveSchedule');

//Event API
Route::get('event/loadeventreason', 'EventController@LoadEventReason');
Route::post('event/saveeventreason', 'EventController@SaveEventReason');
Route::get('event/loadeventprofile', 'EventController@LoadEventProfile');
Route::post('event/updateeventreason', 'EventController@UpdateEventReason');

