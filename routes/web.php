<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['preventbackhistory']], function () {

    Route::group(['middleware' => ['checkislogin']], function () {
        Route::get('/', 'LoginController@index');
        Route::get('/login', 'LoginController@index')->name('login');
    });

    Route::group(['middleware' => ['checkisuser']], function () {
        Route::get('/main', 'MainController@index');
        Route::get('/roomstatus/{view?}', 'RoomStatusController@index');
        Route::get('/rooms/{view?}/{id?}', 'RoomController@index');
        Route::get('/eagleye', 'EagleEyeController@index');
        Route::get('/components', 'EagleEyeController@components');
        Route::get('/standard', 'EagleEyeController@standard');
        Route::get('/remarks', 'EagleEyeController@remarks');
        Route::get('/findings', 'EagleEyeController@findings');
        Route::get('/inventory', 'InventoryController@index');
        Route::get('/vehicle', 'VehiclesController@index');
        Route::get('/reservation', 'ReservationController@index');
        Route::get('/useraccess/{view?}/{id?}', 'UserAccessController@index');
        Route::get('/kiosk', 'KioskController@index');
        Route::get('/settings/{view?}/{id?}', 'SettingController@index');
        Route::get('/checklist', 'ChecklistController@index');
        Route::get('/nationality', 'NationalityController@index');
        Route::get('/areas', 'AreaController@index');
        Route::get('/blocklistadd', 'NotificationsController@add');
        Route::get('/notifgroupadd', 'NotificationsController@groupadd');
        Route::get('/notificationmembershipadd', 'NotificationsController@membershipadd');
        Route::get('/notificationschedule', 'NotificationScheduleController@index');
        Route::get('/event', 'EventController@index');
    });

});

