SELECT * FROM
(SELECT a.id, a.room_id, roomstatus.room_status AS 'previews_status', a.p_dateTime AS 'p_date_time', b.room_status, b.s_dateTime AS 'date_time', TIMEDIFF(b.s_dateTime, a.p_dateTime) AS 'time_difference', a.created_at
FROM rms_logs AS a
LEFT JOIN (SELECT rms_logs.id, roomstatus.room_status, rms_logs.s_dateTime FROM rms_logs LEFT JOIN roomstatus ON rms_logs.s_status=roomstatus.id) AS b ON b.id=a.id
INNER JOIN roomstatus ON roomstatus.id=a.p_status
UNION
SELECT c.id, c.room_id, roomstatus.room_status AS 'previews_status', c.s_datetime AS 'p_date_time', d.room_status, d.e_dateTime, TIMEDIFF(d.e_dateTime, c.s_datetime) AS 'time_difference', c.created_at
FROM rms_logs AS c
LEFT JOIN (SELECT rms_logs.id, roomstatus.room_status, rms_logs.e_dateTime FROM rms_logs LEFT JOIN roomstatus ON rms_logs.e_status=roomstatus.id) AS d ON d.id=c.id
INNER JOIN roomstatus ON roomstatus.id=c.s_status
ORDER BY id DESC) AS room_status

SELECT * FROM
(SELECT a.id, a.room_id, status.room_status AS 'previews_status', a.p_dateTime AS 'p_date_time', b.room_status, CONCAT(emp.lastname, ', ', emp.firstname, ' ', emp.middlename) AS 'user', b.s_dateTime AS 'date_time', TIMEDIFF(b.s_dateTime, a.p_dateTime) AS 'time_difference', a.created_at
FROM rms_logs AS a
LEFT JOIN (SELECT rms_logs.id, rms_logs.s_emp_id , roomstatus.room_status, rms_logs.s_dateTime FROM rms_logs LEFT JOIN roomstatus ON rms_logs.s_status=roomstatus.id) AS b ON b.id=a.id
INNER JOIN roomstatus AS status ON status.id=a.p_status
LEFT JOIN user AS emp ON emp.username=b.s_emp_id
UNION 
SELECT c.id, c.room_id, status.room_status AS 'previews_status', c.s_datetime AS 'p_date_time', d.room_status, CONCAT(emp.lastname, ', ', emp.firstname, ' ', emp.middlename) AS 'user', d.e_dateTime AS 'date_time', TIMEDIFF(d.e_dateTime, c.s_datetime) AS 'time_difference', c.created_at
FROM rms_logs AS c
LEFT JOIN (SELECT rms_logs.id, rms_logs.e_emp_id, roomstatus.room_status, rms_logs.e_dateTime FROM rms_logs LEFT JOIN roomstatus ON rms_logs.e_status=roomstatus.id) AS d ON d.id=c.id
INNER JOIN roomstatus AS status ON status.id=c.s_status
LEFT JOIN user AS emp ON emp.username=d.e_emp_id
ORDER BY id DESC) AS room_status WHERE DATE_FORMAT(room_status.created_at, '%Y-%m-%d')='2018-12-17'AND room_status.room_id=17

SELECT * FROM
(SELECT a.id, a.room_id, roomstatus.room_status AS 'previews_status', a.p_dateTime AS 'p_date_time', b.room_status, b.s_dateTime AS 'date_time', TIMEDIFF(b.s_dateTime, a.p_dateTime) AS 'time_difference', a.created_at
FROM rms_logs AS a
LEFT JOIN (SELECT rms_logs.id, roomstatus.room_status, rms_logs.s_dateTime FROM rms_logs LEFT JOIN roomstatus ON rms_logs.s_status=roomstatus.id) AS b ON b.id=a.id
INNER JOIN roomstatus ON roomstatus.id=a.p_status
UNION
SELECT c.id, c.room_id, roomstatus.room_status AS 'previews_status', c.s_datetime AS 'p_date_time', d.room_status, d.e_dateTime AS 'date_time', TIMEDIFF(d.e_dateTime, c.s_datetime) AS 'time_difference', c.created_at
FROM rms_logs AS c
LEFT JOIN (SELECT rms_logs.id, roomstatus.room_status, rms_logs.e_dateTime FROM rms_logs LEFT JOIN roomstatus ON rms_logs.e_status=roomstatus.id) AS d ON d.id=c.id
INNER JOIN roomstatus ON roomstatus.id=c.s_status
ORDER BY id DESC) AS room_status WHERE room_status.room_id=21 AND DATE_FORMAT(created_at, '%Y-%m')='2018-12'

SELECT a.id, CONCAT(user.firstname, ', ', lastname, ' ', middlename) as 'emp', a.room_id, roomstatus.room_status AS 'previews_status', a.p_dateTime AS 'p_date_time', b.room_status, b.s_dateTime AS 'date_time', TIMEDIFF(b.s_dateTime, a.p_dateTime) AS 'time_difference', a.created_at, tbl_room_buddies.from
FROM rms_logs AS a
LEFT JOIN (SELECT rms_logs.id, roomstatus.room_status, rms_logs.s_dateTime FROM rms_logs LEFT JOIN roomstatus ON rms_logs.s_status=roomstatus.id) AS b ON b.id=a.id
INNER JOIN roomstatus ON roomstatus.id=a.p_status
LEFT JOIN tbl_room_buddies ON tbl_room_buddies.rms_logs_id=a.id
LEFT JOIN user ON user.username=tbl_room_buddies.emp_Id
UNION
SELECT c.id, CONCAT(user.firstname, ', ', lastname, ' ', middlename) as 'emp', c.room_id, roomstatus.room_status AS 'previews_status', c.s_datetime AS 'p_date_time', d.room_status, d.e_dateTime, TIMEDIFF(d.e_dateTime, c.s_datetime) AS 'time_difference', c.created_at, tbl_room_buddies.from
FROM rms_logs AS c
LEFT JOIN (SELECT rms_logs.id, roomstatus.room_status, rms_logs.e_dateTime FROM rms_logs LEFT JOIN roomstatus ON rms_logs.e_status=roomstatus.id) AS d ON d.id=c.id
INNER JOIN roomstatus ON roomstatus.id=c.s_status
LEFT JOIN tbl_room_buddies ON tbl_room_buddies.rms_logs_id=c.id
LEFT JOIN user ON user.username=tbl_room_buddies.emp_Id
ORDER BY id DESC